package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

public class PopupRegister {
    //PopupWindow display method
    private Context context;
    Activity acv;

    String auth_token = "";
    String url = "";
    Button db_rg_button_1;

    Typeface poppin_regular;
    Typeface poppin_bold;

    TextView title, content;

    public  PopupRegister(Context ctx, Activity actvy)
    {
        context=ctx;
        acv=actvy;

    }

    public void showPopupWindow(final View view) {


        auth_token = UserDataRepository.getInstance().getAuthToken();
        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_register_layout, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.alert_popup_window_animation);
        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        poppin_bold = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-Bold.ttf");
        poppin_regular = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-Regular.ttf");

        title = (TextView)popupView.findViewById(R.id.title_text);
        content = (TextView)popupView.findViewById(R.id.content_text);
        title.setTypeface(poppin_bold);
        content.setTypeface(poppin_bold);

        //Initialize the elements of our window, install the handler

        //TextView test2 = popupView.findViewById(R.id.titleText);
        //test2.setText("TNPSC Exams");

        //Toast.makeText(view.getContext(), auth_token, Toast.LENGTH_SHORT).show();
        url = "http://13.235.243.15/an_new_payment_1?code=" + auth_token;

        db_rg_button_1 = popupView.findViewById(R.id.db_rg_button_1);
        db_rg_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(view.getContext(), "Register Online...", Toast.LENGTH_SHORT).show();
                //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                //acv.startActivity(browserIntent);
                //Intent ii = new Intent(context, CourseActivity.class);
                //acv.startActivity(ii);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url)); // only used based on your nbmedia.
                String title = "Select a browser";
                // Create intent to show the chooser dialog
                Intent chooser = Intent.createChooser(intent, title);

                // Verify the original intent will resolve to at least one activity
                if (intent.resolveActivity(acv.getPackageManager()) != null) {
                    acv.startActivity(chooser);
                }

            }
        });

        /*Button buttonEdit = popupView.findViewById(R.id.messageButton);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //As an nbmedia, display the message
                Toast.makeText(view.getContext(), "Wow, popup action button", Toast.LENGTH_SHORT).show();

            }
        });*/



        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }
}
