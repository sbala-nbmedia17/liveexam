package com.nbmedia.vidhvaa.Utils.Helpers;

import android.app.Activity;
import android.content.Context;

import com.nbmedia.vidhvaa.repository.UserDataRepository;

public class UserSignup {

    private Context context;
    Activity _activity;


    public UserSignup(Context context, Activity activity) {

        this.context = context;
        this._activity = activity;



    }

    public boolean checkSignupUser()
    {
        boolean status = false;
        String stat = UserDataRepository.getInstance().checkSignupUser();
        if(stat.equals("3")) {
            //show popup
           status = false;
        }
        else if(stat.equals("1")) {
            //goto reminder
           status = true;

        }

        return status;
    }
}
