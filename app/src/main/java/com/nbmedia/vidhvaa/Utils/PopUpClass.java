package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nbmedia.vidhvaa.R;

public class PopUpClass {
    //PopupWindow display method
    private Context context;
    Activity acv;
    Button db_gr_button_1;
    Button db_gr_button_2;
    Button db_gr_button_3;
    Button db_gr_button_4;
    PopupWindow popupWindow;

    public  PopUpClass(Context ctx, Activity actvy)
    {
        context=ctx;
        acv=actvy;

    }

    public void dismissPopup() {
        if(popupWindow!=null) {
            popupWindow.dismiss();
        }
    }

    public void showPopupWindow(final View view) {


        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_layout, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.alert_popup_window_animation);
        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        popupWindow.setAnimationStyle(R.style.alert_popup_window_animation);
        //Initialize the elements of our window, install the handler

        TextView test2 = popupView.findViewById(R.id.titleText);
        test2.setText("TNPSC Exams");

        db_gr_button_1 = popupView.findViewById(R.id.db_gr_button_1);
        db_gr_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent ii = new Intent(context, CourseActivity.class);
                ii.putExtra("ct_code", "EXCT01");
                ii.putExtra("ex_code", "EX01");
                ii.putExtra("ln_code", "-");
                ii.putExtra("m_p_flg", "0");
                acv.startActivity(ii);*/
            }
        });

        db_gr_button_2 = popupView.findViewById(R.id.db_gr_button_2);
        db_gr_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent ii = new Intent(context, CourseActivity.class);
                ii.putExtra("ct_code", "EXCT01");
                ii.putExtra("ex_code", "EX02");
                ii.putExtra("ln_code", "-");
                ii.putExtra("m_p_flg", "0");
                acv.startActivity(ii);*/
            }
        });

        db_gr_button_3 = popupView.findViewById(R.id.db_gr_button_3);
        db_gr_button_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  Intent ii = new Intent(context, CourseActivity.class);
                ii.putExtra("ct_code", "EXCT01");
                ii.putExtra("ex_code", "EX03");
                ii.putExtra("ln_code", "-");
                ii.putExtra("m_p_flg", "0");
                acv.startActivity(ii);*/
            }
        });

        db_gr_button_4 = popupView.findViewById(R.id.db_gr_button_4);
        db_gr_button_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent ii = new Intent(context, CourseActivity.class);
                ii.putExtra("ct_code", "EXCT01");
                ii.putExtra("ex_code", "EX04");
                ii.putExtra("ln_code", "-");
                ii.putExtra("m_p_flg", "0");
                acv.startActivity(ii);*/
            }
        });

        /*Button buttonEdit = popupView.findViewById(R.id.messageButton);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //As an nbmedia, display the message
                Toast.makeText(view.getContext(), "Wow, popup action button", Toast.LENGTH_SHORT).show();

            }
        });*/



        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }
}
