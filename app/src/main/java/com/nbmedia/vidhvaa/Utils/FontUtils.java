package com.nbmedia.vidhvaa.Utils;

import android.content.Context;
import android.graphics.Typeface;

import com.nbmedia.vidhvaa.VidhvaaApplication;

public class FontUtils {

    private Typeface mPoppinRegular, mPoppinBold, mBamini;
    private static FontUtils mInstance;
    private Context mContext;

    public static FontUtils getInstance() {
        return mInstance == null ? new FontUtils() : mInstance;
    }

    private FontUtils() {
        mContext = VidhvaaApplication.getAppContext();
        mPoppinBold = Typeface.createFromAsset(mContext.getAssets(), "fonts/Poppins-Bold.ttf");
        mPoppinRegular = Typeface.createFromAsset(mContext.getAssets(), "fonts/Poppins-Regular.ttf");
        mBamini = Typeface.createFromAsset(mContext.getAssets(), "fonts/Bamini.TTF");
    }

    public Typeface getPoppinRegular() {
        return mPoppinRegular;
    }

    public Typeface getPoppinBold() {
        return mPoppinBold;
    }

    public Typeface getBamini() {
        return mBamini;
    }

}
