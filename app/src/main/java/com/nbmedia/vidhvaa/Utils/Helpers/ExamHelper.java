package com.nbmedia.vidhvaa.Utils.Helpers;

import android.content.Context;

import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.Utils.PreciseCountdown;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class ExamHelper {

    private static ExamHelper mInstance;
    private Context mContext;

    public int mQuestionCounter, mQuestionLength, mCorrectAnswer, mWrongAnswer, mSkippedAnswer, mProgressbarValue, mLocalCounter, mCounter, mLanguageValue, mSeconds;
    public List<ModelQuestion> mQuizQuestions;
    public boolean isRunning, isLastQuestion, isFirstQuestion;
    public String mAuthToken, mExamCode, mExamTypeCode, mExamCategoryCode, mLanguageCode;
    public JSONArray AllData;
    public PreciseCountdown countDown;

    public ExamHelper()
    {
        mContext = VidhvaaApplication.getAppContext();

        mQuestionCounter = 0;
        mQuestionLength = 0;
        mCorrectAnswer = 0;
        mWrongAnswer = 0;
        mSkippedAnswer = 0;
        mProgressbarValue = 18;
        mLocalCounter = 0;
        mCounter = 18;
        mLanguageValue = 1;
        mSeconds = 18;

        mQuizQuestions = new ArrayList<>();

        isRunning = false;
        isLastQuestion = false;
        isFirstQuestion = true;

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();
        mExamCode = "";
        mExamTypeCode = "";
        mExamCategoryCode = "";
        mLanguageCode = "";
    }

    public String getTagRemover(String tag)
    {
        return tag.replace("<p>", "");
    }

    public String removeTag(String rTag1, String rTag2, String rData)
    {
        rData = rData.replace(rTag1, "");
        rData = rData.replace(rTag2, "");
        rData = addSlashes(rData);

        return rData;
    }

    public String addSlashes(String rData)
    {
        if(rData.contains("\\"))  {
            rData = rData.replace("\\", "\\\\");
        }
        return rData;
    }

    public int getCurrentQuestionSeconds(int rLength, int rCounter, List<ModelQuestion> rQuestions)
    {
        mSeconds = -1;
        if(rLength> 0)
        {
            if (rCounter < rLength) {
                if (!rQuestions.get(rCounter).getmQuestionSeconds().equals("")) {
                    mSeconds = Integer.parseInt(rQuestions.get(rCounter).getmQuestionSeconds());
                }
            }
        }

        return mSeconds;

    }

    public boolean checkLastBeforeQuestion(int rxCounter)
    {
        int rCounter = rxCounter - 1;
        if(rCounter ==  rxCounter) {
            return true;
        }
        return false;
    }

    public boolean checkQuestionValidation(int rLength, int rCounter)
    {
        if(rLength > 0)  {
            if (rCounter < rLength) {
               return true;
            }
        }

        return false;
    }



}
