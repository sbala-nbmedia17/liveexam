package com.nbmedia.vidhvaa.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.nbmedia.vidhvaa.VidhvaaApplication;

import static android.content.Context.MODE_PRIVATE;


public class PreferenceUtils {

    public static final String APP_PREF_NAME = "pref_vidhvaa";
    public static final String PREF_FIRE_TOKEN = "pref_firebase_access_token";
    public static final String PREF_APP_AUTH_TOKEN = "pref_app_auth_token";
    public static final String EXAM_PREF = "pref_exam_status";
    public static final String PREF_USER_DATA = "pref_user_data";
    public static final String PREF_VALIDATE_OTP = "pref_validate_otp";
    public static final String PREF_OFFLINE_PRACTISE_QUESTIONS = "pref_prac_qstn_";
    public static final String PREF_OFFLINE_MODEL_EXAM_QUESTIONS = "pref_model_qstn_";
    public static final String PREF_USER_STATUS = "NONE";
    public static final String PREF_MODEL_EXAM_STATUS = "NONE";
    public static final String PREF_TIMER_HOLD="pref_timer_hold";
    private static PreferenceUtils mInstance;
    private final Context mContext;
    private final SharedPreferences mPreference;
    private final SharedPreferences.Editor mEditor;

    private PreferenceUtils() {
        mContext = VidhvaaApplication.getAppContext();
        mPreference = mContext.getSharedPreferences(APP_PREF_NAME, MODE_PRIVATE);
        mEditor = mPreference.edit();
    }

    public static PreferenceUtils getInstance() {
        return mInstance == null ? new PreferenceUtils() : mInstance;
    }

    public void setPreferenceContent(String key, String value) {
        mEditor.putString(key, value);
        mEditor.commit();
    }

    public Object getPreferenceContent(String key) {
        return mPreference.getString(key, "");
    }

    // This api equalant to logout feature
    public void clearAllSession() {
        String prefToken = mPreference.getString(PREF_FIRE_TOKEN, "");
        mEditor.clear().commit();
//        Log.d("PreferenceUtils", "clearAllSession");
        setPreferenceContent(PREF_FIRE_TOKEN, prefToken);
    }

    public boolean hasPreference(String key){
        return mPreference.contains(key);
    }

}