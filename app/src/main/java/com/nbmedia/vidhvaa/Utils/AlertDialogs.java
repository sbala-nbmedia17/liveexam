package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.nbmedia.vidhvaa.Activity.MainActivity;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.VidhvaaApplication;

public class AlertDialogs {

    private Context context;
    Activity acv;


    public AlertDialogs(Context ctx, Activity actvy)
    {
        context=ctx;
        acv=actvy;

    }

    public void showInformationDialog(String msg){
        ViewGroup viewGroup = acv.findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(acv).inflate(R.layout.dialog_error, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        ImageView img =dialogView.findViewById(R.id.img_info);
        img.setImageResource(android.R.drawable.ic_dialog_info);
        TextView tvContent = dialogView.findViewById(R.id.tv_alert_content);
        tvContent.setText(msg);

        Button btn_ok = dialogView.findViewById(R.id.btn_alert_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    public void showSuccessInformationDialog(String msg){
        ViewGroup viewGroup = acv.findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(acv).inflate(R.layout.dialog_success, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(VidhvaaApplication.getAppContext());

        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        ImageView img =dialogView.findViewById(R.id.img_info);
        img.setImageResource(android.R.drawable.ic_dialog_info);
        TextView tvContent = dialogView.findViewById(R.id.tv_alert_content);
        tvContent.setText(msg);

        Button btn_ok = dialogView.findViewById(R.id.btn_alert_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    public void showErrorInformation(String msg)
    {
        ViewGroup viewGroup = acv.findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(acv).inflate(R.layout.dialog_error, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        TextView tvContent = dialogView.findViewById(R.id.tv_alert_content);
        tvContent.setText(msg);

        Button btn_ok = dialogView.findViewById(R.id.btn_alert_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

    }

    public void showErrorInformationWithActivity(String msg)
    {
        ViewGroup viewGroup = acv.findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(acv).inflate(R.layout.dialog_error, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        TextView tvContent = dialogView.findViewById(R.id.tv_alert_content);
        tvContent.setText(msg);

        Button btn_ok = dialogView.findViewById(R.id.btn_alert_ok);

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii=new Intent(context, MainActivity.class);
                acv.startActivity(ii);
            }
        });

    }
}
