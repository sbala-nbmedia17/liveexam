package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;

public class OwnFont {

    private Context context;
    Activity acv;

    Typeface poppin_regular;
    Typeface poppin_bold;
    Typeface bamini;


    public OwnFont(Context ctx, Activity actvy)
    {
        context=ctx;
        acv=actvy;

        poppin_bold = Typeface.createFromAsset(acv.getAssets(), "fonts/Poppins-Bold.ttf");
        poppin_regular = Typeface.createFromAsset(acv.getAssets(), "fonts/Poppins-Regular.ttf");
        bamini = Typeface.createFromAsset(acv.getAssets(), "fonts/Bamini.TTF");
    }

    public Typeface setPoppin_regular()  {
        return  poppin_regular;
    }

    public Typeface setPoppin_bold()  {
        return  poppin_bold;
    }

    public Typeface setBamini()  {
        return  bamini;
    }

}
