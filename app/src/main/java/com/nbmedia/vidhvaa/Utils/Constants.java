package com.nbmedia.vidhvaa.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;

import com.nbmedia.vidhvaa.R;

public class Constants {

    //GOOGLE CLOUD
    //public static final String DOMAIN_URL ="http://www.vidhvaa.com/vidhvaa/api";
    //public static final String HYBRID_DOMAIN_URL ="http://www.vidhvaa.com/vidhvaa";

    //AWS EC2
    //public static final String DOMAIN_URL ="http://65.0.89.98/api";
    //public static final String HYBRID_DOMAIN_URL ="http://65.0.89.98";

    //AWS LIGHTSAIL
    public static final String DOMAIN_URL = "http://13.235.243.15/api";
    public static final String HYBRID_DOMAIN_URL = "http://13.235.243.15";
    public static final String PAYMENT_URL = "https://www.vidhvaa.in";

    //To store boolean in shared preferences for if the device is registered to not
    public static final String REGISTERED = "registered";
    public static final String FIRST_COLUMN = "First";
    public static final String SECOND_COLUMN = "Second";
    public static final String THIRD_COLUMN = "Third";
    public static final String FOURTH_COLUMN = "Fourth";
    public static final String FIFTH_COLUMN = "Fifth";
    public static final String SIXTH_COLUMN = "Sixth";
    public static final String SEVENTH_COLUMN = "Seventh";
    public static final String EIGHT_COLUMN = "Eight";

    public static final String REGISTER_USER = "" + DOMAIN_URL + "/mobile_new_registration";
    public static final String REGISTER_USER_UPDATE = "" + DOMAIN_URL + "/mobile_new_user_update";
    public static final String REGISTER_USER_OTP = "" + DOMAIN_URL + "/mobile_new_registration_otp";
    public static final String REGISTER_USER_OTP_RESEND = "" + DOMAIN_URL + "/mobile_new_registration_resend_otp";
    public static final String REGISTER_USER_EX_OTP = "" + DOMAIN_URL + "/mobile_registration_ex_otp_new";
    public static final String PROFILE_DETAILS = "" + DOMAIN_URL + "/mobile_user_details";
    public static final String PROFILE_DETAILS_UPDATE = "" + DOMAIN_URL + "/mobile_user_details_update";
    public static final String CHECK_REGISTER_NO = "" + DOMAIN_URL + "/mobile_check_exam_code";
    public static final String USER_GET_PROFILE = "" + DOMAIN_URL + "/paid-user-details";
    public static final String USER_UPDATE_PROFILE = "" + DOMAIN_URL + "/paid-user-update";
    public static final String USER_NAME_UPDATE = "" + DOMAIN_URL + "/mobile_user_name_update";
    public static final String PROFILE_MPIN_UPDATE = "" + DOMAIN_URL + "/mobile_user_mpin_update";
    public static final String PROFILE_IMAGE_UPLOAD = "" + DOMAIN_URL + "/mobile_user_photo_upload";
    public static final String PAYMENT_CHECK = "" + DOMAIN_URL + "/mobile_user_payment_check";
    public static final String DAILY_TASK_CODE = "" + DOMAIN_URL + "/mobile_daily_task_code";
    public static final String DAILY_TASK_REGISTER_CODE = "" + DOMAIN_URL + "/mobile_daily_task_register_code";
    public static final String DAILY_TASK_REGISTER_QUESTIONS = "" + DOMAIN_URL + "/mobile_daily_task_register_questions";
    public static final String DAILY_TASK_QUESTION = "" + DOMAIN_URL + "/mobile_daily_task_question";
    public static final String DAILY_TASK_QUESTIONS = "" + DOMAIN_URL + "/mobile_daily_task";
    public static final String DAILY_TASK_RESULT = "" + DOMAIN_URL + "/mobile_daily_task_result";
    public static final String DAILY_TASK_ONE_RESULT = "" + DOMAIN_URL + "/mobile_daily_task_one_result";
    public static final String DAILY_TASK_RANK = "" + DOMAIN_URL + "/mobile_daily_task_rank";
    public static final String DAILY_EVENT_DATE = "" + DOMAIN_URL + "/mobile_daily_event";
    public static final String DAILY_EVENT_MATERIAL = "" + DOMAIN_URL + "/mobile_daily_event_material";
    public static final String MODEL_EXAM_DATE = "" + DOMAIN_URL + "/mobile_model";
    public static final String MODEL_EXAM_CODE = "" + DOMAIN_URL + "/mobile_model_get_code";
    public static final String MODEL_EXAM_QUESTION = "" + DOMAIN_URL + "/mobile_model_question";
    public static final String MODEL_EXAM_RESULT = "" + DOMAIN_URL + "/mobile_model_result";
    public static final String MODEL_EXAM_ONE_RESULT = "" + DOMAIN_URL + "/mobile_model_one_result";
    public static final String USER_MODEL_EXAM = "" + DOMAIN_URL + "/mobile_user_model_exam";
    public static final String USER_MODEL_EXAM_QUESTION = "" + DOMAIN_URL + "/mobile_user_model_exam_question";
    public static final String COURSER_CATEGORY = "" + DOMAIN_URL + "/mobile_course";
    public static final String COURSER_CATEGORY_CHAPTER = "" + DOMAIN_URL + "/mobile_general_study";
    public static final String MATERIAL_MODEL_QUESTION = "" + DOMAIN_URL + "/mobile_material_model_question";
    public static final String MATERIAL_MODEL_RESULT = "" + DOMAIN_URL + "/mobile_material_model_result";
    public static final String USER_MATERIAL_MODEL_RESULT = "" + DOMAIN_URL + "/mobile_user_material_model_exam_question";
    public static final String USER_HISTORY = "" + DOMAIN_URL + "/mobile_user_history";
    public static final String USER_DAILY_TASK_QUESTION = "" + DOMAIN_URL + "/mobile_user_daily_task_question";
    public static final String MATERIAL_GENERAL_QUESTION = "" + DOMAIN_URL + "/mobile_material_general_question";
    public static final String MATERIAL_LANGUAGE_QUESTION = "" + DOMAIN_URL + "/mobile_material_language_question";
    public static final String MATERIAL_CURRENT_QUESTION = "" + DOMAIN_URL + "/mobile_material_current_question";
    public static final String VALIDATE_AUTH_TOKEN = "" + DOMAIN_URL + "/check_token";
    public static final String MATERIAL_LANGUAGE_CHAPTER = "" + DOMAIN_URL + "/mobile_language_chapter";
    public static final String LEADER_BOARD = "" + DOMAIN_URL + "/mobile_leader";

    public static final String WEEKLY_DAILY_TASK_CODE = "" + DOMAIN_URL + "/weekly_daily_task_code";

    public static final String ONLINE_EXAM_QUESTION = "" + DOMAIN_URL + "/mobile_online_question";
    public static final String PUSH_NOTIFICATION = "" + DOMAIN_URL + "/mobile_notification";

    public static final String TEMP_CHECK_REG_NO = "" + DOMAIN_URL + "/check_exam_code";

    //hybrid url
    public static final String MATERIAL_PAGE_1 = "" + HYBRID_DOMAIN_URL + "/an_material";
    public static final String MATERIAL_PAGE_2 = "" + HYBRID_DOMAIN_URL + "/an_material_practise";

    //Payment External Url
    public static final String EXTERNAL_PAYMENT_URL = "" + PAYMENT_URL + "/an_new_payment_1";

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;
            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);
            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH));
        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();
        }
    }

    public static String getSplashVideo(Context context) {
        return "android.resource://" + context.getPackageName() + "/" + R.raw.splash;
    }

}