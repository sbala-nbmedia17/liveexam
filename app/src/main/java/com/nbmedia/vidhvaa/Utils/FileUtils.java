package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.os.Environment;
import android.view.inputmethod.InputMethodManager;

import java.io.File;

public class FileUtils {


    private Context mContext;
    private static FileUtils mInstance;

    FileUtils(Context context){
        mContext = context;
    }

    public static FileUtils getInstance(Context context){
        return mInstance==null?new FileUtils(context):mInstance;
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public String getParentDirectoryPath(){
        try{
            File folder = new File(Environment.getExternalStorageDirectory() +
                    File.separator + "Vidhvaa/");
            if( folder.exists())
                return folder.getPath();
            else {
                createParentDirectory();
                getParentDirectoryPath();
                return folder.getPath();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public void createParentDirectory(){
        File folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "Vidhvaa");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
    }

}
