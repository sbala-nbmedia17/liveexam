package com.nbmedia.vidhvaa.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.nbmedia.vidhvaa.R;

public class PopupConfirm {
    //PopupWindow display method
    private Context context;
    Activity acv;
    OwnFont ownFont;
    PopupWindow popupWindow;
    Button db_md_button_1;
    Button db_md_button_2;
    TextView tv_title;
    View popupView;

    public  PopupConfirm(Context ctx, Activity actvy)
    {
        context=ctx;
        acv=actvy;
        ownFont = new OwnFont(context, acv);
        initDialog();
    }

    private void initDialog(){
        //Create a View object yourself through inflater
        View view= acv.findViewById(android.R.id.content).getRootView();
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        popupView = inflater.inflate(R.layout.popup_dashboard_confirm, null);
        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.alert_popup_window_animation);
    }

    public void showLogoutWindow(final View view) {

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        tv_title = popupView.findViewById(R.id.tv_popup_dashboard_title);
        tv_title.setText("Are you want to logout");
        tv_title.setTypeface(ownFont.setPoppin_regular());

        db_md_button_1 = popupView.findViewById(R.id.button_dashboard_exit);
        db_md_button_1.setText("LOGOUT");
        db_md_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
//                ((DashboardActivity)acv).logoutMySession();

            }
        });

        db_md_button_2 = popupView.findViewById(R.id.button_dashboard_cancel);
        db_md_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(view.getContext(), "Yes That's good!", Toast.LENGTH_SHORT).show();
                popupWindow.dismiss();
            }
        });




        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });

    }

    public void showPopupWindow(final View view) {

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler
        tv_title = popupView.findViewById(R.id.tv_popup_dashboard_title);
        tv_title.setTypeface(ownFont.setPoppin_regular());

        db_md_button_1 = popupView.findViewById(R.id.button_dashboard_exit);
        db_md_button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(view.getContext(), "Sorry You Can't Exit", Toast.LENGTH_SHORT).show();
//                ((DashboardActivity)acv).exitDashboard();

                popupWindow.dismiss();
            }
        });

        db_md_button_2 = popupView.findViewById(R.id.button_dashboard_cancel);
        db_md_button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(view.getContext(), "Yes That's good!", Toast.LENGTH_SHORT).show();
                popupWindow.dismiss();
            }
        });




        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public void dismissPopup(){
        popupWindow.dismiss();
    }
}
