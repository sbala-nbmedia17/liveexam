package com.nbmedia.vidhvaa.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;

import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String CHANNEL_ID = "101";




    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN,""+s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.

        String Title = "";
        String Message = "";
        String flag = "";

        //showNotification("Vidhvaa", "This is test message from PHP Server");


       if (remoteMessage.getData()!=null && remoteMessage.getData().size() > 0)
        {

            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
//                Log.i(TAG, "Notification Data: " + json);
                Title = json.get("title").toString();
                Message = json.get("message").toString();
                flag = json.get("flag").toString();

                if (flag.equalsIgnoreCase("LogOut")){
//                    Log.d("FireBase","clearAllSession");
                    PreferenceUtils.getInstance().clearAllSession();
                    //new LocalDB(this, this).clearDataBase();
                    return;
                }
                showNotification(Title, Message, flag);



            } catch (Exception e) {
//                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }


    public static String getToken(Context context) {
        return PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN).toString();
    }

    void showNotification(String title, String message, String flag) {

        int requestID = (int) System.currentTimeMillis();

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "VIDHVAA",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("YOUR_NOTIFICATION_CHANNEL_DESCRIPTION");
            mNotificationManager.createNotificationChannel(channel);
        }

        //Intent notificationIntent = new Intent(getApplicationContext(), NotificationActivity.class);

       /* if(flag.equals("MPIN")) {
            notificationIntent = new Intent(getApplicationContext(), ReminderActivity.class);
        }

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);*/

       // PendingIntent contentIntent = PendingIntent.getActivity(this, requestID,notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

      /*  NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher) // notification icon
                .setContentTitle(title) // title for notification
                .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_launcher))
                .setContentText(message)// message for notification
                 .setContentIntent(contentIntent)
                .setAutoCancel(true); // clear notification after click
        //Intent intent = new Intent(getApplicationContext(), ACTIVITY_NAME.class);
        //PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntennt.FLAG_UPDATE_CURRENT);
        //mBuilder.setContentIntent(pi);
        mNotificationManager.notify(0, mBuilder.build());*/
    }
}
