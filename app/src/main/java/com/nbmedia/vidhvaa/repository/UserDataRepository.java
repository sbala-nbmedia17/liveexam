package com.nbmedia.vidhvaa.repository;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;

import org.json.JSONObject;

public class UserDataRepository {

    private Context mContext;
    private static UserDataRepository mInstance;

    private PreferenceUtils mPreferenceUtils;

    private UserDataRepository() {
        mContext = VidhvaaApplication.getAppContext();
    }

    public static UserDataRepository getInstance() {
        return mInstance == null ? new UserDataRepository() : mInstance;
    }

    public void updateUserType(String userType) {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            jsonObject.remove("user_type");
            jsonObject.put("user_type", "" + userType);
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUserName(String userName) {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            jsonObject.remove("user_name");
            jsonObject.put("user_name", "" + userName);
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUserMPIN(String userMPin) {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            jsonObject.remove("user_mpin");
            jsonObject.put("user_mpin", "" + userMPin);
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUserCode(String userCode) {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            jsonObject.remove("user_code");
            jsonObject.put("user_code", "" + userCode);
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getEmailId() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        if (!userData.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(userData);
                return jsonObject.getString("user_email");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public String getMobileNumber() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        if (!userData.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(userData);
                return jsonObject.getString("user_mobile");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public String getAuthToken() {
        return PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
    }

    public String getUserName() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        if (!userData.isEmpty()) {
            try {
                JSONObject jsonObject = new JSONObject(userData);
                //return jsonObject.getString("user_type").equalsIgnoreCase("GUEST") ? "Guest" : jsonObject.getString("user_name");
                return jsonObject.getString("user_name");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public String getUserCode() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            return jsonObject.getString("user_code");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getUserMPin() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            return jsonObject.getString("user_mpin");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public String getUserType() {
        String userData = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_DATA).toString();
        try {
            JSONObject jsonObject = new JSONObject(userData);
            return jsonObject.getString("user_type");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String checkSignupUser()
    {
        String stat = "0";
        String u_status = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_STATUS).toString();

        if(u_status.equals("PAID")) {
            stat = "1";     //paid user
        }
        if(u_status.equals("PENDING_OTP")) {
            stat = "2";     //otp pending
        }
        if(u_status.equals("GUEST")) {
            stat = "3";     //dashboard but not paid
        }
        return stat;
    }

    public boolean checkSignup()
    {
        String u_status = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_STATUS).toString();

        if(u_status.equals("PAID")) {
            return true;
        }

        return false;
    }

    public String getAuthToken_before()
    {
        return PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
    }
}