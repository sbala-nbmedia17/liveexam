package com.nbmedia.vidhvaa.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nbmedia.vidhvaa.Model.DailyQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.OwnFont;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.Utils.RequestSingleton;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModelExamExplainActivity extends BaseActivity implements ServiceResponseListener {

    private Context mContext;

    //OwnFont ownFont;
    ProgressDialog loading;

    public Toolbar toolbar;
    public BottomNavigationView bottomNavigationView;

    CheckBox ch_1, ch_2, ch_3, ch_4;
    TextView textViewQuestionNo,textViewQuestionTitle;
    public int lang_value = 1;

    WebView daily_question, correct_explain, correct_answer;
    WebView ch_text_1, ch_text_2, ch_text_3, ch_text_4;
    Button btn_next, btn_back;
    LinearLayout dy_1, dy_2, dy_3, dy_4;

    String auth_token = "";
    String mq_code = "";
    String mq_flg = "";

    public int q_counter = 0;
    public int q_length = 0;
    public List<DailyQuestion> mQuizQuestions = new ArrayList<>();
    public List<DailyQuestion> mQuizTamilQuestions = new ArrayList<>();

    Bundle bundle = null;

    public float dpHeight = 0;
    public float dpWidth = 0 ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_exam_explain);
        init();
    }

    public void init()
    {
        mContext = ModelExamExplainActivity.this;

        //ownFont = new OwnFont(mContext,ModelExamExplainActivity.this);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }


        bundle = getIntent().getExtras();
        mq_code = bundle.getString("mq_code");
        mq_flg = bundle.getString("mq_flg");

        daily_question = (WebView) findViewById(R.id.tv_explain_question);
        ch_1 = (CheckBox)findViewById(R.id.chk_explain_1);
        ch_2 = (CheckBox)findViewById(R.id.chk_explain_2);
        ch_3 = (CheckBox)findViewById(R.id.chk_explain_3);
        ch_4 = (CheckBox)findViewById(R.id.chk_explain_4);
        correct_answer = (WebView)findViewById(R.id.tv_explain_correct);
        correct_explain = (WebView) findViewById(R.id.tv_explain_detail);
        textViewQuestionTitle = (TextView)findViewById(R.id.tv_explain_question_title);
        textViewQuestionNo= (TextView)findViewById(R.id.tv_explain_question_no);
        dy_1 = (LinearLayout)findViewById(R.id.exp_lay_1);
        dy_2 = (LinearLayout)findViewById(R.id.exp_lay_2);
        dy_3 = (LinearLayout)findViewById(R.id.exp_lay_3);
        dy_4 = (LinearLayout)findViewById(R.id.exp_lay_4);
        btn_next = (Button)findViewById(R.id.btn_explain_next_r);
        btn_back = (Button)findViewById(R.id.btn_explain_back_r);

        ch_text_1 = (WebView) findViewById(R.id.chk_explain_text_1);
        ch_text_2 = (WebView) findViewById(R.id.chk_explain_text_2);
        ch_text_3 = (WebView) findViewById(R.id.chk_explain_text_3);
        ch_text_4 = (WebView) findViewById(R.id.chk_explain_text_4);

        //setFont
        textViewQuestionTitle.setTypeface(mFontInstance.getPoppinBold());
        textViewQuestionNo.setTypeface(mFontInstance.getPoppinBold());


        auth_token = UserDataRepository.getInstance().getAuthToken();
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels;
        dpWidth = displayMetrics.widthPixels ;

        //button
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                q_counter++;
                if(q_counter < q_length)     {
                    setQuestion();
                }
                else {
                    Toast.makeText(getApplicationContext(), "No Questions Available", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(q_counter > 0)
                {
                    q_counter--;
                    if(q_counter < q_length)     {
                        setQuestion();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "No Questions Available", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ch_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_2.setChecked(false);
                    ch_3.setChecked(false);
                    ch_4.setChecked(false);
                }
            }
        });

        ch_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_3.setChecked(false);
                    ch_4.setChecked(false);
                }
            }
        });

        ch_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_2.setChecked(false);
                    ch_4.setChecked(false);
                }
            }
        });

        ch_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_2.setChecked(false);
                    ch_3.setChecked(false);
                }
            }
        });


        if(!auth_token.equals(""))
        {
            if(Constants.isNetworkAvailable(mContext))  {
                if(mq_flg.equals("1")) {
                    sendApi(auth_token, mq_code);
                }
                else if(mq_flg.equals("2")) {
                    sendDailyApi(auth_token, mq_code);
                }
            }
            else  {
                Intent ii = new Intent(getApplicationContext(), NoInternetActivity.class);
                startActivity(ii);
            }
        }

    }

    public void sendApi(String ap_auth_token, String ap_mm_code) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("mq_code", ap_mm_code);


        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        loading = new ProgressDialog(mContext,R.style.full_screen_dialog){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.fill_dialog);
                getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
            }
        };

        loading.setCancelable(false);
        loading.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.USER_MODEL_EXAM_QUESTION, jsonObj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loading.cancel();
                        try {
                            //Toast.makeText(getApplicationContext(), "Flag: " + response.toString(), Toast.LENGTH_SHORT).show();

                            JSONObject jObj = new JSONObject(response.toString());
                            int rest = jObj.getInt("status");

                            if(rest == 200)
                            {
                                JSONArray ja_data = jObj.getJSONArray("questions");
                                int length = ja_data.length();
                                q_length = length;

                                DailyQuestion mDailyModel;
                                int i =0;
                                String dy_code = "-";
                                int flg = 0;

                                for(i=0; i<length; i++)
                                {
                                    JSONObject ob_1 = ja_data.getJSONObject(i);

                                    mDailyModel = new DailyQuestion();

                                    mDailyModel.setQues_id(ob_1.getString("mq_ques_id"));
                                    mDailyModel.setQues_code(mq_code);
                                    mDailyModel.setQuestion_order(ob_1.getString("mq_order"));
                                    mDailyModel.setQuestion(ob_1.getString("mq_question"));
                                    mDailyModel.setAns_1(ob_1.getString("mq_ans_1"));
                                    mDailyModel.setAns_2(ob_1.getString("mq_ans_2"));
                                    mDailyModel.setAns_3(ob_1.getString("mq_ans_3"));
                                    mDailyModel.setAns_4(ob_1.getString("mq_ans_4"));
                                    mDailyModel.setCorrect_answer(ob_1.getString("mq_correct_ans"));
                                    mDailyModel.setExplain(ob_1.getString("mq_explain"));
                                    mDailyModel.setUser_answer(ob_1.getString("mq_user_ans"));
                                    mDailyModel.setLanguage(ob_1.getString("ln_code"));

                                    mQuizQuestions.add(mDailyModel);

                                }

                                setQuestion();
                            }
                            else {

                                Toast.makeText(getApplicationContext(),"No Data Added...",Toast.LENGTH_LONG).show();
                            }

                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.cancel();
                        if(error instanceof AuthFailureError) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    logoutMySession();
                                }
                            });
                        }
                        Toast.makeText(getApplicationContext(), "Error: " + error, Toast.LENGTH_SHORT).show();

                    }
                });

        if (Constants.isNetworkAvailable(this)) {
            RequestSingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        } else {
            getDialog().showInformationDialog("Please Check your internet connection");
        }

    }

    public void sendDailyApi(String ap_auth_token, String ap_dy_code) {
        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("dy_code", ap_dy_code);


        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

    }
      /*loading.setCancelable(false);
        loading.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.USER_DAILY_TASK_QUESTION, jsonObj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loading.cancel();
                        try {
                            //Toast.makeText(getApplicationContext(), "Flag: " + response.toString(), Toast.LENGTH_SHORT).show();

                            JSONObject jObj = new JSONObject(response.toString());
                            int rest = jObj.getInt("status");

                            if(rest == 200)
                            {
                                JSONArray ja_data = jObj.getJSONArray("questions");
                                int length = ja_data.length();
                                q_length = length;

                                DailyQuestion mDailyModel;
                                int i =0;

                                for(i=0; i<length; i++)
                                {
                                    JSONObject ob_1 = ja_data.getJSONObject(i);

                                    mDailyModel = new DailyQuestion();

                                    mDailyModel.setQues_id(ob_1.getString("dy_ques_id"));
                                    mDailyModel.setQues_code(mq_code);
                                    mDailyModel.setQuestion_order(ob_1.getString("dy_order"));
                                    mDailyModel.setQuestion(ob_1.getString("dy_question"));
                                    mDailyModel.setAns_1(ob_1.getString("dy_ans_1"));
                                    mDailyModel.setAns_2(ob_1.getString("dy_ans_2"));
                                    mDailyModel.setAns_3(ob_1.getString("dy_ans_3"));
                                    mDailyModel.setAns_4(ob_1.getString("dy_ans_4"));
                                    mDailyModel.setCorrect_answer(ob_1.getString("dy_correct_ans"));
                                    mDailyModel.setExplain(ob_1.getString("dy_explain"));
                                    mDailyModel.setUser_answer(ob_1.getString("dy_user_ans"));
                                    mDailyModel.setLanguage(ob_1.getString("ln_code"));

                                    mQuizQuestions.add(mDailyModel);

                                }

                                setQuestion();
                            }
                            else {

                                Toast.makeText(getApplicationContext(),"No Data Added...",Toast.LENGTH_LONG).show();
                            }

                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.cancel();
                        Toast.makeText(getApplicationContext(), "Error: " + error, Toast.LENGTH_SHORT).show();

                    }
                });

        if (Constants.isNetworkAvailable(this)) {
            RequestSingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        } else {
            getDialog().showInformationDialog("Please Check your internet connection");
        }*/

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();

        try {
            //Toast.makeText(getApplicationContext(), "Flag: " + response.toString(), Toast.LENGTH_SHORT).show();

            JSONObject jObj = new JSONObject(response.toString());
            int rest = jObj.getInt("status");

            if(rest == 200)
            {
                JSONArray ja_data = jObj.getJSONArray("questions");
                int length = ja_data.length();
                q_length = length;

                DailyQuestion mDailyModel;
                int i =0;

                for(i=0; i<length; i++)
                {
                    JSONObject ob_1 = ja_data.getJSONObject(i);

                    mDailyModel = new DailyQuestion();

                    mDailyModel.setQues_id(ob_1.getString("dy_ques_id"));
                    mDailyModel.setQues_code(mq_code);
                    mDailyModel.setQuestion_order(ob_1.getString("dy_order"));
                    mDailyModel.setQuestion(ob_1.getString("dy_question"));
                    mDailyModel.setAns_1(ob_1.getString("dy_ans_1"));
                    mDailyModel.setAns_2(ob_1.getString("dy_ans_2"));
                    mDailyModel.setAns_3(ob_1.getString("dy_ans_3"));
                    mDailyModel.setAns_4(ob_1.getString("dy_ans_4"));
                    mDailyModel.setCorrect_answer(ob_1.getString("dy_correct_ans"));
                    mDailyModel.setExplain(ob_1.getString("dy_explain"));
                    mDailyModel.setUser_answer(ob_1.getString("dy_user_ans"));
                    mDailyModel.setLanguage(ob_1.getString("ln_code"));

                    mQuizQuestions.add(mDailyModel);

                }

                setQuestion();
            }
            else {

                Toast.makeText(getApplicationContext(),"No Data Added...",Toast.LENGTH_LONG).show();
            }
        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }


    public void setQuestion()
    {
        if(q_length > 0)
        {
            if(q_counter < q_length)
            {
                int x = q_counter + 1;
                textViewQuestionNo.setText(String.valueOf(x) + "/" + String.valueOf(q_length));

                int flg = 0;
                int ans_1 = 0;
                int ans_2 = 0;
                if(mQuizQuestions.get(q_counter).getUser_answer().equals(mQuizQuestions.get(q_counter).getCorrect_answer())) {
                    flg = 1;
                }

                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);

                dy_1.setBackground(ContextCompat.getDrawable(mContext, R.drawable.br_model_answer));
                dy_2.setBackground(ContextCompat.getDrawable(mContext, R.drawable.br_model_answer));
                dy_3.setBackground(ContextCompat.getDrawable(mContext, R.drawable.br_model_answer));
                dy_4.setBackground(ContextCompat.getDrawable(mContext, R.drawable.br_model_answer));

                if(mQuizQuestions.get(q_counter).getUser_answer().equals("1")) {
                    ch_1.setChecked(true);
                    ans_1 = 1;
                    if(flg == 1) {
                        dy_1.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_border));
                        ans_2 =1;
                    }
                    else {
                        dy_1.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_border));
                    }
                }
                else if(mQuizQuestions.get(q_counter).getUser_answer().equals("2")) {
                    ch_2.setChecked(true);
                    ans_1 = 2;
                    if(flg == 1) {
                        dy_2.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_border));
                        ans_2 = 2;
                    }
                    else {
                        dy_2.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_border));
                    }
                }
                else if(mQuizQuestions.get(q_counter).getUser_answer().equals("3")) {
                    ch_3.setChecked(true);
                    ans_1 = 3;
                    if(flg == 1) {
                        dy_3.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_border));
                        ans_2 = 3;
                    }
                    else {
                        dy_3.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_border));
                    }
                }
                else if(mQuizQuestions.get(q_counter).getUser_answer().equals("4")) {
                    ch_4.setChecked(true);
                    ans_1 = 4;
                    if(flg == 1) {
                        dy_4.setBackground(ContextCompat.getDrawable(mContext, R.drawable.green_border));
                        ans_2 = 4;
                    }
                    else {
                        dy_4.setBackground(ContextCompat.getDrawable(mContext, R.drawable.red_border));
                    }
                }

                setFormattedQuestion(mQuizQuestions.get(q_counter).getQuestion(), mQuizQuestions.get(q_counter).getAns_1(),
                        mQuizQuestions.get(q_counter).getAns_2(), mQuizQuestions.get(q_counter).getAns_3(),
                        mQuizQuestions.get(q_counter).getAns_4(), lang_value, mQuizQuestions.get(q_counter).getPattern(),  ans_1, ans_2);

                //prework for correct answer
                correct_answer.getSettings().setJavaScriptEnabled(true);
                correct_answer.getSettings().setLoadWithOverviewMode(true);
                correct_answer.getSettings().setUseWideViewPort(true);

                correct_explain.getSettings().setJavaScriptEnabled(true);
                correct_explain.getSettings().setLoadWithOverviewMode(true);
                correct_explain.getSettings().setUseWideViewPort(true);

                correct_answer.getSettings().setJavaScriptEnabled(true);
                correct_explain.getSettings().setJavaScriptEnabled(true);

                correct_answer.getSettings().setLoadWithOverviewMode(true);
                correct_answer.getSettings().setUseWideViewPort(true);
                correct_explain.getSettings().setLoadWithOverviewMode(true);
                correct_explain.getSettings().setUseWideViewPort(true);

                correct_answer.getSettings().setDefaultFontSize(16);
                correct_explain.getSettings().setDefaultFontSize(16);
                correct_explain.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
                correct_answer.getSettings().setTextSize(WebSettings.TextSize.SMALLER);

                String crrct_answer = "-";
                if(mQuizQuestions.get(q_counter).getCorrect_answer().equals("1")) {
                    crrct_answer = "<b>Correct Answer : A</b>" +mQuizQuestions.get(q_counter).getAns_1();
                }
                else if(mQuizQuestions.get(q_counter).getCorrect_answer().equals("2")) {
                    crrct_answer = "<b>Correct Answer : B</b>" +mQuizQuestions.get(q_counter).getAns_2();
                }
                else if(mQuizQuestions.get(q_counter).getCorrect_answer().equals("3")) {
                    crrct_answer = "<b>Correct Answer : C</b>" +mQuizQuestions.get(q_counter).getAns_3();
                }
                else if(mQuizQuestions.get(q_counter).getCorrect_answer().equals("4")) {
                    crrct_answer = "<b>Correct Answer : D</b>" +mQuizQuestions.get(q_counter).getAns_4();
                }

                String html_string = "<b>Explanation</b><p align='justify'>" + mQuizQuestions.get(q_counter).getExplain() + "<p>";
                correct_explain.loadDataWithBaseURL("file:///android_asset/", setHTML(html_string, "#FAE5D3",flg, dpWidth), "text/html", "utf-8", null);
                correct_answer.loadDataWithBaseURL("file:///android_asset/", setHTML(crrct_answer, "#FAE5D3",flg, dpWidth), "text/html", "utf-8", null);


            }

        }
    }

    public void setFormattedQuestion(String question, String ans_1, String ans_2, String ans_3 , String ans_4, int flg, String pattern, int c_1, int c_2)
    {

        String opt = "";
        Resources res = getResources();

        daily_question.getSettings().setJavaScriptEnabled(true);
        daily_question.getSettings().setLoadWithOverviewMode(true);
        daily_question.getSettings().setUseWideViewPort(true);

        ch_text_1.getSettings().setJavaScriptEnabled(true);
        ch_text_2.getSettings().setJavaScriptEnabled(true);
        ch_text_3.getSettings().setJavaScriptEnabled(true);
        ch_text_4.getSettings().setJavaScriptEnabled(true);

        ch_text_1.getSettings().setLoadWithOverviewMode(true);
        ch_text_1.getSettings().setUseWideViewPort(true);
        ch_text_2.getSettings().setLoadWithOverviewMode(true);
        ch_text_2.getSettings().setUseWideViewPort(true);
        ch_text_3.getSettings().setLoadWithOverviewMode(true);
        ch_text_3.getSettings().setUseWideViewPort(true);
        ch_text_4.getSettings().setLoadWithOverviewMode(true);
        ch_text_4.getSettings().setUseWideViewPort(true);


        if(dpWidth >= 1080) {
            if(flg == 2)
            {
                daily_question.getSettings().setDefaultFontSize(18);
                ch_text_1.getSettings().setDefaultFontSize(15);
                ch_text_2.getSettings().setDefaultFontSize(15);
                ch_text_3.getSettings().setDefaultFontSize(15);
                ch_text_4.getSettings().setDefaultFontSize(15);
            }
            else {
                daily_question.getSettings().setDefaultFontSize(20);
                ch_text_1.getSettings().setDefaultFontSize(17);
                ch_text_2.getSettings().setDefaultFontSize(17);
                ch_text_3.getSettings().setDefaultFontSize(17);
                ch_text_4.getSettings().setDefaultFontSize(17);
            }

            daily_question.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_1.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_2.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_3.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_4.getSettings().setTextSize(WebSettings.TextSize.SMALLER);

        }
        else if(dpWidth <= 1054 && dpWidth >= 720) {
            if(flg == 2)
            {
                daily_question.getSettings().setDefaultFontSize(17);
                ch_text_1.getSettings().setDefaultFontSize(15);
                ch_text_2.getSettings().setDefaultFontSize(15);
                ch_text_3.getSettings().setDefaultFontSize(15);
                ch_text_4.getSettings().setDefaultFontSize(15);
            }
            else {
                daily_question.getSettings().setDefaultFontSize(19);
                ch_text_1.getSettings().setDefaultFontSize(17);
                ch_text_2.getSettings().setDefaultFontSize(17);
                ch_text_3.getSettings().setDefaultFontSize(17);
                ch_text_4.getSettings().setDefaultFontSize(17);
            }

            daily_question.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_1.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_2.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_3.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_4.getSettings().setTextSize(WebSettings.TextSize.SMALLER);

        }

        daily_question.loadDataWithBaseURL("file:///android_asset/", setHTML(question, "#FAE5D3",flg, dpWidth), "text/html", "utf-8", null);

        if(c_1 == 1 && c_2 == 1) {
            ch_text_1.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_1, "#ABEBC6",1, dpWidth), "text/html", "utf-8", null);
        } else if(c_1 == 1 && c_2 != 1) {
            ch_text_1.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_1, "#F5B7B1",1, dpWidth), "text/html", "utf-8", null);
        }
        else {
            ch_text_1.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_1, "#EAEDED",1, dpWidth), "text/html", "utf-8", null);
        }

        if(c_1 == 2 && c_2 == 2) {
            ch_text_2.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_2, "#ABEBC6",1, dpWidth), "text/html", "utf-8", null);
        } else if(c_1 == 2 && c_2 != 2) {
            ch_text_2.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_2, "#F5B7B1",1, dpWidth), "text/html", "utf-8", null);
        }
        else {
            ch_text_2.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_2, "#EAEDED",1, dpWidth), "text/html", "utf-8", null);
        }


        if(c_1 == 3 && c_2 == 3) {
            ch_text_3.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_3, "#ABEBC6",1, dpWidth), "text/html", "utf-8", null);
        } else if(c_1 == 3 && c_2 != 3) {
            ch_text_3.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_3, "#F5B7B1",1, dpWidth), "text/html", "utf-8", null);
        }
        else {
            ch_text_3.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_3, "#EAEDED",1, dpWidth), "text/html", "utf-8", null);
        }


        if(c_1 == 4 && c_2 == 4) {
            ch_text_4.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_4, "#ABEBC6",1, dpWidth), "text/html", "utf-8", null);
        } else if(c_1 == 4 && c_2 != 4) {
            ch_text_4.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_4, "#F5B7B1",1, dpWidth), "text/html", "utf-8", null);
        }
        else {
            ch_text_4.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_4, "#EAEDED",1, dpWidth), "text/html", "utf-8", null);
        }


    }

    public String setHTML(String question, String color, int flg, double height)
    {
        String opt = "";
        String hc = "<!doctype html><html><head>";
        hc = hc + "<meta name='viewport' content='target-densitydpi=device-dpi, width=device-width, initial-scale=1.0' />";
        hc = hc + "<script type='text/javascript' src='es5/tex-mml-chtml.js'></script>";
        hc = hc + "<style>@font-face {" +
                "      font-family: poppins-regular;\n" +
                "      src: url('fonts/Poppins-Regular.ttf');\n" +
                "    }";
        hc = hc + "@font-face {" +
                "      font-family: Bamini;\n" +
                "      src: url('fonts/Bamini.TTF');\n" +
                "    }\n";
        hc = hc + "p{margin-top: 3px !important; margin-bottom: 3px !important;};";
        hc = hc + "</style></head>";
        if(flg == 1)  {
            hc = hc + "<body style ='background-color:"+color+"'; font-family:poppins-regular;line-height: 80%;>" + question + "</body><html>";
        }
        else {
            hc = hc + "<body style ='background-color:"+color+"';line-height: 80%;>" + question + "</body><html>";
        }

        return hc;

    }



    /*@Override
    public void onBackPressed() {
        //Execute your code here
        Intent ii = new Intent(getApplicationContext(), HistoryActivity.class);
        startActivity(ii);
    }*/
}