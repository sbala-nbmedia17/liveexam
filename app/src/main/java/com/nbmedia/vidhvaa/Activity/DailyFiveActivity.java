package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.DailyTaskQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.Utils.PopupDaily;
import com.nbmedia.vidhvaa.Utils.PreciseCountdown;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DailyFiveActivity extends BaseActivity implements ServiceResponseListener {

    public Toolbar toolbar;
    public List<DailyTaskQuestion> mQuizQuestions = new ArrayList<>();
    public CountDownTimer countDownTimer = null;
    public int mQuestionCounter = 0, mQuestionLength = 0, mCounter = 18, mProgressbarValue = 18, mLanguageValue = 1, mLocalCounter;
    public boolean isRunning = false, isLastQuestion = false, isFirstQuestion = true;
    public String mAuthToken = "", mExamId = "-", mLanguageId = "LN02", mStartTime = "2020-10-10 00:00:00";
    android.text.format.DateFormat dateFormat;

    WebView webViewModelExam;
    TextView textViewmCounterNumber, textViewQuestionNumber, textViewLanguage;
    Bundle bundleParameters = null;
    View view_activity = null;
    private ProgressBar progressBarCircle;
    String preTimerHold="", answerFive = "";

    PreciseCountdown countDown;

    long mCurrentStartTime, mCurrentEndTime, mCurrentResultTime;

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        preTimerHold = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_TIMER_HOLD).toString();
        if(preTimerHold!=null && !preTimerHold.isEmpty()){
            long diffInMillisec = System.currentTimeMillis() - Long.parseLong(preTimerHold);

            long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMillisec);
            long diffInHours = TimeUnit.MILLISECONDS.toHours(diffInMillisec);
            long diffInMin = TimeUnit.MILLISECONDS.toMinutes(diffInMillisec);
            long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);
            /*Log.d("Timer-Value","onResume() --> diffInDays = "+
                    diffInDays+" diffInHours = "+diffInHours+" diffInMin = "+diffInMin+" diffInSec = "+diffInSec);*/
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_TIMER_HOLD,""+System.currentTimeMillis());
//        Log.i("Timer-Value", ""+System.currentTimeMillis());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_five);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        view_activity = findViewById(android.R.id.content).getRootView();

        webViewModelExam = findViewById(R.id.webView_dfive_exam);
        textViewmCounterNumber = findViewById(R.id.textview_dfive_counter);
        textViewQuestionNumber = findViewById(R.id.textview_dfive_question_no);
        textViewLanguage = findViewById(R.id.textview_dfive_language);
        progressBarCircle = findViewById(R.id.progressBarCircle_dfive_clock);

        textViewQuestionNumber.setTypeface(mFontInstance.getPoppinBold());
        textViewmCounterNumber.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguage.setTypeface(mFontInstance.getPoppinBold());
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        bundleParameters = getIntent().getBundleExtra("QUESTIONS_PRE");
        mQuizQuestions = (ArrayList<DailyTaskQuestion>) bundleParameters.getSerializable("QUESTIONS_SET");
        mQuestionLength = mQuizQuestions.size();
        mLanguageId = bundleParameters.getString("ln_code");
        mExamId = bundleParameters.getString("dy_code");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

        if (mLanguageId.equals("LN01")) {
            textViewLanguage.setText("Tamil");
            answerFive = "விடை தெரியவில்லை";
        } else if (mLanguageId.equals("LN02")) {
            textViewLanguage.setText("English");
            answerFive = "Answer not known";
        }

        webViewModelExam.getSettings().setJavaScriptEnabled(true);
        webViewModelExam.getSettings().setDomStorageEnabled(true);
        webViewModelExam.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webViewModelExam.addJavascriptInterface(new DailyFiveActivity.WebAppInterface(this), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewModelExam.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewModelExam.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webViewModelExam.loadUrl("file:///android_asset/exam.html");

        dateFormat = new android.text.format.DateFormat();
        mCurrentStartTime = 0;
        mCurrentEndTime = 0;
    }

    public void initListeners()
    {

    }

    public void initController()
    {
        setProgressBarValues();

        if (mQuestionLength > 0) {
            setQuestion();
        }
    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context ctx){
            this.mContext = ctx;
        }

        @JavascriptInterface
        public void getAnswer(String answer) {
            setAnswer(answer);
        }

        @JavascriptInterface
        public String loadFirstData() {
            String arrayData = "";

            if(mQuizQuestions.size() > 0)
            {
                arrayData = getTagRemover(mQuizQuestions.get(0).getmQuestion()) + "*";
                arrayData = arrayData + getTagRemover(mQuizQuestions.get(0).getmAnswerOne()) + "*";
                arrayData = arrayData + getTagRemover(mQuizQuestions.get(0).getmAnswerTwo()) + "*";
                arrayData = arrayData + getTagRemover(mQuizQuestions.get(0).getmAnswerThree()) + "*";
                arrayData = arrayData + getTagRemover(mQuizQuestions.get(0).getmAnswerFour()) + "*";
                arrayData = arrayData + answerFive;
            }
            //Log.i("first_data", arrayData.toString());
            return arrayData;
        }


    }

    public String getTagRemover(String tag)
    {
        return tag.replace("<p>", "");
    }



    public void setQuestion() {
        // THIS IS COMMENT SECTION

        mStartTime = String.valueOf(dateFormat.format("yyyy-MM-dd HH:mm:ss", new java.util.Date()));


        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                int m_seconds = 18;
                if (mQuestionLength > 0) {
                    if (mQuestionCounter < mQuestionLength) {
                        int x = mQuestionCounter + 1;
                        textViewQuestionNumber.setText(x + "/" + mQuestionLength);

                        if (!mQuizQuestions.get(mQuestionCounter).getmQuestionSeconds().equals("")) {
                            m_seconds = Integer.parseInt(mQuizQuestions.get(mQuestionCounter).getmQuestionSeconds());
                        }

                        int cc = mQuestionLength - 1;

                        if (cc == mQuestionCounter) {
                            isLastQuestion = true;
                        }

                        if(isFirstQuestion)
                        {
                            setFormattedQuestion(mQuizQuestions.get(mQuestionCounter).getmQuestion(), mQuizQuestions.get(mQuestionCounter).getmAnswerOne(),
                                    mQuizQuestions.get(mQuestionCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionCounter).getmAnswerThree(),
                                    mQuizQuestions.get(mQuestionCounter).getmAnswerFour(), answerFive);

                            mCounter = m_seconds;
                            mProgressbarValue = m_seconds;
                            setProgressBarValues();
                            setTimer();
                        }
                        else {
                            mCounter = m_seconds;
                            mProgressbarValue = m_seconds;
                            setProgressBarValues();
                            setTimer();

                            setFormattedQuestion(mQuizQuestions.get(mQuestionCounter).getmQuestion(), mQuizQuestions.get(mQuestionCounter).getmAnswerOne(),
                                    mQuizQuestions.get(mQuestionCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionCounter).getmAnswerThree(),
                                    mQuizQuestions.get(mQuestionCounter).getmAnswerFour(), answerFive);
                        }

                    }

                }

            }
        });

    }


    public void setTimer() {
        //mCounter = mCounter + 1;
       /* long milli = mCounter * 1000;
        isRunning = false;
        countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                textViewmCounterNumber.setText(String.valueOf(mCounter));
                progressBarCircle.setProgress(mCounter);
                mCounter--;
                isRunning = true;
                Log.i("count_down_time", ""+ mCounter);
            }

            @Override
            public void onFinish() {
                isRunning = false;

                if (isLastQuestion) {
                    exitExam();
                }

                if (mQuestionLength > 0) {
                    if (mQuestionCounter < mQuestionLength) {

                        String dy_c = mQuizQuestions.get(mQuestionCounter).getmQuestionLanguage();
                        if (dy_c.equals("0")) {
                            mQuizQuestions.get(mQuestionCounter).setmQuestionLanguage(String.valueOf(mLanguageValue));
                        }
                        mQuestionCounter++;
                        setQuestion();
                    }

                }
            }
        }.start();*/


        long milli = mCounter * 1000;
        isRunning = false;
        this.countDown = new PreciseCountdown(milli, 1000, 1000) {
            @Override
            public void onTick(long timeLeft) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(mCounter > 0){
                            textViewmCounterNumber.setText(String.valueOf(mCounter));
                            progressBarCircle.setProgress(mCounter);
                        }

                       // webViewModelExam.loadUrl("javascript:setTimer('" + mCounter + "')");
                        mCounter--;
                        isRunning = true;

                    }
                });

            }

            @Override
            public void onFinished() {
                onTick(0); // when the timer finishes onTick isn't called
                isRunning = false;


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //textViewmCounterNumber.setText("0");
                        progressBarCircle.setProgress(mCounter);
                    }
                });

                if (isLastQuestion) {
                    exitExam();
                }

                if (mQuestionLength > 0) {
                    if (mQuestionCounter < mQuestionLength) {

                        String dy_c = mQuizQuestions.get(mQuestionCounter).getmQuestionLanguage();
                        if (dy_c.equals("0")) {
                            mQuizQuestions.get(mQuestionCounter).setmQuestionLanguage(String.valueOf(mLanguageValue));
                        }
                        mQuestionCounter++;
                        setQuestion();
                    }

                }
            }
        };

        mCurrentStartTime = System.currentTimeMillis();
        this.countDown.start();

    }

    public void setFormattedQuestion(String question, String ans_1, String ans_2, String ans_3, String ans_4, String ans_5) {
        ans_1 = ans_1.replace("<p>", "");
        ans_1 = ans_1.replace("</p>", "");

        ans_2 = ans_2.replace("<p>", "");
        ans_2 = ans_2.replace("</p>", "");

        ans_3 = ans_3.replace("<p>", "");
        ans_3 = ans_3.replace("</p>", "");

        ans_4 = ans_4.replace("<p>", "");
        ans_4 = ans_4.replace("</p>", "");

        if(question.contains("\\"))  {
            question = question.replace("\\", "\\\\");
        }

        if(ans_1.contains("\\")) {
            ans_1 = ans_1.replace("\\", "\\\\");
        }

        if(ans_2.contains("\\")) {
            ans_2 = ans_2.replace("\\", "\\\\");
        }

        if(ans_3.contains("\\")) {
            ans_3 = ans_3.replace("\\", "\\\\");
        }

        if(ans_4.contains("\\")) {
            ans_4 = ans_4.replace("\\", "\\\\");
        }

        if(isFirstQuestion)  {
            isFirstQuestion = false;
        }
        else {
            webViewModelExam.loadUrl("javascript:loadData('" +question+  "', ' " +
                    ans_1+ "', '"+
                    ans_2 + "', '" +
                    ans_3+ "', '" +
                    ans_4 + "')");
        }

        //webViewModelExam.loadDataWithBaseURL("file:///android_asset/", mWebContent, "text/html", "utf-8", null);

    }

    public void setAnswer(String answer) {
        if (mQuestionCounter < mQuestionLength) {
            mQuizQuestions.get(mQuestionCounter).setmUserAnswer(answer);

            //Get Time
            int questionTime = 18;
            if (!mQuizQuestions.get(mQuestionCounter).getmQuestionSeconds().equals("")) {
                questionTime = Integer.parseInt(mQuizQuestions.get(mQuestionCounter).getmQuestionSeconds());
            }
            mCurrentEndTime = System.currentTimeMillis();
            mCurrentResultTime = mCurrentEndTime - mCurrentStartTime;
//            Log.i("Time in Millis", "" + mCurrentResultTime);
            mCurrentStartTime = 0;
            mCurrentEndTime = 0;


            //int answerTime = mLocalCounter;
           // int resultTime = questionTime - answerTime;
            mQuizQuestions.get(mQuestionCounter).setmUserTime(String.valueOf( mCurrentResultTime));
            if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                updateApiCall(mAuthToken, mExamId, mLanguageId,  mQuizQuestions.get(mQuestionCounter).getmQuestionId(), mQuizQuestions.get(mQuestionCounter).getmUserAnswer(), String.valueOf( mCurrentResultTime));
            }
        }
    }

    private void updateApiCall(String ap_mAuthToken, String ap_mDailyTaskId, String ap_mLanguageId, String ap_ques_id, String ap_user_answer, String ap_user_time) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_mAuthToken);
            jsonObj.put("dy_code", ap_mDailyTaskId);
            jsonObj.put("ln_code", ap_mLanguageId);
            jsonObj.put("dy_ques_id", ap_ques_id);
            jsonObj.put("user_answer", ap_user_answer);
            jsonObj.put("user_time", ap_user_time);
            NetworkManager.getInstance().sendJsonObjectPostRequest("ANSWER", Constants.DAILY_TASK_ONE_RESULT, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void setProgressBarValues() {
        progressBarCircle.setMax(mProgressbarValue);
        progressBarCircle.setProgress(mProgressbarValue);
    }

    public void exitExam() {
       if(isRunning) {
           countDown.stop();
           //countDownTimer.cancel();
       }
        Bundle b = new Bundle();
        b.putSerializable("QUESTION_SET", (Serializable) mQuizQuestions);
        b.putString("ln_code", mLanguageId);
        b.putString("dy_code", mExamId);
        b.putString("start_time",  mStartTime);

        Intent ii = new Intent(getApplicationContext(),  DailyTaskSummaryActivity.class);
        ii.putExtra("QUESTIONS", b);
        startActivity(ii);
        finish();
    }


    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();
        int resultStatus = 0;

        try {
            JSONObject jObj = new JSONObject(response.toString());
            resultStatus = jObj.getInt("status");
//            Log.i("update_answer", String.valueOf(resultStatus));
        }
        catch (Exception e)  {
            // showErrorAlertDialog(e.getMessage());
//            Log.i("Exam_error1", e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
//        Log.i("Exam_error", message);
        //showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {

        PopupDaily popUpClass = new PopupDaily(VidhvaaApplication.getAppContext(), DailyFiveActivity.this);
        popUpClass.showPopupWindow(view_activity);

    }
}