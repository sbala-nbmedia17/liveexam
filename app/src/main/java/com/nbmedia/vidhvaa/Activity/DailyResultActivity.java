package com.nbmedia.vidhvaa.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.nbmedia.vidhvaa.Model.DailyTaskQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DailyResultActivity extends BaseActivity implements View.OnClickListener {

    TextView textViewQuestionNo,textViewQuestionTitle;
    WebView webViewResult;
    Button buttonBack, buttonNext;

    public int mQuestionCounter = 0, mQuestionLength = 0;
    public List<DailyTaskQuestion> mQuizQuestions = new ArrayList<>();

    String mExamCode = "", mLanguageCode = "", mAuthToken = "";
    public String resultData = "";

    public float dpHeight = 0;
    public float dpWidth = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_result);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        textViewQuestionNo = (TextView)findViewById(R.id.textview_daily_result_q_no);
        textViewQuestionTitle = (TextView)findViewById(R.id.textview_daily_result_q_title);
        webViewResult = (WebView)findViewById(R.id.webView_daily_result);
        buttonBack = (Button)findViewById(R.id.button_daily_result_back);
        buttonNext = (Button)findViewById(R.id.button_daily_result_next);

    }

    public void initObjects()
    {
        mContext = DailyResultActivity.this;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        webViewResult.getSettings().setJavaScriptEnabled(true);
        webViewResult.getSettings().setDomStorageEnabled(true);
        webViewResult.setOverScrollMode(webViewResult.OVER_SCROLL_NEVER);
        webViewResult.addJavascriptInterface(new DailyResultActivity.WebAppInterface(this), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewResult.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewResult.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webViewResult.loadUrl("file:///android_asset/exam_result.html");

        textViewQuestionNo.setTypeface(FontUtils.getInstance().getPoppinBold());
        textViewQuestionTitle.setTypeface(FontUtils.getInstance().getPoppinBold());

        Bundle args = getIntent().getBundleExtra("QUESTIONS");
        mQuizQuestions = (ArrayList<DailyTaskQuestion>) args.getSerializable("QUESTIONS_LIST");
        mExamCode = args.getString("dy_code");
        mLanguageCode = args.getString("ln_code");

        mQuestionLength = mQuizQuestions.size();

        if(mQuestionLength > 0)  {
            setQuestion();
        }

        mAuthToken = UserDataRepository.getInstance().getAuthToken();

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels;
        dpWidth = displayMetrics.widthPixels ;
    }

    public void initListeners()
    {
        buttonBack.setOnClickListener(this);
        buttonNext.setOnClickListener(this);
       
    }
    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context ctx){
            this.mContext = ctx;
        }

        @JavascriptInterface
        public String firstData() {

            try{
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        if(mQuestionLength > 0)
                        {
                            if(mQuestionCounter < mQuestionLength)
                            {
                                int x = mQuestionCounter + 1;
                                textViewQuestionNo.setText(String.valueOf(x) + "/" + String.valueOf(mQuestionLength));

                                String crrct_answer = "";
                                if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("1")) {
                                    crrct_answer = "<b>Correct Answer : A</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerOne();
                                }
                                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("2")) {
                                    crrct_answer = "<b>Correct Answer : B</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerTwo();
                                }
                                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("3")) {
                                    crrct_answer = "<b>Correct Answer : C</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerThree();
                                }
                                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("4")) {
                                    crrct_answer = "<b>Correct Answer : D</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerFour();
                                }

                                String explanation = "<b>Explanation</b><p align='justify'>" + mQuizQuestions.get(mQuestionCounter).getmExplain() + "<p>";

                                setNewFormattedQuestion(mQuizQuestions.get(mQuestionCounter).getmQuestion(), mQuizQuestions.get(mQuestionCounter).getmAnswerOne(),
                                        mQuizQuestions.get(mQuestionCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionCounter).getmAnswerThree(),
                                        mQuizQuestions.get(mQuestionCounter).getmAnswerFour(),  crrct_answer, explanation, mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer(), mQuizQuestions.get(mQuestionCounter).getmUserAnswer());

                                resultData = mQuizQuestions.get(mQuestionCounter).getmQuestion() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmAnswerOne() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmAnswerTwo() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmAnswerThree() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmAnswerFour() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer() + "*";
                                resultData = resultData +  mQuizQuestions.get(mQuestionCounter).getmUserAnswer() + "*";
                            }
                        }

                    }
                });

            }
            catch(Exception e)
            {
//                Log.i("exeception_error", e.getMessage());
            }


            return resultData;
        }

    }


    public void initController()
    {
        // No Controller Coding

    }

    public void setQuestion()
    {

        if(mQuestionLength > 0)
        {
            if(mQuestionCounter < mQuestionLength)
            {
                int x = mQuestionCounter + 1;
                textViewQuestionNo.setText(String.valueOf(x) + "/" + String.valueOf(mQuestionLength));

                String crrct_answer = "";
                if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("1")) {
                    crrct_answer = "<b>Correct Answer : A</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerOne();
                }
                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("2")) {
                    crrct_answer = "<b>Correct Answer : B</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerTwo();
                }
                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("3")) {
                    crrct_answer = "<b>Correct Answer : C</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerThree();
                }
                else if(mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer().equals("4")) {
                    crrct_answer = "<b>Correct Answer : D</b>" +mQuizQuestions.get(mQuestionCounter).getmAnswerFour();
                }

                String explanation = "<b>Explanation</b><p align='justify'>" + mQuizQuestions.get(mQuestionCounter).getmExplain() + "<p>";

                setNewFormattedQuestion(mQuizQuestions.get(mQuestionCounter).getmQuestion(), mQuizQuestions.get(mQuestionCounter).getmAnswerOne(),
                        mQuizQuestions.get(mQuestionCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionCounter).getmAnswerThree(),
                        mQuizQuestions.get(mQuestionCounter).getmAnswerFour(),  crrct_answer, explanation, mQuizQuestions.get(mQuestionCounter).getmCorrectAnswer(), mQuizQuestions.get(mQuestionCounter).getmUserAnswer());
                checkButtonVisible();
            }

        }
    }

    public void setNewFormattedQuestion(String question, String ans_1, String ans_2, String ans_3 , String ans_4,  String correct, String explain, String correct_answer, String user_answer)
    {
        ans_1 = ans_1.replace("<p>", "");
        ans_1 = ans_1.replace("</p>", "");

        ans_2 = ans_2.replace("<p>", "");
        ans_2 = ans_2.replace("</p>", "");

        ans_3 = ans_3.replace("<p>", "");
        ans_3 = ans_3.replace("</p>", "");

        ans_4 = ans_4.replace("<p>", "");
        ans_4 = ans_4.replace("</p>", "");

       if(question.contains("\\"))  {
            question = question.replace("\\", "\\\\");
        }

        if(ans_1.contains("\\")) {
            ans_1 = ans_1.replace("\\", "\\\\");
        }

        if(ans_2.contains("\\")) {
            ans_2 = ans_2.replace("\\", "\\\\");
        }

        if(ans_3.contains("\\")) {
            ans_3 = ans_3.replace("\\", "\\\\");
        }

        if(ans_4.contains("\\")) {
            ans_4 = ans_4.replace("\\", "\\\\");
        }

        if(explain.contains("\\")) {
            explain = explain.replace("\\", "\\\\");
        }

        String web_content = "";

        web_content = "<!DOCTYPE html>\n" +
                "    <html lang=\"en\">\n" +
                "    <head>\n" +
                "      <title>Vidhvaa</title>\n" +
                "      <meta charset=\"utf-8\">\n" +
                "      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "      <link rel=\"stylesheet\" href=\"css/bootstrap_simple.min.css\">\n" +
                "      <style type = \"text/css\">\n" +
                "        body { background: #fafafa; font-family: Arial, sans-serif; line-height: 1.6; overflow-x:hidden; }\n" +
                "        .card { padding: 10px; max-width: 360px; border:0px; border-radius: 20px; margin: 10px auto; }\n" +
                "        .highlight { font-weight: bold; color: #294; }\n" +
                "        .shadow1 { box-shadow: 0 5px 10px rgba(154,160,185,.05), 0 15px 40px rgba(166,173,201,.2); }\n" +
                "        .shadow2 { box-shadow: 0 7px 30px -10px rgba(150,170,180,0.5); }\n" +
                "        .round { position: relative;\n" +
                "        }\n" +
                "\n" +
                "        .round label {\n" +
                "          background-color: #fff;\n" +
                "          border: 1px solid #ccc;\n" +
                "          border-radius: 50%;\n" +
                "          cursor: pointer;\n" +
                "          height: 20px;\n" +
                "          left: 0;\n" +
                "          position: absolute;\n" +
                "          top: 0;\n" +
                "          width: 20px;\n" +
                "        }\n" +
                "\n" +
                "        .round label:after {\n" +
                "          border: 2px solid #fff;\n" +
                "          border-top: none;\n" +
                "          border-right: none;\n" +
                "          content: \"\";\n" +
                "          height: 4px;\n" +
                "          left: 4px;\n" +
                "          opacity: 0;\n" +
                "          position: absolute;\n" +
                "          top: 6px;\n" +
                "          transform: rotate(-45deg);\n" +
                "          width: 12px;\n" +
                "        }\n" +
                "\n" +
                "        .round input[type=\"checkbox\"] {\n" +
                "          visibility: hidden;\n" +
                "        }\n" +
                "\n" +
                "        .round input[type=\"checkbox\"]:checked + label {\n" +
                "          background-color: #5DADE2;\n" +
                "          border-color: #5DADE2;\n" +
                "        }\n" +
                "\n" +
                "        .round input[type=\"checkbox\"]:checked + label:after {\n" +
                "          opacity: 1;\n" +
                "        }\n" +
                "        * {\n" +
                "           -webkit-user-select: none;\n" +
                "        }\n" +
                "      </style>\n" +
                "      <script type='text/javascript' src='js/tex-mml-chtml.js'></script>  \n" +
                "    </head>\n" +
                "    <body onload='setUserResult(this)'>\n" +
                "   \n" +
                "    <div class='container' >\n" +
                "      <br>\n" +
                "      <div class='row' >\n" +
                "        <div class='col-sm-12'>\n" +
                "          <div class='card shadow2'>\n" +
                "             " + question + "\n" +
                "          </div>\n" +
                "        </div>\n" +
                "      </div>\n" +
                "      <input type = 'hidden'  id = 'id_result' value = '" + correct_answer + "' />\n" +
                "      <input type = 'hidden'  id = 'id_u_result' value = '" + user_answer + "' />\n" +
                "      <div class='row'>\n" +
                "          <div class='col-sm-12'>\n" +
                "              <div class='card shadow2'  onclick='return false;' id= 'id_card_1'>      \n" +
                "                <div class='round'>\n" +
                "                  <input type='checkbox' id='ans_1' onclick='handleClick(this, \"1\");' />\n" +
                "                  <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_1 + "</span>\n" +
                "                </div>\n" +
                "              </div>        \n" +
                "          </div>\n" +
                "      </div>\n" +
                "      <div class='row'>\n" +
                "        <div class='col-sm-12'>\n" +
                "            <div class='card shadow2'  onclick='return false;' id= 'id_card_2'>      \n" +
                "              <div class='round'>\n" +
                "                <input type='checkbox' id='ans_2' onclick='handleClick(this, \"2\");' />\n" +
                "                <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_2 + "</span>\n" +
                "              </div>\n" +
                "            </div>          \n" +
                "        </div>\n" +
                "      </div>\n" +
                "      <div class='row'>\n" +
                "          <div class='col-sm-12'>\n" +
                "             <div class='card shadow2'  onclick='return false;' id= 'id_card_3'>      \n" +
                "                <div class='round'>\n" +
                "                  <input type='checkbox' id='ans_3' onclick='return false;' />\n" +
                "                  <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_3 + "</span>\n" +
                "                </div>\n" +
                "              </div>        \n" +
                "          </div>\n" +
                "      </div>\n" +
                "      <div class='row'>\n" +
                "        <div class='col-sm-12'>\n" +
                "              <div class='card shadow2'  onclick='return false;' id= 'id_card_4'>      \n" +
                "                <div class='round'>\n" +
                "                  <input type='checkbox' id='ans_4' onclick='handleClick(this, \"4\");' />\n" +
                "                  <label for='checkbox'></label> <span style='padding: 8px;'>" + ans_4 + "</span>\n" +
                "                </div>\n" +
                "              </div>        \n" +
                "        </div>\n" +
                "      </div>\n" +
                "      <div class='row'>\n" +
                "        <div class='col-sm-12'>\n" +
                "              <div class='card shadow2' >      \n" +
                "                     <p> " + correct + "</p>" +
                "              </div>        \n" +
                "        </div>\n" +
                "      </div>\n" +
                "      <div class='row'>\n" +
                "        <div class='col-sm-12'>\n" +
                "              <div class='card shadow2' >      \n" +
                "                     <p> " + explain + "</p>" +
                "              </div>        \n" +
                "        </div>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "\n" +
                "   \n" +
                "   \n" +
                "    <script type = 'text/javascript'>\n" +
                "    var flag = 0;\n" +
                "    function setUserResult()\n" +
                "    {\n" +
                "      \n" +
                "      var c_result = document.getElementById('id_result').value;\n" +
                "      var u_result = document.getElementById('id_u_result').value;\n" +
                "      if(u_result != '0') {\n" +
                "        setAnswer(u_result, c_result);\n" +
                "        flag = 1;\n" +
                "      }\n" +
                "     \n" +
                "    }\n" +
                "    function handleClick(cb, ans)\n" +
                "    {\n" +
                " \n" +
                "      if(flag == 0)\n" +
                "      {\n" +
                "        if(cb.checked)\n" +
                "        {\n" +
                "            var result = document.getElementById('id_result').value;\n" +
                "            //Android.getAnswer(ans);\n" +
                "            setAnswer(ans, result);\n" +
                "\n" +
                "          \n" +
                "          flag = 1;\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "\n" +
                "    function handleAnswerClick(ans)\n" +
                "    {\n" +
                "      if(flag == 0)\n" +
                "      {\n" +
                "        //Android.getAnswer(ans);\n" +
                "        var result = document.getElementById('id_result').value;\n" +
                "        setAnswer(ans, result);\n" +
                "        \n" +
                "        flag = 1;\n" +
                "      }\n" +
                "    }\n" +
                "\n" +
                "    function setAnswer(ans, result)\n" +
                "    {\n" +
                "        if(ans == '1')\n" +
                "        {\n" +
                "          document.getElementById('ans_1').checked = true;\n" +
                "          document.getElementById('ans_1').disabled = true;\n" +
                "          document.getElementById('ans_2').disabled = true;\n" +
                "          document.getElementById('ans_3').disabled = true;\n" +
                "          document.getElementById('ans_4').disabled = true;\n" +
                "\n" +
                "          var div = document.getElementById('id_card_1');\n" +
                "          if(ans == result)\n" +
                "          {\n" +
                "            div.style.backgroundColor = '#82E0AA';\n" +
                "          }\n" +
                "          else {\n" +
                "            div.style.backgroundColor = '#F5B7B1';\n" +
                "            setCorrectBackground(result);"+
                "          }\n" +
                "        }\n" +
                "        else if(ans == '2')\n" +
                "        {\n" +
                "          document.getElementById('ans_2').checked = true;\n" +
                "          document.getElementById('ans_1').disabled = true;\n" +
                "          document.getElementById('ans_2').disabled = true;\n" +
                "          document.getElementById('ans_3').disabled = true;\n" +
                "          document.getElementById('ans_4').disabled = true;\n" +
                "\n" +
                "          var div = document.getElementById('id_card_2');\n" +
                "          if(ans == result)\n" +
                "          {\n" +
                "            div.style.backgroundColor = '#82E0AA';\n" +
                "          }\n" +
                "          else {\n" +
                "            div.style.backgroundColor = '#F5B7B1';\n" +
                "            setCorrectBackground(result);"+
                "          }\n" +
                "        }\n" +
                "        else if(ans == '3')\n" +
                "        {\n" +
                "          document.getElementById('ans_3').checked = true;\n" +
                "          document.getElementById('ans_1').disabled = true;\n" +
                "          document.getElementById('ans_2').disabled = true;\n" +
                "          document.getElementById('ans_3').disabled = true;\n" +
                "          document.getElementById('ans_4').disabled = true;\n" +
                "\n" +
                "          var div = document.getElementById('id_card_3');\n" +
                "          if(ans == result)\n" +
                "          {\n" +
                "            div.style.backgroundColor = '#82E0AA';\n" +
                "          }\n" +
                "          else {\n" +
                "            div.style.backgroundColor = '#F5B7B1';\n" +
                "            setCorrectBackground(result);"+
                "          }\n" +
                "        }\n" +
                "        else if(ans == '4')\n" +
                "        {\n" +
                "          document.getElementById('ans_4').checked = true;\n" +
                "          document.getElementById('ans_1').disabled = true;\n" +
                "          document.getElementById('ans_2').disabled = true;\n" +
                "          document.getElementById('ans_3').disabled = true;\n" +
                "          document.getElementById('ans_4').disabled = true;\n" +
                "\n" +
                "          var div = document.getElementById('id_card_4');\n" +
                "          if(ans == result)\n" +
                "          {\n" +
                "            div.style.backgroundColor = '#82E0AA';\n" +
                "          }\n" +
                "          else {\n" +
                "            div.style.backgroundColor = '#F5B7B1';\n" +
                "            setCorrectBackground(result);"+
                "          }\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                " function setCorrectBackground(ans)\n" +
                "    {\n" +
                "      if(ans == '1') {\n" +
                "        document.getElementById('id_card_1').style.backgroundColor = '#82E0AA';\n" +
                "\n" +
                "      }\n" +
                "      else if(ans == '2') {\n" +
                "        document.getElementById('id_card_2').style.backgroundColor = '#82E0AA';\n" +
                "      }\n" +
                "      else if(ans == '3') {\n" +
                "        document.getElementById('id_card_3').style.backgroundColor = '#82E0AA';\n" +
                "      }\n" +
                "      else if(ans == '4') {\n" +
                "        document.getElementById('id_card_4').style.backgroundColor = '#82E0AA';\n" +
                "\n" +
                "      }\n" +
                "    }"+
                "    </script>\n" +
                "   \n" +
                "    </body>\n" +
                "    </html>\n" +
                "\n";


        //Log.i("Result", web_content);

        //webViewResult.loadDataWithBaseURL("file:///android_asset/", web_content, "text/html", "utf-8", null);

        explain = explain.replaceAll("'","\"");

        webViewResult.loadUrl("javascript:loadData('" +question+  "', ' " +
                ans_1+ "', '"+
                ans_2 + "', '" +
                ans_3+ "', '" +
                ans_4 + "', '" +
                correct_answer + "','" +
                user_answer + "', '" +
                explain + "', '" +
                correct + "')");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_daily_result_back:
                if(mQuestionCounter > 0)
                {
                    mQuestionCounter--;
                    if(mQuestionCounter < mQuestionLength)  {
                        setQuestion();
                    }
                }
                checkButtonVisible();
                break;
            case R.id.button_daily_result_next:
                mQuestionCounter++;
                if(mQuestionCounter < mQuestionLength)
                {
                    setQuestion();
                }
                checkButtonVisible();
                break;
        }
    }

    public void checkButtonVisible()
    {
        if(mQuestionCounter == 0)  {
            buttonBack.setVisibility(View.INVISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
        }
        else if(mQuestionCounter == mQuestionLength)
        {
            buttonBack.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.INVISIBLE);
        }
        else {
            buttonBack.setVisibility(View.VISIBLE);
            buttonNext.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        super.onBackPressed();
        Bundle b = new Bundle();
        b.putSerializable("QUESTION_SET", (Serializable) mQuizQuestions);
        b.putString("ln_code", String.valueOf(mLanguageCode));
        b.putString("dy_code", String.valueOf(mExamCode));
        Intent ii = new Intent(getApplicationContext(), DailyTaskSummaryActivity.class);
        ii.putExtra("QUESTIONS", b);
        startActivity(ii);
        finish();
    }
}