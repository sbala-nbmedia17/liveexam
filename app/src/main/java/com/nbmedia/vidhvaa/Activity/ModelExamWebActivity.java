package com.nbmedia.vidhvaa.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.PopupModel;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ModelExamWebActivity extends BaseActivity implements ServiceResponseListener {
    WebView webViewModelExam;
    Bundle bundle = null;
    View view_activity = null;
    String mUrl = "", mAuthToken ="", mLanguageId = "", mExamCode = "-", mExamDate = "-", mExamTime = "-", mCurrentTime = "-", mModelStatus = "", start_time = "2020-10-10 00:00:00";
    public int mCorrectAnswer = 0, mWrongAnswer = 0, mSkippedAnswer = 0, mTotalTimeTaken = 0;
    public List<ModelQuestion> mQuizQuestions = new ArrayList<>();
    Boolean isLoadingScreen = true;
    JSONArray user_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_exam_web);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        view_activity = findViewById(android.R.id.content).getRootView();

        webViewModelExam = findViewById(R.id.webView_model_exam_web);
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        }

        webViewModelExam.getSettings().setJavaScriptEnabled(true);
        webViewModelExam.getSettings().setDomStorageEnabled(true);
        webViewModelExam.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webViewModelExam.addJavascriptInterface(new ModelExamWebActivity.WebAppInterface(this), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewModelExam.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewModelExam.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        bundle = getIntent().getExtras();
        mLanguageId = bundle.getString("ln_code");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

        user_all = new JSONArray();
    }

    public void initListeners()
    {

    }

    public void initController()
    {
        if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext())) {
            getModelDateApiCall(mAuthToken, mLanguageId);
        }

        mUrl  = Constants.HYBRID_DOMAIN_URL + "/an_model_exam?ln_code=" + mLanguageId + "&auth_token=" + mAuthToken;
        webViewModelExam.loadUrl(mUrl);

        //mLoading.setCancelable(false);
       // mLoading.show();
        webViewModelExam.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {
                // do your stuff here
               // mLoading.cancel();
            }
        });


    }

    @Override
    public void onResume() {

        super.onResume();
        if(!isLoadingScreen)
        {
            webViewModelExam.loadUrl("javascript:wakeupCall()");
        }
        //webViewModelExam.loadUrl("javascript:exitExam()");
        //Toast.makeText(getApplicationContext(), "App is Wake up..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        isLoadingScreen = false;
    }


    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context ctx) {
            this.mContext = ctx;
        }

        @JavascriptInterface
        public void getAnswer(String answer) {
            //setAnswer(answer);
        }

        @JavascriptInterface
        public void sendBack(){
            finish();
        }

        @JavascriptInterface
        public void sendUserData(String examCode, String questionId, String userAnswer, String userSeconds){
            updateApiCall(mAuthToken, examCode, questionId, userAnswer, userSeconds);

            for(int x =0; x< mQuizQuestions.size(); x++) {
                if(mQuizQuestions.get(x).getmQuestionId().equals(questionId))
                {
                    mQuizQuestions.get(x).setmUserAnswer(userAnswer);
                    mQuizQuestions.get(x).setmUserTime(userSeconds);
                }
            }
        }

        @JavascriptInterface
        public void sendTotalResult(String correct, String wrong, String not, String totalTime){
            setOverallAnswer(correct, wrong, not, totalTime);
            Toast.makeText(getApplicationContext(), correct + "--" + wrong + "---" + not + "---" + totalTime, Toast.LENGTH_LONG).show();
        }

    }

    private void getModelDateApiCall(String ap_token, String ap_language) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            jsonObj.put("ln_code", ap_language);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_QUESTIONS_GET", Constants.MODEL_EXAM_DATE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateApiCall(String ap_mAuthToken, String ap_mExamId, String ap_ques_id, String ap_user_answer, String ap_user_time) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_mAuthToken);
            jsonObj.put("mq_code", ap_mExamId);
            jsonObj.put("mq_ques_id", ap_ques_id);
            jsonObj.put("user_answer", ap_user_answer);
            jsonObj.put("user_time", ap_user_time);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_SET_ANSWER", Constants.MODEL_EXAM_ONE_RESULT, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void sendResultApiCall(String ap_mAuthToken, String ap_correct, String ap_wrong, String ap_not, String ap_mMqCore, String ap_ln_code, String ap_start, String ap_user_time, JSONArray all_answers) {


        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_mAuthToken);
            jsonObj.put("correct_answer", ap_correct);
            jsonObj.put("wrong_answer", ap_wrong);
            jsonObj.put("not_answer", ap_not);
            jsonObj.put("mq_code", ap_mMqCore);
            jsonObj.put("ln_code", ap_ln_code);
            jsonObj.put("start_time", ap_start);
            jsonObj.put("time_taken", ap_user_time);
            jsonObj.put("all_answer", all_answers);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_OVERALL_RESULT", Constants.MODEL_EXAM_RESULT, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }


    public void setOverallAnswer(String correctAnswer, String wrongAnswer, String skippedAnswer, String totalTime)
    {
        String ques_id = "";
        String user_ans = "";
        String user_time = "0";
        mTotalTimeTaken = 0;

        try
        {
            mCorrectAnswer = Integer.parseInt(correctAnswer);
            mWrongAnswer = Integer.parseInt(wrongAnswer);
            mSkippedAnswer = Integer.parseInt(skippedAnswer);
            mTotalTimeTaken = Integer.parseInt(totalTime);

            for(int x =0; x< mQuizQuestions.size(); x++)
            {
                ques_id = mQuizQuestions.get(x).getmQuestionId();
                user_ans = mQuizQuestions.get(x).getmUserAnswer();
                user_time =mQuizQuestions.get(x).getmUserTime();

                JSONObject obj = new JSONObject();
                obj.put("mq_code", mExamCode);
                obj.put("mq_ques_id", ques_id);
                obj.put("ln_code", mLanguageId);
                obj.put("mq_user_ans", user_ans);
                obj.put("mq_user_time", user_time);

                user_all.put(obj);

            }

            //send results to server
            if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                sendResultApiCall(mAuthToken, String.valueOf(mCorrectAnswer), String.valueOf(mWrongAnswer), String.valueOf(mSkippedAnswer), mExamCode, mLanguageId, start_time, String.valueOf(mTotalTimeTaken), user_all);
            }

        }
        catch(Exception e)
        {
//            Log.i("error_message", e.getMessage());
        }


    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        int resultStatus = 0;
        try {
            if (Tag.equals("MODEL_QUESTIONS_GET")) {
                JSONObject jObj = new JSONObject(response.toString());
                resultStatus = jObj.getInt("status");
                // resultMessage = jObj.getString("message");
                if(resultStatus == 200)
                {
                    mExamCode = jObj.getString("mq_code");
                    mExamDate = jObj.getString("mq_date");
                    mExamTime = jObj.getString("mq_time");
                    mCurrentTime = jObj.getString("cr_date");

                    //Add Question - Tamil
                    JSONArray ja_data = jObj.getJSONArray("questions");
                    int mQuestionLength = ja_data.length();

                    ModelQuestion modelQuestion;
                    int i =0;

                    for(i=0; i<mQuestionLength; i++)
                    {
                        JSONObject ob_1 = ja_data.getJSONObject(i);

                        modelQuestion = new ModelQuestion(ob_1.getString("mq_ques_id"),
                                mExamCode,  "LN01",
                                ob_1.getString("mq_order"),
                                ob_1.getString("mq_pattern"),
                                ob_1.getString("mq_seconds"),
                                ob_1.getString("mq_question"),
                                ob_1.getString("mq_ans_1"),
                                ob_1.getString("mq_ans_2"),
                                ob_1.getString("mq_ans_3"),
                                ob_1.getString("mq_ans_4"),
                                ob_1.getString("mq_correct_ans"),
                                "0",
                                "0")  ;

                        mQuizQuestions.add(modelQuestion);
                    }

                }

            }
            else if (Tag.equals("MODEL_SET_ANSWER")) {
                JSONObject jObj = new JSONObject(response);
                resultStatus = jObj.getInt("status");
                if (resultStatus == 200) {
                    //Toast.makeText(getApplicationContext(), "updated...", Toast.LENGTH_LONG).show();
                    String xx = "200";
                }
            }
            else if (Tag.equals("MODEL_OVERALL_RESULT")) {
                JSONObject jObj = new JSONObject(response);
                resultStatus = jObj.getInt("status");
                if (resultStatus == 200) {
                    //Toast.makeText(getApplicationContext(), "updated...", Toast.LENGTH_LONG).show();
                    String xx = "200";
                }
            }
        } catch (Exception e) {
            //alertDialogs.showErrorInformation("Error: " + e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        if (mLoading.isShowing()) {
            mLoading.cancel();
        }

        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    logoutMySession();
                }
            });
            return;
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBackPressed() {

        webViewModelExam.loadUrl("javascript:exitExam()");

        //PopupModel popUpClass = new PopupModel(VidhvaaApplication.getAppContext(), ModelExamWebActivity.this);
        //popUpClass.showPopupWindow(view_activity);
    }
}