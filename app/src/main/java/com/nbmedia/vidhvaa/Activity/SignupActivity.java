package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Firebase.MyFirebaseMessagingService;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.AlertDialogs;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends BaseActivity implements ServiceResponseListener , View.OnClickListener{

    EditText editTextUserName, editTextUserMobile, editTextUserState,editTextUserEmailId;
    
    Button buttonSubmit;
    TextView textViewTitle, textViewLink, textViewTermsAndCondition;

    CheckBox checkBoxTerms;

    String mUserMobile = "", mUserMailId = "", mFirebaseToken = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        initViews();
        initObjects();
        initListeners();
    }

    public void initViews()
    {
        editTextUserName = (EditText)findViewById(R.id.sg_full_name);
        editTextUserMobile = (EditText)findViewById(R.id.sg_mobile);
        editTextUserState = (EditText)findViewById(R.id.sg_state);
        editTextUserEmailId = (EditText)findViewById(R.id.sg_email);

        buttonSubmit = (Button)findViewById(R.id.sg_btn_submit);
        textViewLink = (TextView)findViewById(R.id.sg_info_link);
        checkBoxTerms = (CheckBox)findViewById(R.id.sg_terms);
        textViewTermsAndCondition = (TextView)findViewById(R.id.tv_forget_password);

        //setFont
        editTextUserName.setTypeface(mFontInstance.getPoppinRegular());
        editTextUserMobile.setTypeface(mFontInstance.getPoppinRegular());
        editTextUserState.setTypeface(mFontInstance.getPoppinRegular());
        editTextUserEmailId.setTypeface(mFontInstance.getPoppinRegular());
        textViewTermsAndCondition.setTypeface(mFontInstance.getPoppinRegular());
        textViewLink.setTypeface(mFontInstance.getPoppinRegular());

    }

    public void initObjects()
    {
        //textViewTitle.setTypeface(mFontInstance.getPoppinBold());
        mFirebaseToken = MyFirebaseMessagingService.getToken(SignupActivity.this);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

    }

    public void initListeners()
    {
        buttonSubmit.setOnClickListener(this);
        textViewLink.setOnClickListener(this);
    }

    
    public boolean checkValidate()
    {
        boolean setResult = true;

        if(editTextUserEmailId.getText().toString().isEmpty())  {
            editTextUserEmailId.requestFocus();
            editTextUserEmailId.setError("Email is blank");
            setResult = false;
        }
        else if(!isValidMail(editTextUserEmailId.getText().toString().trim()))  {
            editTextUserEmailId.requestFocus();
            editTextUserEmailId.setError("Please enter correct email format");
            setResult = false;
        }

        if(editTextUserMobile.getText().toString().isEmpty()) {
            editTextUserMobile.requestFocus();
            editTextUserMobile.setError("Mobile No is blank");
            setResult = false;
        }
        else if(!isValidMobile(editTextUserMobile.getText().toString().trim()))  {
           editTextUserMobile.requestFocus();
           editTextUserMobile.setError("Mobile No should be 10 Digits");
            setResult = false;
        }
        else if(!isValidMobile_1(editTextUserMobile.getText().toString().trim()))  {
            editTextUserMobile.requestFocus();
            editTextUserMobile.setError("Invalid Mobile No");
            setResult = false;
        }

        if(!checkBoxTerms.isChecked()) {
            showErrorAlertDialog("Please Agree terms and Conditions");
            setResult = false;
        }
        return setResult;
    }

    private boolean isValidMail(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }

    private boolean isValidMobile(String phone) {
        return PhoneNumberUtils.isGlobalPhoneNumber(phone);
    }

    private boolean isValidMobile_1(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void sendApi(String ap_phone, String ap_email, String fb_token) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("user_email", ap_email);
            jsonObj.put("user_mobile", ap_phone);
            jsonObj.put("firebase_token", fb_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("SignupActivity", Constants.REGISTER_USER, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();

        try {
            JSONObject jObj = new JSONObject(response.toString());
            int rest = jObj.getInt("status");
            String msg = jObj.getString("message");
            if(rest == 200)
            {
                String auth_token = jObj.getString("auth_token");
                PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN,auth_token);
                jObj.put("user_mobile", "" + mUserMobile);
                jObj.put("user_email", "" + mUserMailId);
                PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, "" + jObj.toString());
                PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_VALIDATE_OTP, "PENDING_OTP");
                PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_STATUS, "PENDING_OTP");

                startActivity(new Intent(getApplicationContext(), SignOtpActivity.class));
            }
            else
            {
                showErrorAlertDialog(msg);
            }
        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.sg_btn_submit:
                if(checkValidate()) {
                    mUserMobile = editTextUserMobile.getText().toString().trim();
                    mUserMailId = editTextUserEmailId.getText().toString().trim();
                    if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                        sendApi( mUserMobile, mUserMailId, mFirebaseToken);
                    }
                    else  {
                        showErrorAlertDialog("Please check your Internet Connection");
                    }
                }
                break;
            case R.id.sg_info_link:
                Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(ii);
                break;
            
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();

    }
}