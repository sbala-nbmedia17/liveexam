package com.nbmedia.vidhvaa.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.AlertDialogs;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.OwnFont;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignOtpActivity extends BaseActivity implements ServiceResponseListener {
    private Context mContext;
    OwnFont ownFont;

    public EditText otp_1, otp_2, otp_3, otp_4;
    Button btn_submit;
    Bundle bundle = null;
    TextView tv_text, tv_resend, textViewOTPBack,textviewOtpVerification, textViewResendOtpTitle;
    LinearLayout resend_panel;

    String auth_token = "-";
    String level = "0";
    String firebase_token = "";
    String user_email = "";
    String user_mobile = "";
    String m_name = "";
    String m_phone = "";
    String m_state = "";
    String m_email = "";

    EditText[] editTexts;
    StringBuffer sb = new StringBuffer();

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    private void initSms() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_otp);

        initViews();
        initObjects();
        initListeners();
        initController();
        resend_panel.setVisibility(View.GONE);
        initSms();
        ticker.start();
    }

    CountDownTimer ticker = new CountDownTimer(30000, 1000) {
        public void onTick(long millisUntilFinished) {
            int seconds = (int) (millisUntilFinished / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;
            tv_text.setText("Mobile number : "+m_phone+" \nOtp Valid till " + String.format("%02d", minutes)
                    + ":" + String.format("%02d", seconds));
        }

        public void onFinish() {
            tv_resend.setEnabled(true);
            tv_text.setText("Mobile Number : "+m_phone);
            resend_panel.setVisibility(View.VISIBLE);
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }



    public void initViews() {
        mContext = SignOtpActivity.this;

        resend_panel = findViewById(R.id.panel_resend);
        tv_text = (TextView) findViewById(R.id.tv_otp_text);
        tv_resend = (TextView) findViewById(R.id.tv_otp_resend);
        textViewOTPBack = (TextView)findViewById(R.id.tv_otp_back);
        textviewOtpVerification = (TextView)findViewById(R.id.sg_sign_otp_title);
        textViewResendOtpTitle = (TextView)findViewById(R.id.tv_info);

        otp_1 = (EditText) findViewById(R.id.ed_otp_text_1);
        otp_2 = (EditText) findViewById(R.id.ed_otp_text_2);
        otp_3 = (EditText) findViewById(R.id.ed_otp_text_3);
        otp_4 = (EditText) findViewById(R.id.ed_otp_text_4);

        btn_submit = (Button) findViewById(R.id.btn_otp_submit);

        //setFont
        tv_text.setTypeface(mFontInstance.getPoppinRegular());
        textviewOtpVerification.setTypeface(mFontInstance.getPoppinBold());
        textViewResendOtpTitle.setTypeface(mFontInstance.getPoppinRegular());
        tv_resend.setTypeface(mFontInstance.getPoppinRegular());
        textViewOTPBack.setTypeface(mFontInstance.getPoppinRegular());
        otp_1.setTypeface(mFontInstance.getPoppinRegular());
        otp_2.setTypeface(mFontInstance.getPoppinRegular());
        otp_3.setTypeface(mFontInstance.getPoppinRegular());
        otp_4.setTypeface(mFontInstance.getPoppinRegular());


        otp_1.requestFocus();

        editTexts = new EditText[]{otp_1, otp_2, otp_3, otp_4};
        otp_1.addTextChangedListener(new PinTextWatcher(0));
        otp_2.addTextChangedListener(new PinTextWatcher(1));
        otp_3.addTextChangedListener(new PinTextWatcher(2));
        otp_4.addTextChangedListener(new PinTextWatcher(3));

        otp_1.setOnKeyListener(new PinOnKeyListener(0));
        otp_2.setOnKeyListener(new PinOnKeyListener(1));
        otp_3.setOnKeyListener(new PinOnKeyListener(2));
        otp_4.setOnKeyListener(new PinOnKeyListener(3));

    }

    public void initObjects() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        m_name = getIntent().getStringExtra("m_name");
        m_phone = UserDataRepository.getInstance().getMobileNumber();
        m_email = UserDataRepository.getInstance().getEmailId();
        m_state = getIntent().getStringExtra("m_state");

        bundle = getIntent().getExtras();
        firebase_token = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN).toString();
        //Toast.makeText(getApplicationContext(),firebase_token,Toast.LENGTH_LONG).show();

        auth_token = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
        level = "0";

    }

    public void initListeners() {

        //otp_1.requestFocus();
        //alertDialogs.showErrorInformation(auth_token);

        otp_1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (otp_1.getText().toString().length() == 1)     //size as per your requirement
                {
                    otp_2.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        otp_2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (otp_2.getText().toString().length() == 1)     //size as per your requirement
                {
                    otp_3.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        otp_3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (otp_3.getText().toString().length() == 1)     //size as per your requirement
                {
                    otp_4.requestFocus();
                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        otp_4.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (otp_4.getText().toString().length() == 1)     //size as per your requirement
                {

                }
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int z = checkValidate();

                if (z == 0) {
                    String otp = otp_1.getText().toString().trim() +
                            otp_2.getText().toString().trim() +
                            otp_3.getText().toString().trim() + otp_4.getText().toString().trim();

                    if (Constants.isNetworkAvailable(mContext)) {
                        sendApiCall(auth_token, otp, level, firebase_token);

                    } else {
                        showErrorAlertDialog("Please Check your Internet Connection");
                    }
                }

                //alertDialogs.showInformation("Welcome");
            }
        });

        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constants.isNetworkAvailable(mContext)) {
                    reSendApiCall(auth_token);
                } else {
                    showErrorAlertDialog("Please Check your Internet Connection");
                }
            }
        });

        textViewOTPBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutMySession();
            }
        });
    }

    public void initController() {
        tv_text.setText("OTP is sent to mobile :" + user_mobile + " and email :" + user_email);
    }

    public int checkValidate() {
        int z = 0;

        if (otp_1.getText().toString().isEmpty()) {
            otp_1.setError("First No is blank");
            z = 1;
        }

        if (otp_2.getText().toString().isEmpty()) {
            otp_2.setError("Second No is blank");
            z = 1;
        }

        if (otp_3.getText().toString().isEmpty()) {
            otp_3.setError("Third No is blank");
            z = 1;
        }

        if (otp_4.getText().toString().isEmpty()) {
            otp_4.setError("Fourth No is blank");
            z = 1;
        }
        return z;
    }

    private void sendApiCall(String ap_auth_token, String ap_otp, String ap_user_level, String ap_firebase_token) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("user_otp", ap_otp);
            jsonObj.put("user_level", ap_user_level);
            jsonObj.put("firebase_token", ap_firebase_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("SEND_OTP", Constants.REGISTER_USER_OTP, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void reSendApiCall(String ap_auth_token ) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("RESEND_OTP", Constants.REGISTER_USER_OTP_RESEND, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private class PinTextWatcher implements TextWatcher {

        private int currentIndex;
        private boolean isFirst = false, isLast = false;
        private String newTypedString = "";

        public PinTextWatcher(int currentIndex) {
            this.currentIndex = currentIndex;

            if (currentIndex == 0)
                this.isFirst = true;
            else if (currentIndex == editTexts.length - 1)
                this.isLast = true;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            newTypedString = s.subSequence(start, start + count).toString().trim();
        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = newTypedString;

            /* Detect paste event and set first char */
            if (text.length() > 1)
                text = String.valueOf(text.charAt(0)); // TODO: We can fill out other EditTexts

            editTexts[currentIndex].removeTextChangedListener(this);
            editTexts[currentIndex].setText(text);
            editTexts[currentIndex].setSelection(text.length());
            editTexts[currentIndex].addTextChangedListener(this);

            if (text.length() == 1)
                moveToNext();
            else if (text.length() == 0)
                moveToPrevious();
        }

        private void moveToPrevious() {
            if (!isFirst)
                editTexts[currentIndex - 1].requestFocus();
        }

        private void moveToNext() {
            if (!isLast)
                editTexts[currentIndex + 1].requestFocus();

            if (isAllEditTextsFilled() && isLast) { // isLast is optional
                editTexts[currentIndex].requestFocus();
                hideKeyboard();
            }
        }

        private void hideKeyboard() {
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }
        }

        private boolean isAllEditTextsFilled() {
            for (EditText editText : editTexts)
                if (editText.getText().toString().trim().length() == 0)
                    return false;
            return true;
        }

    }

    private class PinOnKeyListener implements View.OnKeyListener {
        private int currentIndex;

        public PinOnKeyListener(int currentIndex) {
            this.currentIndex = currentIndex;
        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_DEL && event.getAction() == KeyEvent.ACTION_DOWN) {
                if (editTexts[currentIndex].getText().toString().isEmpty() && currentIndex != 0)
                    editTexts[currentIndex - 1].requestFocus();
            }
            return false;
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();

        try {
            if(Tag.equals("SEND_OTP"))
            {
                JSONObject jObj = new JSONObject(response.toString());
                int rest = jObj.getInt("status");
                String msg = jObj.getString("message");
//                Log.d("Test-Bala", "response : " + response);
                if (rest == 200)
                {
                    String user_type = jObj.getString("user_type");
                    String user_auth = jObj.getString("auth_token");
                    String user_status = "0";

                    jObj.put("user_mobile", "" + m_phone);
                    jObj.put("user_email", "" + m_email);
                    jObj.put("user_status", "" + user_status);

                    //lDb.insertUser(m_name, m_phone, m_email, m_state, user_mpin, user_auth, user_code, user_level, "0");
                    PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_VALIDATE_OTP,"");
                    PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN, user_auth);
                    PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_DATA, "" + jObj.toString());
                    PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_STATUS, user_type);

                    Intent ii = new Intent(getApplicationContext(), DashboardNewActivity.class);
                    startActivity(ii);
                    finish();

                }
                else if (rest == 420) {
                    showErrorAlertDialog(msg);
                }

            }
            else if(Tag.equals("RESEND_OTP"))
            {
                ticker.cancel();
                ticker.start();
            }
        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}