package com.nbmedia.vidhvaa.Activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

public class LoginActivity extends BaseActivity {

    private Context mContext;


    EditText ed_email;
    EditText ed_password;
    Button btn_submit;
    TextView title;
    TextView login_link;

    String st_email = "";
    String st_password = "";
    Class<?> activityClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = LoginActivity.this;

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.login_bottom));
            window.setNavigationBarColor(getResources().getColor(R.color.login_bottom));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        init_components();

    }

    public void init_components()
    {
        title = (TextView)findViewById(R.id.tv_login_title);
        ed_email = (EditText)findViewById(R.id.ed_email_id);
        ed_password = (EditText)findViewById(R.id.ed_password);
        btn_submit = (Button)findViewById(R.id.btn_login_submit);
        login_link = (TextView)findViewById(R.id.tv_info_link);

        title.setTypeface(mFontInstance.getPoppinBold());



        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;

        String dis = dpHeight + "--" + dpWidth;
        String stat = UserDataRepository.getInstance().checkSignupUser();

        if(stat.equals("0"))
        {
            Toast.makeText(getApplicationContext(),"You are not Registered User! Please Signup!",Toast.LENGTH_LONG).show();
            Intent ii = new Intent(getApplicationContext(), SignupActivity.class);
            startActivity(ii);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            int z = checkValidate();

            if(z == 0) {
                st_email = ed_email.getText().toString().trim();
                st_password = ed_password.getText().toString().trim();


                String stat = "1";

                //Toast.makeText(getApplicationContext(), "status :" +stat, Toast.LENGTH_SHORT).show();

                if(stat.equals("1") ) {


                  /*  Intent ii = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(ii);*/

                }
                else if(stat.equals("2") ) {
                    getDialog().showInformationDialog("Pending OTP Verification");
//                    Toast.makeText(getApplicationContext(), "Pending OTP Verification", Toast.LENGTH_SHORT).show();
                }
                else {
                    getDialog().showErrorInformation("Incorrect login data \nPlease try again");
//                    Toast.makeText(getApplicationContext(), "Invalid Login", Toast.LENGTH_SHORT).show();
                }
            }

            }
        });

        login_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(ii);
            }
        });
    }

    public int checkValidate()
    {
        int z = 0;
        if(ed_email.getText().toString().isEmpty())  {
            ed_email.setError("Email is blank");
            z = 1;
        }

        if(ed_password.getText().toString().isEmpty())  {
            ed_password.setError("Password is blank");
            z = 1;
        }
        return z;
    }
}