package com.nbmedia.vidhvaa.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.DailyTaskQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DailyTaskSummaryActivity extends BaseActivity implements ServiceResponseListener {

    private Context mContext;

    public Toolbar toolbar;
    WebView webViewDailySummary;

    TextView textViewTitle;
    Button buttonViewResult, buttonViewRank;

    public int mCorrectAnswer = 0, mWrongAnswer = 0, mSkippedAnswer = 0, mTotalQuestion = 0;
    public List<DailyTaskQuestion> mQuizQuestions = new ArrayList<>();
    public long mTotalTimeTaken = 0;

    String mAuthToken = "", mExamCode = "-", mLanguageId = "LN02", mWebContent = "", mSubjectContent = "", mStartTime = "2020-12-10 00:00:00";
    String mQuestionCode = "", mUserAnswer = "", mUserTime = "";

    JSONArray mAllResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_task_summary);

        initViews();
        initObjects();
        initListener();
        initController();
    }

    public void initViews() {
        toolbar = findViewById(R.id.toolbar_daily_summary);
        webViewDailySummary = findViewById(R.id.webview_daily_summary);
        textViewTitle = (TextView) findViewById(R.id.textview_summary_title);

        buttonViewRank = (Button) findViewById(R.id.btn_daily_rank);
        buttonViewResult = (Button) findViewById(R.id.btn_daily_summary_result);

        textViewTitle.setTypeface(mFontInstance.getPoppinBold());
        buttonViewResult.setTypeface(mFontInstance.getPoppinBold());
    }

    public void initObjects() {
        mContext = DailyTaskSummaryActivity.this;


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        //toolbar
        TextView tv = (TextView) toolbar.getChildAt(0);
        tv.setTypeface(FontUtils.getInstance().getPoppinRegular());
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        webViewDailySummary.getSettings().setJavaScriptEnabled(true);
        webViewDailySummary.getSettings().setDomStorageEnabled(true);
        webViewDailySummary.setOverScrollMode(webViewDailySummary.OVER_SCROLL_NEVER);

        textViewTitle.setText("TEST 2");

        //Get Parameters
        Bundle args = getIntent().getBundleExtra("QUESTIONS");
        mQuizQuestions = (ArrayList<DailyTaskQuestion>) args.getSerializable("QUESTION_SET");
        mExamCode = args.getString("dy_code");
        mLanguageId = args.getString("ln_code");
        mStartTime = args.getString("start_time");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

    }

    public void initListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttonViewResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putSerializable("QUESTIONS_LIST", (Serializable) mQuizQuestions);
                b.putString("ln_code", String.valueOf(mLanguageId));
                b.putString("dy_code", String.valueOf(mExamCode));
                Intent ii = new Intent(getApplicationContext(), DailyResultActivity.class);
                ii.putExtra("QUESTIONS", b);
                startActivity(ii);
            }
        });


        buttonViewRank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent ii = new Intent(getApplicationContext(), DailyTaskRankActivity.class);
                ii.putExtra("dy_code", mExamCode);
                startActivity(ii);
            }
        });
    }

    public void initController() {
        mTotalTimeTaken = 0;
        mAllResult = new JSONArray();

        try {
            for (int x = 0; x < mQuizQuestions.size(); x++) {
                mQuestionCode = mQuizQuestions.get(x).getmQuestionId();
                mUserAnswer = mQuizQuestions.get(x).getmUserAnswer();
                mUserTime = mQuizQuestions.get(x).getmUserTime();

                if (mQuizQuestions.get(x).getmUserAnswer().equals(mQuizQuestions.get(x).getmCorrectAnswer())) {
                    mCorrectAnswer++;
                    mTotalTimeTaken = mTotalTimeTaken + Long.valueOf(mUserTime);
                } else {
                    if (!mQuizQuestions.get(x).getmUserAnswer().equals("0")) {
                        mWrongAnswer++;
                    } else {
                        mSkippedAnswer++;
                    }
                }

                mTotalQuestion++;

                //store individual Answer
                JSONObject obj = new JSONObject();
                obj.put("dy_code", mExamCode);
                obj.put("dy_ques_id", mQuestionCode);
                obj.put("ln_code", mLanguageId);

                obj.put("dy_user_ans", mUserAnswer);
                obj.put("dy_user_time", mUserTime);
                mAllResult.put(obj);
            }

            mSubjectContent = mSubjectContent + "<tr><th>Total Number of Questions</th>";
            mSubjectContent = mSubjectContent + "<td>" + mTotalQuestion + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#58D68D'>&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Total Number of Correct Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mCorrectAnswer + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#E74C3C'>&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Total Number of Wrong Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mWrongAnswer + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#626567'>&nbsp;&nbsp;&nbsp;&nbsp;</span>" +
                    "" +
                    "" +
                    "" +
                    "" +
                    "Total Number of Skipped Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mSkippedAnswer + "</td></tr>";

            setAnswerResult(String.valueOf(mCorrectAnswer), String.valueOf(mWrongAnswer), String.valueOf(mSkippedAnswer));

            if (Constants.isNetworkAvailable(mContext)) {
                updateResultApiCall(mAuthToken, String.valueOf(mCorrectAnswer), String.valueOf(mWrongAnswer), String.valueOf(mSkippedAnswer), mExamCode, mStartTime, mLanguageId, String.valueOf(mTotalTimeTaken), mAllResult);
            }
        } catch (Exception e) {

        }
    }

    private void updateResultApiCall(String ap_mAuthToken, String ap_correct, String ap_wrong, String ap_not, String ap_mExamCode, String ap_start, String ap_mLanguageId, String ap_time_taken, JSONArray all_answers) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_mAuthToken);
            jsonObj.put("correct_answer", ap_correct);
            jsonObj.put("wrong_answer", ap_wrong);
            jsonObj.put("not_answer", ap_not);
            jsonObj.put("time_taken", ap_time_taken);
            jsonObj.put("dy_code", ap_mExamCode);
            jsonObj.put("start_time", ap_start);
            jsonObj.put("ln_code", ap_mLanguageId);
            jsonObj.put("all_answer", all_answers);
            NetworkManager.getInstance().sendJsonObjectPostRequest("DAILY_TASK_RESULT", Constants.DAILY_TASK_RESULT, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void setAnswerResult(String correct, String wrong, String not) {
        mWebContent = "<!DOCTYPE html>\n" +
                "<html lang=\"en\" >\n" +
                "<head>\n" +
                "  <head>\n" +
                "    <title>Vidhvaa</title>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <link rel=\"stylesheet\" href=\"css/bootstrap_simple.min.css\">\n" +
                "    <style type = \"text/css\">\n" +
                "      body { background: #fafafa; font-family: Arial, sans-serif; line-height: 1.6; overflow-x:hidden; }\n" +
                "      .card { padding: 10px; max-width: 360px; border:0px; border-radius: 20px; margin: 10px auto; }\n" +
                "      .highlight { font-weight: bold; color: #294; }\n" +
                "      .shadow1 { box-shadow: 0 5px 10px rgba(154,160,185,.05), 0 15px 40px rgba(166,173,201,.2); }\n" +
                "      .shadow2 { box-shadow: 0 7px 30px -10px rgba(150,170,180,0.5); }\n" +
                "      .btn-circle-blue { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#626567; color:#FFFFFF ; }\n" +
                "      .btn-circle-green { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#58D68D; color:#FFFFFF ; }\n" +
                "      .btn-circle-red { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#E74C3C; color:#FFFFFF ; }\n" +
                "      .row {margin:0; padding:5px;}\n" +
                "      canvas { max-width: 100%; max-height: 100%; }\n" +
                "      .score { padding: 0; font-size: 30px; margin-top: 45px; }\n" +
                "      .badge{ margin-top: 20px; }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body >\n" +
                "<div class='container' >\n" +
                "  <br>\n" +
                "  <div class='row'>\n" +
                "    <div class='col-12 col-xs-12 col-sm-12'>\n" +
                "        <div id='canvas-holder'>\n" +
                "            <canvas id='chart-area' width='300' height='300'></canvas>\n" +
                "          </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "\n" +
                "   <div class='row'>\n" +
                "      <div class='col-md-12'>\n" +
                "         <table class = 'table table-striped'>\n" +
                " " + mSubjectContent +
                "         </table>\n" +
                "      </div>\n" +
                "  </div>\n" +
                " \n" +
                "  \n" +
                "  \n" +
                " \n" +
                "</div>\n" +
                "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>\n" +
                "<script type ='text/javascript' >\n" +
                "var pieData = [\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(correct) + ",\n" +
                "    color: '#58D68D',\n" +
                "    highlight: '#58D68D',\n" +
                "    label: 'Correct Answers' },\n" +
                "\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(wrong) + ",\n" +
                "    color: '#E74C3C',\n" +
                "    highlight: '#E74C3C',\n" +
                "    label: 'Wrong Answers' },\n" +
                "\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(not) + ",\n" +
                "    color: '#626567 ',\n" +
                "    highlight: '#626567 ',\n" +
                "    label: 'Skipped Answers' },\n" +
                "\n" +
                "  ];" +
                "  options = {\n" +
                "  \n" +
                "  segmentShowStroke: true,\n" +
                "  segmentStrokeColor: 'rgba(0,0,0,0)',\n" +
                "  segmentStrokeWidth: 1,\n" +
                "  percentageInnerCutout: 50, // This is 0 for Pie charts\n" +
                "  animationSteps: 30,\n" +
                "  animationEasing: 'none',\n" +
                "  animateRotate: true,\n" +
                "  animateScale: true,\n" +
                "  legendTemplate: \"<ul class=\\\"<%=name.toLowerCase()%>-legend\\\"><% for (var i=0; i<segments.length; i++){%><li><span style=\\\"background-color:<%=segments[i].fillColor%>\\\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>\" };\n" +
                "\n" +
                "\n" +
                "  window.onload = function () {\n" +
                "    var ctx = document.getElementById('chart-area').getContext('2d');\n" +
                "    window.myPie = new Chart(ctx).Pie(pieData, options);\n" +
                "  };\n" +
                "\n" +
                "\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>\n";

        //Log.i("result_data", mWebContent);

        webViewDailySummary.loadDataWithBaseURL("file:///android_asset/", mWebContent, "text/html", "utf-8", null);
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);

        try {
            if (Tag.equals("QUESTION")) {
                mLoading.cancel();
                JSONObject jObj = new JSONObject(response.toString());
//                Log.d("Test-Oppo", "DialyTaskSummaryActivity Response : " + response.toString());

            } else if (Tag.equals("ANSWER")) {
                JSONObject jObj = new JSONObject(response.toString());
                int rest = jObj.getInt("status");
            }
        } catch (Exception e) {
            //Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
//            Log.i("dailY_summary", e.getMessage());
        }

    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    logoutMySession();
                }
            });
            return;
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

//        Log.i("daily_error", message);

        //alertDialogs.showErrorInformation(message);
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        super.onBackPressed();
        Intent ii = new Intent(getApplicationContext(), DashboardNewActivity.class);
        startActivity(ii);
        finish();

    }
}