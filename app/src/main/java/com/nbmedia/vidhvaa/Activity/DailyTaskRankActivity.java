package com.nbmedia.vidhvaa.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nbmedia.vidhvaa.Model.DailyQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.Utils.OwnFont;
import com.nbmedia.vidhvaa.Utils.RequestSingleton;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DailyTaskRankActivity extends BaseActivity {

    private Context mContext;
    ProgressDialog loading;

    public Toolbar toolbar;
    public BottomNavigationView bottomNavigationView;

    String auth_token = "";

    TableLayout tl;
    TableRow tr;
    TextView label,tv_daily_title;

    public int q_counter = 0;
    public int q_length = 0;
    public List<DailyQuestion> mQuizQuestions = new ArrayList<>();

    Bundle bundle = null;
    public String dy_code = "-";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_task_rank);

        initViews();
        initObjects();
        initListener();
        initController();
    }

    public void initViews()
    {
        //ownFont = new OwnFont(mContext, DailyTaskRankActivity.this);

        toolbar = findViewById(R.id.db_daily_rank_bar);


        tl = (TableLayout) findViewById(R.id.rank_table);
        tv_daily_title = (TextView) findViewById(R.id.tv_daily_rank_title);
        tv_daily_title.setTypeface(mFontInstance.getPoppinBold());
    }

    public void initObjects()
    {
        mContext = DailyTaskRankActivity.this;


        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        //toolbar
        TextView tv = (TextView) toolbar.getChildAt(0);
        tv.setTypeface(mFontInstance.getPoppinRegular());
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Get Questions Data Set
        bundle = getIntent().getExtras();
        dy_code = bundle.getString("dy_code");

        auth_token = UserDataRepository.getInstance().getAuthToken();
    }

    public void initListener()
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void initController()
    {
        try{
            addHeader("NO", "NAME", "MARK");
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
//            Log.i("Error Table :" , e.getMessage().toString());
        }

        if(Constants.isNetworkAvailable(mContext))  {
            sendApi(auth_token, dy_code);
        }
        else  {
            getDialog().showErrorInformation("Please check your internet connection");
//            Toast.makeText(getApplicationContext(), "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }



    public void sendApi(String ap_auth_token, String ap_dy_code) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("dy_code", ap_dy_code);


        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        loading = new ProgressDialog(mContext,R.style.full_screen_dialog){
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.fill_dialog);
                getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
            }
        };

        loading.setCancelable(false);
        loading.show();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.DAILY_TASK_RANK, jsonObj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        loading.cancel();
                        try {

                            JSONObject jObj = new JSONObject(response.toString());
                            int rest = jObj.getInt("status");

                            if(rest == 200)
                            {
                                JSONArray ja_data = jObj.getJSONArray("rank_list");
                                int length = ja_data.length();


                                int i =0;
                                for(i=0; i<length; i++)
                                {
                                    JSONObject ob_1 = ja_data.getJSONObject(i);

                                    String no_1 = String.valueOf(i+1);
                                    String name_1 = ob_1.getString("user_name");
                                    String cr_answer = ob_1.getString("correct_answer");
//                                    Log.i("daily_result", name_1 + "--" + cr_answer);
                                    addHeader(no_1, name_1, cr_answer);

                                }


                            }
                            else {

                                Toast.makeText(getApplicationContext(),"No Data Added...",Toast.LENGTH_LONG).show();
                            }

                        }
                        catch (Exception e)
                        {
                            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.cancel();
                        if(error instanceof AuthFailureError) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    logoutMySession();
                                }
                            });
                        }
                        Toast.makeText(getApplicationContext(), "Error: " + error, Toast.LENGTH_SHORT).show();

                    }
                });

        if (Constants.isNetworkAvailable(this)) {
            RequestSingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
        } else {
            getDialog().showInformationDialog("Please Check your internet connection");
        }
    }


    void addHeader(String no, String name, String mark){
        /** Create a TableRow dynamically **/
        tr = new TableRow(this);

        /** Creating a TextView to add to the row **/
        TextView lab = new TextView(this);
        lab.setText(no);
        lab.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,TableRow.LayoutParams.WRAP_CONTENT));
        lab.setPadding(5, 5, 5, 5);
        lab.setBackgroundColor(Color.WHITE);
        LinearLayout Ll = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        Ll.addView(lab,params);
        tr.addView((View)Ll); // Adding textView to tablerow.

        /** Creating a TextView to add to the row **/
        TextView r2 = new TextView(this);
        r2.setText(name);
        r2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        r2.setPadding(5, 5, 5, 5);
        r2.setBackgroundColor(Color.WHITE);
        LinearLayout L2 = new LinearLayout(this);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params2.setMargins(5, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        L2.addView(r2,params2);
        tr.addView((View)L2); // Adding textView to tablerow.

        TextView r3 = new TextView(this);
        r3.setText(mark);
        r3.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        r3.setPadding(5, 5, 5, 5);
        r3.setBackgroundColor(Color.WHITE);
        LinearLayout L3 = new LinearLayout(this);
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params3.setMargins(5, 5, 5, 5);
        //Ll.setPadding(10, 5, 5, 5);
        L3.addView(r3,params3);
        tr.addView((View)L3); // Adding textView to tablerow.



        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                TableLayout.LayoutParams.FILL_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
       finish();
    }
}