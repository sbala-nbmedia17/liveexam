package com.nbmedia.vidhvaa.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.VideoView;

import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Firebase.MyFirebaseMessagingService;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;


public class SplashScreenActivity extends BaseActivity implements ServiceResponseListener {

    private Context mContext;
    Handler handler;
    VideoView videoview;

    String mAuthToken = "";

    private void validateToken() {
        try{
            JSONObject jsonToken= new JSONObject();
            jsonToken.put("auth_token",""+mAuthToken);
            NetworkManager.getInstance().sendJsonObjectPostRequest("ValidateToken",Constants.VALIDATE_AUTH_TOKEN,this,jsonToken);
        }catch (Exception e) {e.printStackTrace();}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        hideSystemUi(SplashScreenActivity.class);
        super.onCreate(savedInstanceState);
        new RequestFirebaseToken().execute();
        setContentView(R.layout.activity_home);
        initViews();
        initObjects();
        runHadler();
    }

    private void sendApiCall(String ap_auth_token) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("USER_DETAILS_UPDATE", Constants.REGISTER_USER_UPDATE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d(Tag,"SplashScreenActivity : "+response);
        try {
            if(Tag.equalsIgnoreCase("ValidateToken")){
                JSONObject jsonObject = new JSONObject(response);
                if(jsonObject.get("isValid").toString().equalsIgnoreCase("false")){
                    new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            // Fresh Entry
                            startActivity(new Intent(SplashScreenActivity.this,SignupActivity.class));
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                finishAffinity();
                            }
                        }
                    }, 4050);
                }
            }
            else if(Tag.equals("USER_DETAILS_UPDATE")){
                JSONObject jObj = new JSONObject(response);
                int rest = jObj.getInt("status");
                if(rest == 200)
                {
                    String m_name = jObj.getString("user_name");
                    String m_password = jObj.getString("user_pwd");
                    String user_code = jObj.getString("user_code");
                    UserDataRepository.getInstance().updateUserName(m_name);
                    UserDataRepository.getInstance().updateUserType("PAID");
                    UserDataRepository.getInstance().updateUserMPIN(m_password);
                    UserDataRepository.getInstance().updateUserCode(user_code);
                    PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_USER_STATUS, "PAID");
                }
            }
        }
        catch(Exception e)
        {

        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {
    }

    private class RequestFirebaseToken extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            loadFireBaseToaken();
            return null;
        }
    }

    private void initVideo() {
        videoview.setVideoURI(Uri.parse(Constants.getSplashVideo(this)));
        videoview.start();
    }

    public void initViews() {
        mContext = SplashScreenActivity.this;
        videoview = (VideoView) findViewById(R.id.video_view);
    }

    private Intent getDecidedIntent(int query){
        Intent intent = new Intent();
        switch (query){
            case 0:
                intent.setClass(this,SignupActivity.class);
                break;
            case 1:
                /*intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                if(!PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.EXAM_PREF).toString().isEmpty()){
                    // Please validate the resuming the exam activity over here
                    intent.setClass(this,TempOneActivity.class);
                }else{
                    intent.setClass(this,DashboardActivity.class);
                }*/
                intent.setClass(this,DashboardNewActivity.class);
                break;
            case 2:
                intent.setClass(this,SignOtpActivity.class);
                break;
            case 3:

                if(Constants.isNetworkAvailable(mContext)) {
                    sendApiCall(mAuthToken);
                }
                else {
                    intent.setClass(this, NoInternetActivity.class);
                    return intent;
                }

                intent.setClass(this,DashboardNewActivity.class);
                break;
        }

        if(PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString().isEmpty()){
            intent.setClass(this,SignupActivity.class);
        } else {
            if (PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_VALIDATE_OTP)
                    .toString().equalsIgnoreCase("PENDING_OTP")){
                intent.setClass(this,SignOtpActivity.class);
            }
        }
        return intent;
    }

    public void initObjects() {
        mAuthToken = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
        initVideo();

    }

    public void runHadler() {
        String stat = UserDataRepository.getInstance().checkSignupUser();
        int querystat = 0;
        if(!stat.equals("")) {
            querystat= Integer.parseInt(stat);

        }

        final Intent intent=getDecidedIntent(querystat);
        intent.setFlags(FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        validateToken();
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                // Fresh Entry
                startActivity(intent);
                finish();

            }
        }, 4050);
    }

    private void loadFireBaseToaken(){
        if(PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN).toString().isEmpty()) {
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN, MyFirebaseMessagingService.getToken(this));
        }
//        Log.d("Test-Bala","updateJson : "+PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_FIRE_TOKEN));
    }

}