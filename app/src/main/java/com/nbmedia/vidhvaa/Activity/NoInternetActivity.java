package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.OwnFont;

public class NoInternetActivity extends BaseActivity {

    Button btn;
    TextView tv_title;
    private Context mContext;
    //OwnFont ownFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.white));
            window.setNavigationBarColor(getResources().getColor(R.color.border));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        //mContext =NoInternetActivity.this;
        //ownFont = new OwnFont(mContext, NoInternetActivity.this);

        btn = (Button)findViewById(R.id.ng_btn_submit);
        tv_title =(TextView)findViewById(R.id.ng_tv_title);

        tv_title.setTypeface(mFontInstance.getPoppinRegular());


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent ii = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(ii);*/
            }
        });
    }
}