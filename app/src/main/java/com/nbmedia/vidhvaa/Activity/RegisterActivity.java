package com.nbmedia.vidhvaa.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

public class RegisterActivity extends BaseActivity implements View.OnClickListener{

    Button buttonRegister;

    String mAuthToken = "", mUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
        initObjects();
        initListeners();
    }

    public void initViews()
    {
        buttonRegister = (Button)findViewById(R.id.re_btn_submit);
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.register_top));
            window.setNavigationBarColor(getResources().getColor(R.color.register_top));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        mAuthToken = UserDataRepository.getInstance().getAuthToken();
        mUrl = Constants.EXTERNAL_PAYMENT_URL + "?code=" + mAuthToken;
    }

    public void initListeners()
    {
        buttonRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.re_btn_submit:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(mUrl));
                String title = "Please Select Browser (Chrome is Preferred Browser)";
                Intent chooser = Intent.createChooser(intent, title);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(chooser);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
       finish();
    }
}