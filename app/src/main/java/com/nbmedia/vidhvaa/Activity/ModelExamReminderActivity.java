package com.nbmedia.vidhvaa.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModelExamReminderActivity extends BaseActivity implements View.OnClickListener, ServiceResponseListener {

    public Toolbar toolbar;

    TextView textViewExamTitle, textViewCounter, textViewLanguage;
    
    LinearLayout mo_lay_counter;

    String mAuthToken = "", mExamCode = "-", mExamDate = "-", mExamTime = "-", mCurrentTime = "-", mLanguageId = "LN02", mModelStatus = "";
    public int counter = 18, mCounterSeconds = 0;
    public CountDownTimer countDownTimer = null;
    public boolean isRunning = false;
    public Bundle bundleData = null;

    public List<ModelQuestion> mQuizQuestions = new ArrayList<>();
    Bundle bundle = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_exam_reminder);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        textViewExamTitle = (TextView)findViewById(R.id.tv_mo_rem_title);
        textViewCounter = (TextView)findViewById(R.id.tv_mo_rem_counter);
        textViewLanguage = (TextView)findViewById(R.id.tv_mo_language);

        mo_lay_counter = (LinearLayout)findViewById(R.id.lay_mo_rem_bottom);

        toolbar = findViewById(R.id.db_model_pre_bar);

        textViewExamTitle.setTypeface(mFontInstance.getPoppinBold());
        textViewCounter.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguage.setTypeface(mFontInstance.getPoppinRegular());

    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        textViewExamTitle.setTypeface(FontUtils.getInstance().getPoppinRegular());
        textViewCounter.setTypeface(FontUtils.getInstance().getPoppinRegular());

        bundle = getIntent().getExtras();
        mLanguageId = bundle.getString("ln_code");

        mModelStatus = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_MODEL_EXAM_STATUS).toString();

        if(mModelStatus.equals("ACTIVE")) {
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_MODEL_EXAM_STATUS, "NONE");
        }
    }

    public void initListeners()
    {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBackScreen();
            }
        });

    }

    public void initController()
    {
        //Toast.makeText(getApplicationContext(), "Width : " + String.valueOf(dpWidth) + "Height :" + String.valueOf(dpHeight), Toast.LENGTH_SHORT).show();
        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();;

        if(mLanguageId.equals("LN01")) {
            textViewLanguage.setText("Language : TAMIL");
        }
        else if(mLanguageId.equals("LN02")) {
            textViewLanguage.setText("Language : ENGLISH");
        }

        if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext())) {
            getModelDateApiCall(mAuthToken, mLanguageId);
        }
        else {
            Intent ii = new Intent(getApplicationContext(), NoInternetActivity.class);
            startActivity(ii);
            getBackScreen();
        }
    }

    public void startExam()
    {
        countDownTimer.cancel();
//        Log.i("language_code", mLanguageId);
        Bundle b = new Bundle();
        b.putSerializable("QUESTIONS_SET",(Serializable)mQuizQuestions);
        b.putString("ln_code", mLanguageId);
        b.putString("mm_code", mExamCode);
        Intent ii = new Intent(VidhvaaApplication.getAppContext(), ModelExamActivity.class);
        ii.putExtra("QUESTIONS_PRE",b);
        this.startActivity(ii);
        this.finish();
       }

    private void getModelDateApiCall(String ap_token, String ap_language) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            jsonObj.put("ln_code", ap_language);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_QUESTIONS_GET", Constants.MODEL_EXAM_DATE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void setTimer(long seconds)
    {
        long ms = seconds * 1000;

        countDownTimer = new CountDownTimer(ms,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isRunning = true;
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

                textViewCounter.setText("" + String.format("%02d", hours)
                        + ":" + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            @Override
            public void onFinish() {
                isRunning = false;
                Toast.makeText(getApplicationContext(), "Start Exam", Toast.LENGTH_SHORT).show();
                startExam();
            }
        }.start();
    }

    public void getBackScreen()
    {
        if(isRunning) {
            countDownTimer.cancel();
        }
        finish();
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId()){
            case  R.id.lay_model_condt_tamil:

                break;
            case R.id.lay_model_condt_english:

                break;
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();
        int resultStatus = 0;
        int seconds = 0;
        String resultMessage = "-";

        try {
            JSONObject jObj = new JSONObject(response.toString());
//            Log.i("message", response.toString());
            resultStatus = jObj.getInt("status");
           // resultMessage = jObj.getString("message");
            if(resultStatus == 200)
            {
                mExamCode = jObj.getString("mq_code");
                mExamDate = jObj.getString("mq_date");
                mExamTime = jObj.getString("mq_time");
                mCurrentTime = jObj.getString("cr_date");
                mCounterSeconds = jObj.getInt("seconds");

                if(mCounterSeconds > 0)
                {
                    setTimer(mCounterSeconds);

                    JSONArray ja_data = jObj.getJSONArray("questions");
                    int mQuestionLength = ja_data.length();

                    ModelQuestion modelQuestion;
                    int i =0;

                    for(i=0; i<mQuestionLength; i++)
                    {
                        JSONObject ob_1 = ja_data.getJSONObject(i);

                        modelQuestion = new ModelQuestion(ob_1.getString("mq_ques_id"),
                                mExamCode,  "LN01",
                                ob_1.getString("mq_order"),
                                ob_1.getString("mq_pattern"),
                                ob_1.getString("mq_seconds"),
                                ob_1.getString("mq_question"),
                                ob_1.getString("mq_ans_1"),
                                ob_1.getString("mq_ans_2"),
                                ob_1.getString("mq_ans_3"),
                                ob_1.getString("mq_ans_4"),
                                ob_1.getString("mq_correct_ans"),
                                "0",
                                "0")  ;

                        mQuizQuestions.add(modelQuestion);
                    }
                }
                else
                {
                    showSuccessAlertDialog("Exam date will be announced soon");
                }
                //Add Question - Tamil
            }
            else
            {
                //showErrorAlertDialog(resultMessage);
                showSuccessAlertDialog("Exam date will be announced soon");
            }
        }
        catch (Exception e)  {
           // showErrorAlertDialog(e.getMessage());
//            Log.i("Exam_error1", e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
//        Log.i("Exam_error", message);
        //showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
       getBackScreen();

    }


}