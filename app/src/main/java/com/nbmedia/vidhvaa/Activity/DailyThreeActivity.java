package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONObject;

public class DailyThreeActivity extends BaseActivity implements ServiceResponseListener, View.OnClickListener {

    TextView textViewTitle,textViewForgetPassword;
    Toolbar toolbar;
    Button buttonSubmit;
    CheckBox checkBoxTermsConditions;
    Bundle bundle = null;
    LinearLayout panelEnglish, panelTamil;

    String mAuthToken = "", mLanguageId = "";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_three);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        textViewTitle = (TextView)findViewById(R.id.textview_dthree_title);
        toolbar = (Toolbar)findViewById(R.id.toolbar_dthree_bar);
        textViewForgetPassword = (TextView)findViewById(R.id.tv_forget_password);
        buttonSubmit = (Button)findViewById(R.id.button_dthree_submit);
        checkBoxTermsConditions = (CheckBox)findViewById(R.id.checkbox_dthree_terms);
        panelTamil = (LinearLayout)findViewById(R.id.lay_daily_condt_tamil);
        panelEnglish = (LinearLayout)findViewById(R.id.lay_daily_condt_english);

        textViewForgetPassword.setTypeface(mFontInstance.getPoppinRegular());
        textViewTitle.setTypeface(mFontInstance.getPoppinBold());
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

        bundle = getIntent().getExtras();
        mLanguageId = bundle.getString("ln_code");
    }

    public void initListeners()
    {
        buttonSubmit.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void initController()
    {
        if(mLanguageId.equals("LN01")) {
            panelTamil.setVisibility(View.VISIBLE);
            panelEnglish.setVisibility(View.GONE);
        }
        else if(mLanguageId.equals("LN02")) {
            panelTamil.setVisibility(View.GONE);
            panelEnglish.setVisibility(View.VISIBLE);
        }
    }

    public boolean checkValidate()
    {
        boolean setResult = true;

        if(!checkBoxTermsConditions.isChecked()) {
            setResult = false;
        }
        return setResult;
    }



    @Override
    public void onClick(View v) {
        Intent intent;
        String stat;
        switch (v.getId()) {
            case R.id.button_dthree_submit:
                if(checkValidate()) {
                    intent =new Intent(getApplicationContext(), DailyFourActivity.class);
                    intent.putExtra("ln_code", mLanguageId);
                    startActivity(intent);
                    finish();
                }
                else {
                    showErrorAlertDialog("Please Accept Terms and Conditions");
                }
                break;
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();

        try {
            JSONObject jObj = new JSONObject(response.toString());
            int rest = jObj.getInt("status");
            String msg = jObj.getString("message");
            if(rest == 200)
            {

            }
            else
            {
                showErrorAlertDialog(msg);
            }
        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        finish();
    }
}