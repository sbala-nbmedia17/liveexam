package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

import static android.view.View.GONE;

public class DailyOneActivity extends BaseActivity implements ServiceResponseListener, View.OnClickListener {

    TextView textViewTitle;
    Toolbar toolbar;
    EditText editTextRegisterNo;
    Button buttonSubmit;
    RelativeLayout panelModel;

    String mExamCode = "", mRegisterStatus = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_one);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        textViewTitle = (TextView)findViewById(R.id.textview_done_title);
        toolbar = (Toolbar)findViewById(R.id.toolbar_done_bar);
        editTextRegisterNo = (EditText)findViewById(R.id.edittext_done_register_no);
        buttonSubmit = (Button)findViewById(R.id.button_done_submit);
        panelModel = (RelativeLayout)findViewById(R.id.relative_done);

        textViewTitle.setTypeface(mFontInstance.getPoppinBold());
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        textViewTitle.setTypeface(FontUtils.getInstance().getPoppinRegular());
        buttonSubmit.setTypeface(FontUtils.getInstance().getPoppinRegular());

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();
        panelModel.setVisibility(GONE);

    }

    public void initListeners()
    {
        buttonSubmit.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBackScreen(); finish();
            }
        });
    }

    public void initController()
    {
        if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
            getCodeApiCall(mAuthToken);
        }
        else  {
            showErrorAlertDialog("Please check your Internet Connection");
        }
    }

    public boolean checkValidate()
    {
        boolean setResult = true;

        if(editTextRegisterNo.getText().toString().isEmpty())  {
            editTextRegisterNo.requestFocus();
            editTextRegisterNo.setError("Register No is blank");
            setResult = false;
        }

        return setResult;
    }

    private void getCodeApiCall(String ap_token) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("DAILY_GET_CODE", Constants.DAILY_TASK_REGISTER_CODE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkRegisterNoCall(String ap_token, String ap_exam_code, String ap_register_no) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            jsonObj.put("exam_code", ap_exam_code);
            jsonObj.put("register_no", ap_register_no);
            NetworkManager.getInstance().sendJsonObjectPostRequest("CHECK_REGISTER_NO", Constants.TEMP_CHECK_REG_NO, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void getBackScreen()
    {
        finish();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_done_submit:
                if(checkValidate()) {

                    if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                        checkRegisterNoCall(mAuthToken, mExamCode, editTextRegisterNo.getText().toString().trim() );
                    }
                    else  {
                        showErrorAlertDialog("Please check your Internet Connection");
                    }
                }
                break;
                
        }
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();
        JSONObject jsonObject = null;
        String mLocalFiveMins = "0";
        int resultStatus = 0;

        try {
            if(Tag.equals("DAILY_GET_CODE"))
            {
                jsonObject = new JSONObject(response);
                resultStatus = jsonObject.getInt("status");

                if(resultStatus == 200)
                {
                    mExamCode = jsonObject.getString("dy_code");
                    if(!mExamCode.equals("-")) {
                        mRegisterStatus = jsonObject.getString("register_status");

                        if(mRegisterStatus.equals("1")) {
                            panelModel.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            startActivity(new Intent(getApplicationContext(), DailyTwoActivity.class));
                            finish();
                        }
                    }
                    else {
                        showSuccessAlertDialog("Exam will be Announced Soon!");
                    }

                }
                else
                {
                    showSuccessAlertDialog("Exam will be Announced Soon!");
                }
            }
            else if(Tag.equals("CHECK_REGISTER_NO"))
            {
                jsonObject = new JSONObject(response.toString());
                resultStatus = jsonObject.getInt("status");
                if(resultStatus == 200) {
                    startActivity(new Intent(getApplicationContext(), DailyTwoActivity.class));
                    finish();
                }
                else  if(resultStatus == 301) {
                    showErrorAlertDialog("Invalid Register No");
                }
                else  if(resultStatus == 302) {
                    startActivity(new Intent(getApplicationContext(), DailyTwoActivity.class));
                    finish();
                }
            }

        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
//            Log.i("Error", e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        getBackScreen();
    }
}