package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.SimpleDateFormat;

import static android.view.View.GONE;

public class ModelExamOneActivity extends BaseActivity implements ServiceResponseListener, View.OnClickListener {

    TextView textViewTitle, textViewTimer, textViewInstruction;
    Toolbar toolbar;
    EditText editTextRegisterNo;
    Button buttonSubmit;
    RelativeLayout  panelModel;
    Boolean isRunning = false;

    String mAuthToken = "", mExamCode = "", mRegisterStatus = "0";
    public CountDownTimer countDownTimer = null;
    public int mTimeDifference = 0;
   

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_exam_one);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        textViewTitle = (TextView)findViewById(R.id.textview_mone_title);
        toolbar = (Toolbar)findViewById(R.id.toolbar_mone_bar);
        editTextRegisterNo = (EditText)findViewById(R.id.edittext_mone_register_no);
        buttonSubmit = (Button)findViewById(R.id.button_mone_submit);
        panelModel = (RelativeLayout)findViewById(R.id.relative_mone);
        textViewTimer = (TextView)findViewById(R.id.textview_mone_timer);
        textViewInstruction = (TextView)findViewById(R.id.textview_mone_instruction);

        //setFont
        textViewTitle.setTypeface(mFontInstance.getPoppinBold());
        editTextRegisterNo.setTypeface(mFontInstance.getPoppinRegular());
        buttonSubmit.setTypeface(mFontInstance.getPoppinBold());
        textViewTimer.setTypeface(mFontInstance.getPoppinBold());
        textViewInstruction.setTypeface(mFontInstance.getPoppinRegular());
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
           // window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();
        panelModel.setVisibility(GONE);
        textViewTimer.setVisibility(GONE);
        textViewInstruction.setVisibility(GONE);
    }

    public void initListeners()
    {
        buttonSubmit.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBackScreen(); finish();
            }
        });
    }

    public void initController()
    {
        if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
            getCodeApiCall(mAuthToken);
        }
        else  {
            showErrorAlertDialog("Please check your Internet Connection");
        }
    }

    public boolean checkValidate()
    {
        boolean setResult = true;

        if(editTextRegisterNo.getText().toString().isEmpty())  {
            editTextRegisterNo.requestFocus();
            editTextRegisterNo.setError("User Name is blank");
            setResult = false;
        }

        return setResult;
    }

    private void getCodeApiCall(String ap_token) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_GET_CODE", Constants.MODEL_EXAM_CODE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkRegisterNoCall(String ap_token, String ap_exam_code, String ap_register_no) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            jsonObj.put("exam_code", ap_exam_code);
            jsonObj.put("register_no", ap_register_no);
            NetworkManager.getInstance().sendJsonObjectPostRequest("CHECK_REGISTER_NO", Constants.TEMP_CHECK_REG_NO, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
    
    public void getBackScreen()
    {
        if(isRunning) {
            countDownTimer.cancel();
        }
        finish();
    }



    @Override
    public void onClick(View v) {
       
        switch (v.getId()) {
            case R.id.button_mone_submit:
                if(checkValidate()) {

                    if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                        checkRegisterNoCall(mAuthToken, mExamCode, editTextRegisterNo.getText().toString().trim() );
                    }
                    else  {
                        showErrorAlertDialog("Please check your Internet Connection");
                    }
                }
                break;


        }
    }

    public void setTimer(long seconds)
    {
        long ms = seconds * 1000;
        countDownTimer = new CountDownTimer(ms,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

                textViewTimer.setText("" + String.format("%02d", hours)
                        + ":" + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
                isRunning = true;
            }

            @Override
            public void onFinish() {
                isRunning = false;
                if(mRegisterStatus.equals("1")) {
                    panelModel.setVisibility(View.VISIBLE);
                    textViewTimer.setVisibility(GONE);
                    textViewInstruction.setVisibility(GONE);
                }
                else if(mRegisterStatus.equals("2")) {
                    startActivity(new Intent(getApplicationContext(), ModelExamTwoActivity.class));
                    countDownTimer.cancel();
                    finish();
                    
                }

            }
        }.start();
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();
        JSONObject jsonObject = null;
        String mLocalFiveMins = "0";
        int resultStatus = 0;

        try {
            if(Tag.equals("MODEL_GET_CODE"))
            {

                jsonObject = new JSONObject(response);
                resultStatus = jsonObject.getInt("status");

                if(resultStatus == 200)
                {
                    mExamCode = jsonObject.getString("mq_code");
                    mRegisterStatus = jsonObject.getString("register_status");

                    if(mRegisterStatus.equals("1")) {
                        panelModel.setVisibility(View.VISIBLE);

                        mTimeDifference = Integer.parseInt(jsonObject.getString("differenceInSeconds"));
                        mLocalFiveMins = jsonObject.getString("five_minutes");
                        if (mLocalFiveMins.equals("1")) {
                            //Less than 5 minutes go
                            if (mRegisterStatus.equals("1")) {
                                panelModel.setVisibility(View.VISIBLE);
                            } else if (mRegisterStatus.equals("2")) {
                                startActivity(new Intent(getApplicationContext(), ModelExamTwoActivity.class));
                                finish();
                            }
                        } else
                            {
                            if (mTimeDifference > 0) {
                                setTimer(mTimeDifference);
                                textViewTimer.setVisibility(View.VISIBLE);
                                textViewInstruction.setVisibility(View.VISIBLE);
                                panelModel.setVisibility(GONE);
                            }
                        }
                    }
                    else
                    {
                        startActivity(new Intent(getApplicationContext(), ModelExamTwoActivity.class));
                        finish();
                    }
                }
                else
                {
                    showSuccessAlertDialog("Exam Date will be Announced Soon!");
                }
            }
            else if(Tag.equals("CHECK_REGISTER_NO"))
            {
                jsonObject = new JSONObject(response.toString());
                resultStatus = jsonObject.getInt("status");
                if(resultStatus == 200) {
                    startActivity(new Intent(getApplicationContext(), ModelExamTwoActivity.class));

                    finish();
                }
                else  if(resultStatus == 301) {
                    showErrorAlertDialog("Invalid Register No");
                }
                else  if(resultStatus == 302) {
                    startActivity(new Intent(getApplicationContext(), ModelExamTwoActivity.class));
                    if(isRunning) {
                        countDownTimer.cancel();
                    }
                    finish();
                }

            }

        }
        catch (Exception e)  {
            showErrorAlertDialog(e.getMessage());
//            Log.i("Error", e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        getBackScreen();
        
    }
}