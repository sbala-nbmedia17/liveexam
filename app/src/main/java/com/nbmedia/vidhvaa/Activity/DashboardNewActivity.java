package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.VidhvaaApplication;

public class DashboardNewActivity extends BaseActivity implements View.OnClickListener{

    Button buttonTestOne, buttonTestTwo;
    boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_new);

        initViews();
        initObject();
        initListeners();
        initController();

    }

    public void initViews()
    {
        buttonTestOne = (Button)findViewById(R.id.button_exam_one);
        buttonTestTwo = (Button)findViewById(R.id.button_exam_two);
    }

    public void initObject()
    {
        callYellowTopWhiteBottomColor();

        doubleBackToExitPressedOnce = false;
    }

    public void initListeners()
    {
       buttonTestOne.setOnClickListener(this);
       buttonTestTwo.setOnClickListener(this);
    }

    public void initController()
    {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_exam_one:
                startActivity(new Intent(getApplicationContext(), ModelExamOneActivity.class));
                break;
            case R.id.button_exam_two:
                startActivity(new Intent(getApplicationContext(), DailyOneActivity.class));
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        if (doubleBackToExitPressedOnce) {
            finish();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}