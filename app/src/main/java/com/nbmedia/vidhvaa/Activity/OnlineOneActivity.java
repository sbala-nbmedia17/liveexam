package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.DailyTaskQuestion;
import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.FontUtils;
import com.nbmedia.vidhvaa.Utils.PopupOnline;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class OnlineOneActivity extends BaseActivity implements ServiceResponseListener {

    Toolbar toolbar;
    TextView textViewExamTitle, textViewCounter, textViewLanguage;

    String mAuthToken = "", mExamCode = "-", mExamDate = "-", mExamTime = "-", mCurrentTime = "-", mLanguageId = "LN02", mModelStatus = "";
    public int counter = 18, mCounterSeconds = 0;
    public CountDownTimer countDownTimer = null;
    public boolean isRunning = false;
    public Bundle bundleData = null;
    
    RelativeLayout panelQuestion;
    LinearLayout panelQuestionTimer, panelTimer;

    public List<ModelQuestion> mQuizQuestions = new ArrayList<>();
    Bundle bundle = null;
    
    //Exam vairables
    WebView webViewModelExam;
    TextView textViewmCounterNumber, textViewQuestionNumber, textViewLanguage1, textViewQuestion, textViewLangage;
    private ProgressBar progressBarCircle;

    public int mQuestionmCounter = 0, mQuestionLength = 0, mCounter = 18, mProgressbarValue = 18, mLanguageValue = 1, mUserAnswerTime = 0, mLocalCounter;
    public String mExamId = "-", start_time = "2020-10-10 00:00:00";
    public boolean isLastQuestion = false, isFirstQuestion = true, isExamStart = false;
    View view_activity = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_one);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        
        textViewCounter = (TextView)findViewById(R.id.textview_ofour_counter);
        textViewLanguage =  (TextView)findViewById(R.id.textview_ofour_language);
        panelTimer = (LinearLayout) findViewById(R.id.linear_online_counter);

        panelQuestion = (RelativeLayout)findViewById(R.id.relative_online_question);
        panelQuestionTimer = (LinearLayout)findViewById(R.id.linear_online_top_timer);
        webViewModelExam = findViewById(R.id.webView_online_exam);
        progressBarCircle = findViewById(R.id.progressBarCircle_online_clock);
        view_activity = findViewById(android.R.id.content).getRootView();

        textViewmCounterNumber = findViewById(R.id.textview_online_counter);
        textViewQuestionNumber = findViewById(R.id.textview_online_question_no);
        textViewLanguage = findViewById(R.id.db_model_switch_title);
        textViewQuestion = findViewById(R.id.textview_online_question_title);
        textViewLanguage1 = findViewById(R.id.textview_online_switch_content);

        //setFont

        textViewQuestion.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguage.setTypeface(mFontInstance.getPoppinBold());
        textViewmCounterNumber.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguage1.setTypeface(mFontInstance.getPoppinBold());
        textViewQuestionNumber.setTypeface(mFontInstance.getPoppinBold());

    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }


        textViewCounter.setTypeface(FontUtils.getInstance().getPoppinRegular());

        webViewModelExam.getSettings().setJavaScriptEnabled(true);
        webViewModelExam.getSettings().setDomStorageEnabled(true);
        webViewModelExam.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webViewModelExam.addJavascriptInterface(new OnlineOneActivity.WebAppInterface(this), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewModelExam.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewModelExam.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webViewModelExam.loadUrl("file:///android_asset/liveexam.html");

        mLanguageId = "LN01";

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();
        
        panelTimer.setVisibility(View.VISIBLE);
        panelQuestion.setVisibility(GONE);
        panelQuestionTimer.setVisibility(GONE);
    }

    public void initListeners()
    {

    }

    public void initController()
    {
        if(mLanguageId.equals("LN01")) {
            textViewLanguage.setText("Language : TAMIL");
            textViewLanguage1.setText("TAMIL");
        }
        else if(mLanguageId.equals("LN02")) {
            textViewLanguage.setText("Language : ENGLISH");
            textViewLanguage1.setText("ENGLISH");
        }

        if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext())) {
            getModelDateApiCall(mAuthToken, mLanguageId);
        }
        else {
            Intent ii = new Intent(getApplicationContext(), NoInternetActivity.class);
            startActivity(ii);
            getBackScreen();
        }
    }

    private void getModelDateApiCall(String ap_token, String ap_language) {

        mLoading.setCancelable(false);
        mLoading.show();

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_token);
            jsonObj.put("ln_code", ap_language);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_QUESTIONS_GET", Constants.MODEL_EXAM_DATE, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void getBackScreen()
    {
        if(isRunning) {
            countDownTimer.cancel();
        }
        finish();
    }

    public void startExam()
    {
        //countDownTimer.cancel();
       /* Log.i("language_code", mLanguageId);
        Bundle b = new Bundle();
        b.putSerializable("QUESTIONS_SET",(Serializable)mQuizQuestions);
        b.putString("ln_code", mLanguageId);
        b.putString("dy_code", mExamCode);
        Intent ii = new Intent(VidhvaaApplication.getAppContext(), OnlineTwoActivity.class);
        ii.putExtra("QUESTIONS_PRE",b);
        this.startActivity(ii);
        this.finish();*/
        
        panelTimer.setVisibility(GONE);

        panelQuestionTimer.setVisibility(View.VISIBLE);
        isRunning = false;
        isLastQuestion = false;
        isFirstQuestion = true;
        mQuestionLength = mQuizQuestions.size();
        setProgressBarValues();

        if (mQuestionLength > 0) {
            setQuestion();
            isExamStart = true;
        }
    }

    public void setTimer(long seconds)
    {
        long ms = seconds * 1000;

        countDownTimer = new CountDownTimer(ms,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                isRunning = true;
                int seconds = (int) (millisUntilFinished / 1000);

                int hours = seconds / (60 * 60);
                int tempMint = (seconds - (hours * 60 * 60));
                int minutes = tempMint / 60;
                seconds = tempMint - (minutes * 60);

                textViewCounter.setText("" + String.format("%02d", hours)
                        + ":" + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds));
            }

            @Override
            public void onFinish() {
                isRunning = false;
                Toast.makeText(getApplicationContext(), "Start Exam", Toast.LENGTH_SHORT).show();
                startExam();

            }
        }.start();
    }

    //Question Part starts
    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context ctx) {
            this.mContext = ctx;
        }

        @JavascriptInterface
        public void getAnswer(String answer) {
            setAnswer(answer);
        }

    }

    public void setAnswer(String answer) {
        if (mQuestionmCounter < mQuestionLength) {
            mQuizQuestions.get(mQuestionmCounter).setmUserAnswer(answer);

            //Get Time
            int questionTime = 18;
            if (!mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds().equals("")) {
                questionTime = Integer.parseInt(mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds());
            }

            int answerTime = mLocalCounter;
            int resultTime = questionTime - answerTime;
            mQuizQuestions.get(mQuestionmCounter).setmUserTime(String.valueOf(resultTime));
            /*if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                updateApiCall(mAuthToken, mExamId, mQuizQuestions.get(mQuestionmCounter).getmQuestionId(), mQuizQuestions.get(mQuestionmCounter).getmUserAnswer(), String.valueOf(resultTime));
            }*/
        }
    }

    public void setQuestion() {
        // THIS IS COMMENT SECTION
        int m_seconds = 18;

        if (mQuestionLength > 0) {
            if (mQuestionmCounter < mQuestionLength) {
                int x = mQuestionmCounter + 1;
                textViewQuestionNumber.setText(x + "/" + mQuestionLength);

                if (!mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds().equals("")) {
                    m_seconds = Integer.parseInt(mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds());
                }

                int cc = mQuestionLength - 1;

                if (cc == mQuestionmCounter) {
                    isLastQuestion = true;
                }

                if(isFirstQuestion)
                {
                    setFormattedQuestion(mQuizQuestions.get(mQuestionmCounter).getmQuestion(), mQuizQuestions.get(mQuestionmCounter).getmAnswerOne(),
                            mQuizQuestions.get(mQuestionmCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionmCounter).getmAnswerThree(),
                            mQuizQuestions.get(mQuestionmCounter).getmAnswerFour());

                    mCounter = m_seconds;
                    mProgressbarValue = m_seconds;
                    setProgressBarValues();
                    setQuestionTimer();
                }
                else {
                    mCounter = m_seconds;
                    mProgressbarValue = m_seconds;
                    setProgressBarValues();
                    setQuestionTimer();

                    setFormattedQuestion(mQuizQuestions.get(mQuestionmCounter).getmQuestion(), mQuizQuestions.get(mQuestionmCounter).getmAnswerOne(),
                            mQuizQuestions.get(mQuestionmCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionmCounter).getmAnswerThree(),
                            mQuizQuestions.get(mQuestionmCounter).getmAnswerFour());
                }

            }

        }

    }

    public void setFormattedQuestion(String question, String ans_1, String ans_2, String ans_3, String ans_4) {
        ans_1 = ans_1.replace("<p>", "");
        ans_1 = ans_1.replace("</p>", "");

        ans_2 = ans_2.replace("<p>", "");
        ans_2 = ans_2.replace("</p>", "");

        ans_3 = ans_3.replace("<p>", "");
        ans_3 = ans_3.replace("</p>", "");

        ans_4 = ans_4.replace("<p>", "");
        ans_4 = ans_4.replace("</p>", "");


        if(isFirstQuestion)
        {
            final String qust = question;
            final String ans1 = ans_1;
            final String ans2 = ans_2;
            final String ans3 = ans_3;
            final String ans4 = ans_4;

            webViewModelExam.loadUrl("javascript:loadData('" +question+  "', ' " +
                    ans_1+ "', '"+
                    ans_2 + "', '" +
                    ans_3+ "', '" +
                    ans_4 + "')");

            isFirstQuestion = false;
        }
        else {
            webViewModelExam.loadUrl("javascript:loadData('" +question+  "', ' " +
                    ans_1+ "', '"+
                    ans_2 + "', '" +
                    ans_3+ "', '" +
                    ans_4 + "')");
        }

        //webViewModelExam.loadDataWithBaseURL("file:///android_asset/", mWebContent, "text/html", "utf-8", null);

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void setQuestionTimer() {
        //mCounter = 6;
        mLocalCounter = mCounter + 1;
        mCounter = mCounter + 2;
        long milli = mCounter * 1000;
        isRunning = false;
        countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mCounter--;
                mLocalCounter--;
                isRunning = true;
                progressBarCircle.setProgress(mLocalCounter);
                if(mLocalCounter >= 0){
                    textViewmCounterNumber.setText(String.valueOf(mLocalCounter));
                }
            }

            @Override
            public void onFinish() {
                isRunning = false;
                textViewmCounterNumber.setText("0");
                if (isLastQuestion) {
                    exitExam();
                }

                if (mQuestionLength > 0) {
                    if (mQuestionmCounter < mQuestionLength) {

                        String dy_c = mQuizQuestions.get(mQuestionmCounter).getmQuestionLanguage();
                        if (dy_c.equals("0")) {
                            mQuizQuestions.get(mQuestionmCounter).setmQuestionLanguage(String.valueOf(mLanguageValue));
                        }
                        mQuestionmCounter++;
                        setQuestion();
                    }

                }
            }
        }.start();
    }

    private void setProgressBarValues() {
        progressBarCircle.setMax(mProgressbarValue);
        progressBarCircle.setProgress(mProgressbarValue);
    }

    public void exitExam() {

        if(isRunning) {
            countDownTimer.cancel();
        }
        Bundle b = new Bundle();
        b.putSerializable("QUESTION_SET", (Serializable) mQuizQuestions);
        b.putString("ln_code", mLanguageId);
        b.putString("mm_code", mExamId);
        b.putString("start_time", start_time);
        Intent ii = new Intent(getApplicationContext(), OnlineThreeActivity.class);
        ii.putExtra("QUESTIONS", b);
        startActivity(ii);
        finish();
    }



    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);
        mLoading.cancel();
        int resultStatus = 0;
        int seconds = 0;
        String resultMessage = "-";

        try {
            JSONObject jObj = new JSONObject(response.toString());
            resultStatus = jObj.getInt("status");
            // resultMessage = jObj.getString("message");
            if(resultStatus == 200)
            {
                mExamCode = jObj.getString("mq_code");
                mExamDate = jObj.getString("mq_date");
                mExamTime = jObj.getString("mq_time");
                mCurrentTime = jObj.getString("cr_date");
                mCounterSeconds = jObj.getInt("seconds");

                if(mCounterSeconds > 0)  {
                    setTimer(mCounterSeconds);
                    panelQuestion.setVisibility(View.VISIBLE);
                }
                else {
                    showSuccessAlertDialog("Exam Date will be announced soon!");
                }

                //Add Question - Tamil
                JSONArray ja_data = jObj.getJSONArray("questions");
                int mQuestionLength = ja_data.length();

                ModelQuestion modelQuestion;
                int i =0;

                for(i=0; i<mQuestionLength; i++)
                {
                    JSONObject ob_1 = ja_data.getJSONObject(i);

                    modelQuestion = new ModelQuestion(ob_1.getString("mq_ques_id"),
                            mExamCode,  "LN01",
                            ob_1.getString("mq_order"),
                            ob_1.getString("mq_pattern"),
                            ob_1.getString("mq_seconds"),
                            ob_1.getString("mq_question"),
                            ob_1.getString("mq_ans_1"),
                            ob_1.getString("mq_ans_2"),
                            ob_1.getString("mq_ans_3"),
                            ob_1.getString("mq_ans_4"),
                            ob_1.getString("mq_correct_ans"),
                            "0",
                            "0")  ;

                    mQuizQuestions.add(modelQuestion);
                }

            }
            else
            {
                //showErrorAlertDialog(resultMessage);
                showSuccessAlertDialog("Exam Date will be announced soon!");
            }
        }
        catch (Exception e)  {
            // showErrorAlertDialog(e.getMessage());
//            Log.i("Exam_error1", e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
//        Log.i("Exam_error", message);
        //showErrorAlertDialog(message);
    }

    @Override
    public void onBackPressed() {

        if(isExamStart) {
            PopupOnline popUpClass = new PopupOnline(VidhvaaApplication.getAppContext(), OnlineOneActivity.this);
            popUpClass.showPopupWindow(view_activity);
        }
        else {
            getBackScreen();
        }


    }
}