package com.nbmedia.vidhvaa.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.PopupModel;
import com.nbmedia.vidhvaa.Utils.PreciseCountdown;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;
import com.nbmedia.vidhvaa.services.NetworkManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ModelExamActivity extends BaseActivity implements ServiceResponseListener {

    public Toolbar toolbar;
    public List<ModelQuestion> mQuizQuestions = new ArrayList<>();
    public CountDownTimer countDownTimer = null;
    public int mQuestionmCounter = 0, mQuestionLength = 0, mCounter = 18, mProgressbarValue = 18, mLanguageValue = 1, mUserAnswerTime = 0, mLocalCounter;
    public boolean isRunning = false, isLastQuestion = false, isFirstQuestion = true;
    public String mAuthToken = "", mExamId = "-", mLanguageId = "LN02", start_time = "2020-10-10 00:00:00", mModelStatus = "", answerFive = "";

    WebView webViewModelExam;
    TextView textViewmCounterNumber, textViewQuestionNumber, textViewLanguage, textViewQuestionTitle ,textViewLanguageTitle;
    Bundle bundle = null;
    View view_activity = null;
    private ProgressBar progressBarCircle;
    PreciseCountdown countDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_model_exam);

        initViews();
        initObjects();
        initController();
    }

    public void initViews() {

        view_activity = findViewById(android.R.id.content).getRootView();

        webViewModelExam = findViewById(R.id.webView_model_exam);
        textViewmCounterNumber = findViewById(R.id.tv_model_counter);
        textViewQuestionNumber = findViewById(R.id.db_mo_question_no);
        textViewQuestionTitle = findViewById(R.id.db_mo_question);
        textViewLanguage = findViewById(R.id.db_model_switch_content);
        textViewLanguageTitle = findViewById(R.id.db_model_switch_title);
        progressBarCircle = findViewById(R.id.progressBarCircle_daily_clock);

        //setFont
        textViewQuestionTitle.setTypeface(mFontInstance.getPoppinBold());
        textViewQuestionNumber.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguageTitle.setTypeface(mFontInstance.getPoppinBold());
        textViewLanguage.setTypeface(mFontInstance.getPoppinBold());
        textViewmCounterNumber.setTypeface(mFontInstance.getPoppinBold());

    }

    public void initObjects() {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
            //AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        }

        Bundle args = getIntent().getBundleExtra("QUESTIONS_PRE");
        mQuizQuestions = (ArrayList<ModelQuestion>) args.getSerializable("QUESTIONS_SET");
        mQuestionLength = mQuizQuestions.size();
        mLanguageId = args.getString("ln_code");
        mExamId = args.getString("mm_code");

        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

        mModelStatus = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_MODEL_EXAM_STATUS).toString();

        if(mModelStatus.equals("NONE")) {
            PreferenceUtils.getInstance().setPreferenceContent(PreferenceUtils.PREF_MODEL_EXAM_STATUS, "ACTIVE");
        }
        else  if(mModelStatus.equals("ACTIVE")) {
            showErrorAlertDialog("Sorry! Something is happened!");
//            startActivity(new Intent(getApplicationContext(), DashboardActivity.class));
            finish();
        }

        if (mLanguageId.equals("LN01")) {
            textViewLanguage.setText("Tamil");
            answerFive = "விடை தெரியவில்லை";
        } else if (mLanguageId.equals("LN02")) {
            textViewLanguage.setText("English");
            answerFive = "Answer not known";
        }

        webViewModelExam.getSettings().setJavaScriptEnabled(true);
        webViewModelExam.getSettings().setDomStorageEnabled(true);
        webViewModelExam.setOverScrollMode(View.OVER_SCROLL_NEVER);
        webViewModelExam.addJavascriptInterface(new ModelExamActivity.WebAppInterface(this), "Android");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webViewModelExam.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webViewModelExam.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        webViewModelExam.loadUrl("file:///android_asset/exam.html");


       /* android.provider.Settings.System.putInt(getContentResolver(),
                Settings.System.SCREEN_OFF_TIMEOUT, 1800000);*/
    }


    public void initController() {
       setProgressBarValues();

        if (mQuestionLength > 0) {
            setQuestion();
        }

    }

    private void updateApiCall(String ap_mAuthToken, String ap_mExamId, String ap_ques_id, String ap_user_answer, String ap_user_time) {

        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_mAuthToken);
            jsonObj.put("mq_code", ap_mExamId);
            jsonObj.put("mq_ques_id", ap_ques_id);
            jsonObj.put("user_answer", ap_user_answer);
            jsonObj.put("user_time", ap_user_time);
            NetworkManager.getInstance().sendJsonObjectPostRequest("MODEL_SET_ANSWER", Constants.MODEL_EXAM_ONE_RESULT, this, jsonObj);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void setQuestion() {
        // THIS IS COMMENT SECTION
        int m_seconds = 18;

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                int m_seconds = 18;
                if (mQuestionLength > 0) {
                    if (mQuestionmCounter < mQuestionLength) {
                        int x = mQuestionmCounter + 1;
                        textViewQuestionNumber.setText(x + "/" + mQuestionLength);

                        if (!mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds().equals("")) {
                            m_seconds = Integer.parseInt(mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds());
                        }

                        int cc = mQuestionLength - 1;

                        if (cc == mQuestionmCounter) {
                            isLastQuestion = true;
                        }

                        if(isFirstQuestion)
                        {
                            setFormattedQuestion(mQuizQuestions.get(mQuestionmCounter).getmQuestion(), mQuizQuestions.get(mQuestionmCounter).getmAnswerOne(),
                                    mQuizQuestions.get(mQuestionmCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionmCounter).getmAnswerThree(),
                                    mQuizQuestions.get(mQuestionmCounter).getmAnswerFour(), answerFive);

                            mCounter = m_seconds;
                            mProgressbarValue = m_seconds;
                            setProgressBarValues();
                            setTimer();
                        }
                        else {
                            mCounter = m_seconds;
                            mProgressbarValue = m_seconds;
                            setProgressBarValues();
                            setTimer();

                            setFormattedQuestion(mQuizQuestions.get(mQuestionmCounter).getmQuestion(), mQuizQuestions.get(mQuestionmCounter).getmAnswerOne(),
                                    mQuizQuestions.get(mQuestionmCounter).getmAnswerTwo(), mQuizQuestions.get(mQuestionmCounter).getmAnswerThree(),
                                    mQuizQuestions.get(mQuestionmCounter).getmAnswerFour(), answerFive);
                        }

                    }

                }

            }
        });



    }

    public void setFormattedQuestion(String question, String ans_1, String ans_2, String ans_3, String ans_4, String ans_5) {
        ans_1 = ans_1.replace("<p>", "");
        ans_1 = ans_1.replace("</p>", "");

        ans_2 = ans_2.replace("<p>", "");
        ans_2 = ans_2.replace("</p>", "");

        ans_3 = ans_3.replace("<p>", "");
        ans_3 = ans_3.replace("</p>", "");

        ans_4 = ans_4.replace("<p>", "");
        ans_4 = ans_4.replace("</p>", "");

        if(question.contains("\\"))  {
            question = question.replace("\\", "\\\\");
        }

        if(ans_1.contains("\\")) {
            ans_1 = ans_1.replace("\\", "\\\\");
        }

        if(ans_2.contains("\\")) {
            ans_2 = ans_2.replace("\\", "\\\\");
        }

        if(ans_3.contains("\\")) {
            ans_3 = ans_3.replace("\\", "\\\\");
        }

        if(ans_4.contains("\\")) {
            ans_4 = ans_4.replace("\\", "\\\\");
        }

        String mWebContent = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "  <title>Vidhvaa</title>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "  <link rel=\"stylesheet\" href=\"css/bootstrap_simple.min.css\">\n" +
                "  <style type = \"text/css\">\n" +
                "    body { background: #fafafa; font-family: Arial, sans-serif; line-height: 1.6; overflow-x:hidden; }\n" +
                "    .card { padding: 10px; max-width: 360px; border:0px; border-radius: 20px; margin: 10px auto; }\n" +
                "    .highlight { font-weight: bold; color: #294; }\n" +
                "    .shadow1 { box-shadow: 0 5px 10px rgba(154,160,185,.05), 0 15px 40px rgba(166,173,201,.2); }\n" +
                "    .shadow2 { box-shadow: 0 7px 30px -10px rgba(150,170,180,0.5); }\n" +
                "    .round { position: relative; }\n" +
                "\n" +
                "    .round label { background-color: #fff; border: 1px solid #ccc; border-radius: 50%; cursor: pointer; height: 20px; left: 0; position: absolute; top: 0; width: 20px; }\n" +
                "\n" +
                "    .round label:after {\n" +
                "      border: 2px solid #fff; border-top: none; border-right: none; content: \"\"; height: 4px; left: 4px; opacity: 0; position: absolute; top: 6px; transform: rotate(-45deg);\n" +
                "      width: 12px;\n" +
                "    }\n" +
                "\n" +
                "    .round input[type=\"checkbox\"] { visibility: hidden; }\n" +
                "    .round input[type=\"checkbox\"]:checked + label { background-color: #F5B041; border-color: #F5B041; }\n" +
                "    .round input[type=\"checkbox\"]:checked + label:after { opacity: 1; }\n" +
                "    * { -webkit-user-select: none; }\n" +
                "  </style>\n" +
                "  <script type='text/javascript' src='js/tex-mml-chtml.js'></script>  \n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div class='container' >\n" +
                "  <div class='row' >\n" +
                "    <div class='col-sm-12'>\n" +
                "      <div class='card shadow2'>\n" +
                "         " + question + "\n" +
                "      </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div class='row'>\n" +
                "      <div class='col-sm-12'>\n" +
                "          <div class='card shadow2'  onclick='handleAnswerClick(\"1\");' id= 'id_card_1'>      \n" +
                "            <div class='round'>\n" +
                "              <input type='checkbox' id='ans_1' onclick='handleClick(this, \"1\");' />\n" +
                "              <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_1 + "</span>\n" +
                "            </div>\n" +
                "          </div>        \n" +
                "      </div>\n" +
                "  </div>\n" +
                "  <div class='row'>\n" +
                "    <div class='col-sm-12'>\n" +
                "        <div class='card shadow2'  onclick='handleAnswerClick(\"2\");' id= 'id_card_2'>      \n" +
                "          <div class='round'>\n" +
                "            <input type='checkbox' id='ans_2' onclick='handleClick(this, \"2\");' />\n" +
                "            <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_2 + "</span>\n" +
                "          </div>\n" +
                "        </div>          \n" +
                "    </div>\n" +
                "  </div>\n" +
                "  <div class='row'>\n" +
                "      <div class='col-sm-12'>\n" +
                "         <div class='card shadow2'  onclick='handleAnswerClick(\"3\");' id= 'id_card_3'>      \n" +
                "            <div class='round'>\n" +
                "              <input type='checkbox' id='ans_3' onclick='handleClick(this, \"3\");' />\n" +
                "              <label for='checkbox'></label> <span style='padding-left: 8px;'>" + ans_3 + "</span>\n" +
                "            </div>\n" +
                "          </div>        \n" +
                "      </div>\n" +
                "  </div>\n" +
                "  <div class='row'>\n" +
                "    <div class='col-sm-12'>\n" +
                "          <div class='card shadow2'  onclick='handleAnswerClick(\"4\");' id= 'id_card_4'>      \n" +
                "            <div class='round'>\n" +
                "              <input type='checkbox' id='ans_4' onclick='handleClick(this, \"4\");' />\n" +
                "              <label for='checkbox'></label> <span style='padding: 8px;'>" + ans_4 + "</span>\n" +
                "            </div>\n" +
                "          </div>        \n" +
                "    </div>\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "<script type = 'text/javascript'>\n" +
                "var flag = 0;\n" +
                "function handleClick(cb, ans)\n" +
                "{\n" +
                "\n" +
                "  if(flag == 0)\n" +
                "  {\n" +
                "    if(cb.checked)\n" +
                "    {\n" +
                "        Android.getAnswer(ans);\n" +
                "        setAnswer(ans);\n" +
                "        flag = 1;\n" +
                "    }\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "function handleAnswerClick(ans)\n" +
                "{\n" +
                "  if(flag == 0)\n" +
                "  {\n" +
                "    Android.getAnswer(ans);\n" +
                "    setAnswer(ans);\n" +
                "    flag = 1;\n" +
                "  }\n" +
                "}\n" +
                "\n" +
                "function setAnswer(ans)\n" +
                "{\n" +
                "    if(ans == '1')\n" +
                "    {\n" +
                "      document.getElementById('ans_1').checked = true;\n" +
                "      document.getElementById('ans_1').disabled = true;\n" +
                "      document.getElementById('ans_2').disabled = true;\n" +
                "      document.getElementById('ans_3').disabled = true;\n" +
                "      document.getElementById('ans_4').disabled = true;\n" +
                "    }\n" +
                "    else if(ans == '2')\n" +
                "    {\n" +
                "      document.getElementById('ans_2').checked = true;\n" +
                "      document.getElementById('ans_1').disabled = true;\n" +
                "      document.getElementById('ans_2').disabled = true;\n" +
                "      document.getElementById('ans_3').disabled = true;\n" +
                "      document.getElementById('ans_4').disabled = true;\n" +
                "    }\n" +
                "    else if(ans == '3')\n" +
                "    {\n" +
                "      document.getElementById('ans_3').checked = true;\n" +
                "      document.getElementById('ans_1').disabled = true;\n" +
                "      document.getElementById('ans_2').disabled = true;\n" +
                "      document.getElementById('ans_3').disabled = true;\n" +
                "      document.getElementById('ans_4').disabled = true;\n" +
                "    }\n" +
                "    else if(ans == '4')\n" +
                "    {\n" +
                "      document.getElementById('ans_4').checked = true;\n" +
                "      document.getElementById('ans_1').disabled = true;\n" +
                "      document.getElementById('ans_2').disabled = true;\n" +
                "      document.getElementById('ans_3').disabled = true;\n" +
                "      document.getElementById('ans_4').disabled = true;\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>";

        //Log.i("web_content", mWebContent);
        if(isFirstQuestion)
        {
            final String qust = question;
            final String ans1 = ans_1;
            final String ans2 = ans_2;
            final String ans3 = ans_3;
            final String ans4 = ans_4;
            final String ans5 = ans_5;

            webViewModelExam.setWebViewClient(new WebViewClient() {

                public void onPageFinished(WebView view, String url) {
                    webViewModelExam.loadUrl("javascript:loadData('" +qust+  "', ' " +
                            ans1+ "', '"+
                            ans2 + "', '" +
                            ans3+ "', '" +
                            ans4 + "')");
                }
            });

            isFirstQuestion = false;
        }
        else {
            webViewModelExam.loadUrl("javascript:loadData('" +question+  "', ' " +
                    ans_1+ "', '"+
                    ans_2 + "', '" +
                    ans_3+ "', '" +
                    ans_4 + "')");
        }

        //webViewModelExam.loadDataWithBaseURL("file:///android_asset/", mWebContent, "text/html", "utf-8", null);

    }

    public void setAnswer(String answer) {
        if (mQuestionmCounter < mQuestionLength) {
            mQuizQuestions.get(mQuestionmCounter).setmUserAnswer(answer);

            //Get Time
            int questionTime = 18;
            if (!mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds().equals("")) {
                questionTime = Integer.parseInt(mQuizQuestions.get(mQuestionmCounter).getmQuestionSeconds());
            }

            int answerTime = mLocalCounter;
            int resultTime = questionTime - answerTime;
            mQuizQuestions.get(mQuestionmCounter).setmUserTime(String.valueOf(resultTime));
            if(Constants.isNetworkAvailable(VidhvaaApplication.getAppContext()))  {
                updateApiCall(mAuthToken, mExamId, mQuizQuestions.get(mQuestionmCounter).getmQuestionId(), mQuizQuestions.get(mQuestionmCounter).getmUserAnswer(), String.valueOf(resultTime));
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void setTimer() {
        //mCounter = 6;
        long milli = mCounter * 1000;
        isRunning = false;
        countDownTimer = new CountDownTimer(milli, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                progressBarCircle.setProgress(mCounter);
                textViewmCounterNumber.setText(String.valueOf(mCounter));
                mCounter--;
                isRunning = true;

            }

            @Override
            public void onFinish() {
                isRunning = false;

                if (isLastQuestion) {
                    exitExam();
                }

                if (mQuestionLength > 0) {
                    if (mQuestionmCounter < mQuestionLength) {

                        String dy_c = mQuizQuestions.get(mQuestionmCounter).getmQuestionLanguage();
                        if (dy_c.equals("0")) {
                            mQuizQuestions.get(mQuestionmCounter).setmQuestionLanguage(String.valueOf(mLanguageValue));
                        }
                        mQuestionmCounter++;
                        setQuestion();
                    }

                }
            }
        }.start();

        /*long milli = mCounter * 1000;
        isRunning = false;
        this.countDown = new PreciseCountdown(milli, 1000, 1000) {
            @Override
            public void onTick(long timeLeft) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewmCounterNumber.setText(String.valueOf(mCounter));
                        progressBarCircle.setProgress(mCounter);
                        // webViewModelExam.loadUrl("javascript:setTimer('" + mCounter + "')");
                        mCounter--;
                        isRunning = true;

                    }
                });

            }

            @Override
            public void onFinished() {
                onTick(0); // when the timer finishes onTick isn't called
                isRunning = false;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textViewmCounterNumber.setText("0");
                        progressBarCircle.setProgress(mCounter);
                    }
                });

                if (isLastQuestion) {
                    exitExam();
                }

                if (mQuestionLength > 0) {
                    if (mQuestionmCounter < mQuestionLength) {

                        String dy_c = mQuizQuestions.get(mQuestionmCounter).getmQuestionLanguage();
                        if (dy_c.equals("0")) {
                            mQuizQuestions.get(mQuestionmCounter).setmQuestionLanguage(String.valueOf(mLanguageValue));
                        }
                        mQuestionmCounter++;
                        setQuestion();
                    }

                }
            }
        };

        this.countDown.start();*/
    }

    private void setProgressBarValues() {
        progressBarCircle.setMax(mProgressbarValue);
        progressBarCircle.setProgress(mProgressbarValue);
    }

    public void exitExam() {
        if(isRunning) {
            countDownTimer.cancel();
            //countDown.stop();
        }
        Bundle b = new Bundle();
        b.putSerializable("QUESTION_SET", (Serializable) mQuizQuestions);
        b.putString("ln_code", mLanguageId);
        b.putString("mm_code", mExamId);
        b.putString("start_time", start_time);
        Intent ii = new Intent(getApplicationContext(), ModelExamResultActivity.class);
        ii.putExtra("QUESTIONS", b);
        startActivity(ii);
        finish();
    }

    @Override
    public void onResponse(String Tag, String response) {
//        Log.d("Test", "Request Tag : " + Tag + " Response : " + response);

        try {
            if (Tag.equals("MODEL_SET_ANSWER")) {
                JSONObject jObj = new JSONObject(response);
                int resultStatus = jObj.getInt("status");
                if (resultStatus == 200) {
                    //Toast.makeText(getApplicationContext(), "updated...", Toast.LENGTH_LONG).show();
                    String xx = "200";
                }
            }
        } catch (Exception e) {
            //alertDialogs.showErrorInformation("Error: " + e.getMessage());
        }
    }

    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {

        if (mLoading.isShowing()) {
            mLoading.cancel();
        }

        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    logoutMySession();
                }
            });
            return;
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        PopupModel popUpClass = new PopupModel(VidhvaaApplication.getAppContext(), ModelExamActivity.this);
        popUpClass.showPopupWindow(view_activity);
    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context ctx) {
            this.mContext = ctx;
        }

        @JavascriptInterface
        public void getAnswer(String answer) {
            setAnswer(answer);
        }

    }
}