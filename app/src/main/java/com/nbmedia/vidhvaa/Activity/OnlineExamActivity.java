package com.nbmedia.vidhvaa.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nbmedia.vidhvaa.Model.DailyQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.Utils.OwnFont;
import com.nbmedia.vidhvaa.Utils.PopupModel;
import com.nbmedia.vidhvaa.Utils.RequestSingleton;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OnlineExamActivity extends BaseActivity implements ServiceResponseListener {

    private Context mContext;
    OwnFont ownFont;
    ProgressDialog loading;

    public Toolbar toolbar;
    public BottomNavigationView bottomNavigationView;

    WebView model_question;
    TextView tv_counter;
    CheckBox ch_1, ch_2, ch_3, ch_4;
    LinearLayout lay_1, lay_2, lay_3, lay_4;

    WebView  ch_text_1, ch_text_2, ch_text_3, ch_text_4;
    TextView tv_sub_title, tv_lang,textViewquestion,textViewLanguage;

    Button btn_next, btn_submit;
    LinearLayout mo_lay_1, mo_lay_2, mo_lay_3, mo_lay_4;

    String auth_token = "";
    public int q_counter = 0;
    public int q_length = 0;
    public List<DailyQuestion> mQuizQuestions = new ArrayList<>();


    public int counter = 18;
    public int pr_value = 18;
    public CountDownTimer countDownTimer = null;
    public boolean isRunning = false;
    public int lang_value = 1;
    private ProgressBar progressBarCircle;

    String mm_code = "ME70343";
    String ln_code = "LN01";
    Bundle bundle = null;
    View view_activity = null;

    public float dpHeight = 0;
    public float dpWidth = 0 ;
    public int last_question = 0;
    public String start_time = "2020-10-10 00:00:00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_exam);

        initViews();
        initObjects();
        initListeners();
        initController();

    }

    public void initViews()
    {
        mContext = OnlineExamActivity.this;
        ownFont = new OwnFont(mContext,OnlineExamActivity.this);
        view_activity = (View)findViewById(android.R.id.content).getRootView();

        model_question = (WebView) findViewById(R.id.tv_online_question);
        tv_counter= (TextView)findViewById(R.id.tv_online_counter);
        tv_sub_title= (TextView)findViewById(R.id.tv_online_question_no);
        textViewquestion = (TextView)findViewById(R.id.tv_online_question_title);
        textViewLanguage = (TextView)findViewById(R.id.tv_online_lang_title);
        tv_lang = (TextView)findViewById(R.id.tv_online_lang);
        ch_1 = (CheckBox)findViewById(R.id.chk_online_1);
        ch_2 = (CheckBox)findViewById(R.id.chk_online_2);
        ch_3 = (CheckBox)findViewById(R.id.chk_online_3);
        ch_4 = (CheckBox)findViewById(R.id.chk_online_4);
        progressBarCircle = (ProgressBar)findViewById(R.id.progressBarCircle_online_clock);
        ch_text_1 = (WebView) findViewById(R.id.chk_online_text_1);
        ch_text_2 = (WebView) findViewById(R.id.chk_online_text_2);
        ch_text_3 = (WebView) findViewById(R.id.chk_online_text_3);
        ch_text_4 = (WebView) findViewById(R.id.chk_online_text_4);

        mo_lay_1 = (LinearLayout)findViewById(R.id.on_lay_1);
        mo_lay_2 = (LinearLayout)findViewById(R.id.on_lay_2);
        mo_lay_3 = (LinearLayout)findViewById(R.id.on_lay_3);
        mo_lay_4 = (LinearLayout)findViewById(R.id.on_lay_4);

        //button
        btn_next = (Button)findViewById(R.id.btn_online_next);
        btn_submit = (Button)findViewById(R.id.btn_online_submit);

        //setFont
        textViewLanguage.setTypeface(mFontInstance.getPoppinBold());
        textViewquestion .setTypeface(mFontInstance.getPoppinBold());
        tv_sub_title.setTypeface(mFontInstance.getPoppinBold());
        tv_lang.setTypeface(mFontInstance.getPoppinBold());
        tv_counter.setTypeface(mFontInstance.getPoppinBold());

    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        dpHeight = displayMetrics.heightPixels;
        dpWidth = displayMetrics.widthPixels ;

        bundle = getIntent().getExtras();
        //mm_code = bundle.getString("mm_code");
        //ln_code = bundle.getString("ln_code");

        auth_token = UserDataRepository.getInstance().getAuthToken();

        if(ln_code.equals("LN01")) {
            tv_lang.setText("Tamil");
        }
        else if(ln_code.equals("LN02")) {
            tv_lang.setText("English");
        }
    }

    public void initListeners()
    {
        ch_1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_2.setChecked(false);
                    ch_3.setChecked(false);
                    ch_4.setChecked(false);
                    setAnswer("1");
                }
            }
        });

        mo_lay_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ch_1.setChecked(true);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("1");
            }
        });


        ch_text_1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ch_1.setChecked(true);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("1");
                return false;
            }
        });

        ch_2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_3.setChecked(false);
                    ch_4.setChecked(false);
                    setAnswer("2");
                }
            }
        });

        mo_lay_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ch_2.setChecked(true);
                ch_1.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("2");
            }
        });

        ch_text_2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ch_2.setChecked(true);
                ch_1.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("2");
                return false;
            }
        });

        ch_3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_2.setChecked(false);
                    ch_4.setChecked(false);
                    setAnswer("3");
                }
            }
        });

        mo_lay_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ch_3.setChecked(true);
                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("3");
            }
        });

        ch_text_3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ch_3.setChecked(true);
                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_4.setChecked(false);
                setAnswer("3");
                return false;
            }
        });

        ch_4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ch_1.setChecked(false);
                    ch_2.setChecked(false);
                    ch_3.setChecked(false);
                    setAnswer("4");
                }
            }
        });

        mo_lay_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ch_4.setChecked(true);
                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                setAnswer("4");
            }
        });

        ch_text_4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ch_4.setChecked(true);
                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                setAnswer("4");
                return false;
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isRunning) {
                    if (countDownTimer != null) {
                        countDownTimer.cancel();
                    }
                }

                q_counter++;
                if(q_counter < q_length)  {
                    setQuestion();
                }
                else {
                    btn_next.setVisibility(View.INVISIBLE);
                    getDialog().showInformationDialog("No records available ..");
                    Toast.makeText(getApplicationContext(), "No Questions Available", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                exitExam();
            }
        });

    }

    public void initController()
    {
        setProgressBarValues();

        if(!auth_token.equals(""))
        {
            if(Constants.isNetworkAvailable(mContext))  {
                sendApi(auth_token, mm_code, ln_code);
            }
            else  {
                Intent ii = new Intent(getApplicationContext(), NoInternetActivity.class);
                startActivity(ii);
            }
        }

    }

    public void sendApi(String ap_auth_token, String ap_mm_code, String ap_ln_code) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("on_code", ap_mm_code);
            jsonObj.put("ln_code", ap_ln_code);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onResponse(String Tag, String reponse) {
        mLoading.cancel();
        try {
            Toast.makeText(getApplicationContext(), "Flag: " + reponse.toString(), Toast.LENGTH_SHORT).show();

            JSONObject jObj = new JSONObject(reponse.toString());
            int rest = jObj.getInt("status");

            if(rest == 200)
            {
                JSONArray ja_data = jObj.getJSONArray("questions");
                q_length = ja_data.length();
                mm_code = jObj.getString("on_code");
                start_time = jObj.getString("start_time");

                DailyQuestion mDailyModel;
                int i =0;

                for(i=0; i<q_length; i++)
                {
                    JSONObject ob_1 = ja_data.getJSONObject(i);

                    mDailyModel = new DailyQuestion();

                    mDailyModel.setQues_id(ob_1.getString("on_ques_id"));
                    mDailyModel.setQues_code(mm_code);
                    mDailyModel.setQuestion_order(ob_1.getString("on_order"));
                    mDailyModel.setQuestion(ob_1.getString("on_question"));
                    mDailyModel.setAns_1(ob_1.getString("on_ans_1"));
                    mDailyModel.setAns_2(ob_1.getString("on_ans_2"));
                    mDailyModel.setAns_3(ob_1.getString("on_ans_3"));
                    mDailyModel.setAns_4(ob_1.getString("on_ans_4"));
                    mDailyModel.setCorrect_answer(ob_1.getString("on_correct_ans"));
                    mDailyModel.setExplain(ob_1.getString("on_explain"));
                    mDailyModel.setUser_answer("0");
                    mDailyModel.setLanguage("0");
                    mDailyModel.setPattern(ob_1.getString("on_pattern"));
                    mDailyModel.setSeconds(ob_1.getString("on_seconds"));

                    mQuizQuestions.add(mDailyModel);

                }

                setQuestion();
            }
            else {

                Toast.makeText(getApplicationContext(),"No Data Added...",Toast.LENGTH_LONG).show();
            }

        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onErrorResponse(String Tag, VolleyError volleyError) {
        mLoading.cancel();
        //alertDialogs.showErrorInformation("Error: " + error);

        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }

        showErrorAlertDialog(message);
    }


    /*
    public void updateApi(String ap_auth_token, String ap_mm_code, String ap_ques_id, String ap_user_answer) {
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject();
            jsonObj.put("auth_token", ap_auth_token);
            jsonObj.put("mq_code", ap_mm_code);
            jsonObj.put("mq_ques_id", ap_ques_id);
            jsonObj.put("user_answer", ap_user_answer);

        } catch (JSONException e) {
            throw new RuntimeException(e);
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.MODEL_EXAM_ONE_RESULT, jsonObj, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Toast.makeText(getApplicationContext(), "Flag: " + response.toString(), Toast.LENGTH_SHORT).show();

                            JSONObject jObj = new JSONObject(response.toString());
                            int rest = jObj.getInt("status");

                            if(rest == 200)
                            {
                                //Toast.makeText(getApplicationContext(),"updated...",Toast.LENGTH_LONG).show();
                            }

                        }
                        catch (Exception e)
                        {
                            //Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        //Toast.makeText(getApplicationContext(), "Error: " + error, Toast.LENGTH_SHORT).show();

                    }
                });

        RequestSingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);

    }
    */


    public void setQuestion()
    {
        int m_seconds = 18;

        if(q_length > 0)
        {
            if(q_counter < q_length)
            {
                int x = q_counter + 1;
                tv_sub_title.setText(String.valueOf(x) + "/" + String.valueOf(q_length));

                setFormattedQuestion(mQuizQuestions.get(q_counter).getQuestion(), mQuizQuestions.get(q_counter).getAns_1(),
                        mQuizQuestions.get(q_counter).getAns_2(), mQuizQuestions.get(q_counter).getAns_3(),
                        mQuizQuestions.get(q_counter).getAns_4(), lang_value, mQuizQuestions.get(q_counter).getPattern());

                if(!mQuizQuestions.get(q_counter).getSeconds().equals(""))  {
                    m_seconds = Integer.parseInt(mQuizQuestions.get(q_counter).getSeconds());
                }


                ch_1.setChecked(false);
                ch_2.setChecked(false);
                ch_3.setChecked(false);
                ch_4.setChecked(false);

                int cc = q_length-1;

                if(cc == q_counter)
                {
                    btn_next.setVisibility(View.INVISIBLE);
                    last_question = 1;
                    Toast.makeText(getApplicationContext(), "No Questions Available", Toast.LENGTH_SHORT).show();
                }

                counter = m_seconds;
                pr_value = m_seconds;
                setProgressBarValues();
                setTimer();
            }

        }

    }

    public void setFormattedQuestion(String question, String ans_1, String ans_2, String ans_3 , String ans_4, int flg, String pattern)
    {

        String opt = "";
        Resources res = getResources();

        model_question.getSettings().setJavaScriptEnabled(true);
        model_question.getSettings().setLoadWithOverviewMode(true);
        model_question.getSettings().setUseWideViewPort(true);

        ch_text_1.getSettings().setJavaScriptEnabled(true);
        ch_text_2.getSettings().setJavaScriptEnabled(true);
        ch_text_3.getSettings().setJavaScriptEnabled(true);
        ch_text_4.getSettings().setJavaScriptEnabled(true);

        ch_text_1.getSettings().setLoadWithOverviewMode(true);
        ch_text_1.getSettings().setUseWideViewPort(true);
        ch_text_2.getSettings().setLoadWithOverviewMode(true);
        ch_text_2.getSettings().setUseWideViewPort(true);
        ch_text_3.getSettings().setLoadWithOverviewMode(true);
        ch_text_3.getSettings().setUseWideViewPort(true);
        ch_text_4.getSettings().setLoadWithOverviewMode(true);
        ch_text_4.getSettings().setUseWideViewPort(true);


        if(dpWidth >= 1080) {
            if(flg == 2)
            {
                model_question.getSettings().setDefaultFontSize(18);
                ch_text_1.getSettings().setDefaultFontSize(15);
                ch_text_2.getSettings().setDefaultFontSize(15);
                ch_text_3.getSettings().setDefaultFontSize(15);
                ch_text_4.getSettings().setDefaultFontSize(15);
            }
            else {
                model_question.getSettings().setDefaultFontSize(20);
                ch_text_1.getSettings().setDefaultFontSize(17);
                ch_text_2.getSettings().setDefaultFontSize(17);
                ch_text_3.getSettings().setDefaultFontSize(17);
                ch_text_4.getSettings().setDefaultFontSize(17);
            }

            model_question.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_1.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_2.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_3.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_4.getSettings().setTextSize(WebSettings.TextSize.SMALLER);

        }
        else if(dpWidth <= 1054 && dpWidth >= 720) {
            if(flg == 2)
            {
                model_question.getSettings().setDefaultFontSize(17);
                ch_text_1.getSettings().setDefaultFontSize(15);
                ch_text_2.getSettings().setDefaultFontSize(15);
                ch_text_3.getSettings().setDefaultFontSize(15);
                ch_text_4.getSettings().setDefaultFontSize(15);
            }
            else {
                model_question.getSettings().setDefaultFontSize(19);
                ch_text_1.getSettings().setDefaultFontSize(17);
                ch_text_2.getSettings().setDefaultFontSize(17);
                ch_text_3.getSettings().setDefaultFontSize(17);
                ch_text_4.getSettings().setDefaultFontSize(17);
            }

            model_question.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_1.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_2.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_3.getSettings().setTextSize(WebSettings.TextSize.SMALLER);
            ch_text_4.getSettings().setTextSize(WebSettings.TextSize.SMALLER);

        }

        model_question.loadDataWithBaseURL("file:///android_asset/", setHTML(question, "#FAE5D3",flg, dpWidth), "text/html", "utf-8", null);
        ch_text_1.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_1, "#EAEDED",flg, dpWidth), "text/html", "utf-8", null);
        ch_text_2.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_2, "#EAEDED",flg, dpWidth), "text/html", "utf-8", null);
        ch_text_3.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_3, "#EAEDED",flg, dpWidth), "text/html", "utf-8", null);
        ch_text_4.loadDataWithBaseURL("file:///android_asset/", setHTML(ans_4, "#EAEDED",flg, dpWidth), "text/html", "utf-8", null);

    }

    public String setHTML(String question, String color, int flg, double height)
    {
        String opt = "";
        String hc = "<!doctype html><html><head>";
        hc = hc + "<meta name='viewport' content='target-densitydpi=device-dpi, width=device-width, initial-scale=1.0' />";
        hc = hc + "<script type='text/javascript' src='es5/tex-mml-chtml.js'></script>";
        hc = hc + "<style>@font-face {" +
                "      font-family: poppins-regular;\n" +
                "      src: url('fonts/Poppins-Regular.ttf');\n" +
                "    }";
        hc = hc + "@font-face {" +
                "      font-family: Bamini;\n" +
                "      src: url('fonts/Bamini.TTF');\n" +
                "    }\n";
        hc = hc + "p{margin-top: 3px !important; margin-bottom: 3px !important;};";
        hc = hc + "</style></head>";
        if(flg == 1)  {
            hc = hc + "<body style ='background-color:"+color+"'; font-family:poppins-regular;line-height: 80%;>" + question + "</body><html>";
        }
        else {
            hc = hc + "<body style ='background-color:"+color+"';line-height: 80%;>" + question + "</body><html>";
        }

        return hc;

    }

    public void setAnswer(String answer)
    {
        if(q_counter < q_length)  {
            mQuizQuestions.get(q_counter).setUser_answer(answer);
        }
    }

    public void setTimer()
    {
        //counter = 6;
        long milli = counter * 1000;
        isRunning = false;
        countDownTimer = new CountDownTimer(milli,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tv_counter.setText(String.valueOf(counter));
                counter--;
                isRunning = true;
                progressBarCircle.setProgress((int)counter);
            }
            @Override
            public void onFinish() {
                isRunning = false;

                if(last_question == 1)
                {
                    exitExam();
                }

                if(q_length > 0) {
                    if (q_counter < q_length) {

                        String dy_c = mQuizQuestions.get(q_counter).getLanguage();
                        if(dy_c.equals("0")) {
                            mQuizQuestions.get(q_counter).setLanguage(String.valueOf(lang_value));
                        }

                        //Send Result to Server
                        /*if(Constants.isNetworkAvailable(mContext))  {
                            updateApi(auth_token, mm_code, mQuizQuestions.get(q_counter).getQues_id(), mQuizQuestions.get(q_counter).getUser_answer());
                        }*/


                        q_counter++;
                        setQuestion();
                    }
                    else {
                        btn_next.setVisibility(View.INVISIBLE);
                        btn_submit.setVisibility(View.VISIBLE);
                    }
                }



            }
        }.start();
    }

    private void setProgressBarValues()
    {
        progressBarCircle.setMax(pr_value);
        progressBarCircle.setProgress(pr_value);
    }

    public void exitExam()
    {
        /*Bundle b = new Bundle();
        b.putSerializable("QUESTION_SET",(Serializable)mQuizQuestions);
        b.putString("ln_code", ln_code);
        b.putString("mm_code", mm_code);
        b.putString("start_time", start_time);
        Intent ii = new Intent(getApplicationContext(), ModelExamResultActivity.class);
        ii.putExtra("QUESTIONS",b);
        startActivity(ii);
        finish();*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        PopupModel popUpClass = new PopupModel(mContext, OnlineExamActivity.this);
        popUpClass.showPopupWindow(view_activity);
    }


}