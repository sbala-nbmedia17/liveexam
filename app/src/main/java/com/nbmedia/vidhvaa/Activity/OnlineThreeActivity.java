package com.nbmedia.vidhvaa.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Toast;

import com.nbmedia.vidhvaa.Model.ModelQuestion;
import com.nbmedia.vidhvaa.R;
import com.nbmedia.vidhvaa.Utils.Constants;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;

public class OnlineThreeActivity extends AppCompatActivity {

    WebView webViewModelResult;

    public List<ModelQuestion> mQuizQuestions = new ArrayList<>();

    String mAuthToken = "",  mMqCore = "-", ln_code = "-", start_time = "2020-10-10 00:00:00", mWebContent = "", mSubjectContent = "", mModelStatus = "";
    public int mCorrectAnswer = 0, mWrongAnswer = 0, mSkippedAnswer = 0, mTotalAnswer = 0, mTotalTimeTaken = 0;

    JSONArray user_all;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_three);

        initViews();
        initObjects();
        initListeners();
        initController();
    }

    public void initViews()
    {
        webViewModelResult = (WebView)findViewById(R.id.webview_online_result);
        final Drawable drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_hear);
        final Shape.DrawableShape drawableShape = new Shape.DrawableShape(drawable, true);

        final KonfettiView konfettiView = findViewById(R.id.online_konfettiView);
        konfettiView.build()
                .addColors(Color.YELLOW, Color.CYAN, Color.MAGENTA,Color.BLUE)
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(2000L)
                .addShapes(Shape.Square.INSTANCE, Shape.Circle.INSTANCE, drawableShape)
                .addSizes(new Size(12, 5f))
                .setPosition(-50f, konfettiView.getWidth() + 50f, -50f, -50f)
                .streamFor(300, 5000L);
    }

    public void initObjects()
    {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.dashboardtop));
            window.setNavigationBarColor(getResources().getColor(R.color.bottomcolor));
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
        }

        //Toolbar
        webViewModelResult.getSettings().setJavaScriptEnabled(true);
        webViewModelResult.getSettings().setDomStorageEnabled(true);
        webViewModelResult.setOverScrollMode(webViewModelResult.OVER_SCROLL_NEVER);


        //Bundle Parameters for Questions
        Bundle args = getIntent().getBundleExtra("QUESTIONS");
        mQuizQuestions = (ArrayList<ModelQuestion>) args.getSerializable("QUESTION_SET");
        ln_code = args.getString("ln_code");
        mMqCore = args.getString("mm_code");
        start_time = args.getString("start_time");
        mAuthToken = UserDataRepository.getInstance().getAuthToken_before();

        user_all = new JSONArray();
    }

    public void initListeners()
    {

    }

    public void initController()
    {
        String ques_id = "";
        String user_ans = "";
        String user_time = "0";
        mTotalTimeTaken = 0;

        try
        {
            for(int x =0; x< mQuizQuestions.size(); x++)
            {
                ques_id = mQuizQuestions.get(x).getmQuestionId();
                user_ans = mQuizQuestions.get(x).getmUserAnswer();
                user_time =mQuizQuestions.get(x).getmUserTime();

                if(mQuizQuestions.get(x).getmUserAnswer().equals(mQuizQuestions.get(x).getmCorrectAnswer()))  {
                    mCorrectAnswer++;
                    mTotalTimeTaken = mTotalTimeTaken + Integer.parseInt(user_time);
//                    Log.i("Answer Time", ques_id + "---" + user_time);
//                    Log.i("Total Time :", String.valueOf(mTotalTimeTaken));
                }
                else {
                    if(!mQuizQuestions.get(x).getmUserAnswer().equals("0"))  {
                        mWrongAnswer++;
                    }
                    else {
                        mSkippedAnswer++;
                    }
                }

                mTotalAnswer++;

                JSONObject obj = new JSONObject();
                obj.put("mq_code", mMqCore);
                obj.put("mq_ques_id", ques_id);
                obj.put("ln_code", ln_code);
                obj.put("mq_user_ans", user_ans);
                obj.put("mq_user_time", user_time);

                user_all.put(obj);

                int size = mQuizQuestions.size();

                mSkippedAnswer = size - (mCorrectAnswer + mWrongAnswer);

            }

            mSubjectContent = mSubjectContent + "<tr><th>Total Number of Questions</th>";
            mSubjectContent = mSubjectContent + "<td>" + mTotalAnswer + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#58D68D'>&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Total Number of Correct Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mCorrectAnswer + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#E74C3C'>&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Total Number of Wrong Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mWrongAnswer + "</td></tr>";

            mSubjectContent = mSubjectContent + "<tr><th><span style ='background-color:#626567'>&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Total Number of Skipped Answers</th>";
            mSubjectContent = mSubjectContent + "<td>" + mSkippedAnswer + "</td></tr>";

            setAnswerResult(String.valueOf(mCorrectAnswer), String.valueOf(mWrongAnswer), String.valueOf(mSkippedAnswer));

        }
        catch(Exception e)
        {

        }

    }

    public void setAnswerResult(String correct, String wrong, String not)
    {
        mWebContent = "<!DOCTYPE html>\n" +
                "<html lang=\"en\" >\n" +
                "<head>\n" +
                "  <head>\n" +
                "    <title>Vidhvaa</title>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <link rel=\"stylesheet\" href=\"css/bootstrap_simple.min.css\">\n" +
                "    <style type = \"text/css\">\n" +
                "      body { background: #fafafa; font-family: Arial, sans-serif; line-height: 1.6; overflow-x:hidden; }\n" +
                "      .card { padding: 10px; max-width: 360px; border:0px; border-radius: 20px; margin: 10px auto; }\n" +
                "      .highlight { font-weight: bold; color: #294; }\n" +
                "      .shadow1 { box-shadow: 0 5px 10px rgba(154,160,185,.05), 0 15px 40px rgba(166,173,201,.2); }\n" +
                "      .shadow2 { box-shadow: 0 7px 30px -10px rgba(150,170,180,0.5); }\n" +
                "      .btn-circle-blue { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#626567; color:#FFFFFF ; }\n" +
                "      .btn-circle-green { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#58D68D; color:#FFFFFF ; }\n" +
                "      .btn-circle-red { width: 35px; height: 35px; text-align: center; padding: 6px 0; font-size: 15px; border-radius: 50%; background-color:#E74C3C; color:#FFFFFF ; }\n" +
                "      .row {margin:0; padding:5px;}\n" +
                "      canvas { max-width: 100%; max-height: 100%; }\n" +
                "      .score { padding: 0; font-size: 30px; margin-top: 45px; }\n" +
                "      .badge{ margin-top: 20px; }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body >\n" +
                "<div class='container' >\n" +
                "  <br>\n" +
                "  <div class='row'>\n" +
                "    <div class='col-12 col-xs-12 col-sm-12'>\n" +
                "        <div id='canvas-holder'>\n" +
                "            <canvas id='chart-area' width='300' height='300'></canvas>\n" +
                "          </div>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "\n" +
                "   <div class='row'>\n" +
                "      <div class='col-md-12'>\n" +
                "         <table class = 'table table-striped'>\n" +
                " " + mSubjectContent +
                "         </table>\n" +
                "      </div>\n" +
                "  </div>\n" +
                " \n" +
                "  \n" +
                "  \n" +
                " \n" +
                "</div>\n" +
                "<script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js'></script>\n" +
                "<script type ='text/javascript' >\n" +
                "var pieData = [\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(correct) + ",\n" +
                "    color: '#58D68D',\n" +
                "    highlight: '#58D68D',\n" +
                "    label: 'Correct Answers' },\n" +
                "\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(wrong) + ",\n" +
                "    color: '#E74C3C',\n" +
                "    highlight: '#E74C3C',\n" +
                "    label: 'Wrong Answers' },\n" +
                "\n" +
                "  {\n" +
                "    value: " + Integer.parseInt(not) + ",\n" +
                "    color: '#626567 ',\n" +
                "    highlight: '#626567 ',\n" +
                "    label: 'Skipped Answers' },\n" +
                "\n" +
                "  ];"+
                "  options = {\n" +
                "  \n" +
                "  segmentShowStroke: true,\n" +
                "  segmentStrokeColor: 'rgba(0,0,0,0)',\n" +
                "  segmentStrokeWidth: 1,\n" +
                "  percentageInnerCutout: 50, // This is 0 for Pie charts\n" +
                "  animationSteps: 30,\n" +
                "  animationEasing: 'none',\n" +
                "  animateRotate: true,\n" +
                "  animateScale: true,\n" +
                "  legendTemplate: \"<ul class=\\\"<%=name.toLowerCase()%>-legend\\\"><% for (var i=0; i<segments.length; i++){%><li><span style=\\\"background-color:<%=segments[i].fillColor%>\\\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>\" };\n" +
                "\n" +
                "\n" +
                "  window.onload = function () {\n" +
                "    var ctx = document.getElementById('chart-area').getContext('2d');\n" +
                "    window.myPie = new Chart(ctx).Pie(pieData, options);\n" +
                "  };\n" +
                "\n" +
                "\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>\n";

        //Log.i("result_data", mWebContent);

        webViewModelResult.loadDataWithBaseURL("file:///android_asset/", mWebContent, "text/html", "utf-8", null);
    }

    @Override
    public void onBackPressed() {
        //Execute your code here
        /*Intent ii = new Intent(getApplicationContext(), DashboardActivity.class);
        startActivity(ii);
        finish();*/
    }
}