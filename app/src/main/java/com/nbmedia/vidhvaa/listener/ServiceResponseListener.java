package com.nbmedia.vidhvaa.listener;

import com.android.volley.VolleyError;

public interface ServiceResponseListener {
    void onResponse(String Tag, String reponse);

    void onErrorResponse(String Tag, VolleyError volleyError);
}
