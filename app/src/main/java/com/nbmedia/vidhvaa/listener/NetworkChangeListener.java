package com.nbmedia.vidhvaa.listener;

public interface NetworkChangeListener {
    void onNetworkConnectionChanged(boolean isConnected);
}
