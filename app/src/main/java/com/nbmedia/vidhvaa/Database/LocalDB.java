package com.nbmedia.vidhvaa.Database;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nbmedia.vidhvaa.Model.DailyResult;
import com.nbmedia.vidhvaa.Utils.PreferenceUtils;
import com.nbmedia.vidhvaa.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.List;

public class LocalDB {
    SQLiteDatabase db;
    private Context context;
    Activity acv;

    public LocalDB(Context ctx, Context actvy)
    {
        context=ctx;
        acv= (Activity) actvy;
        db=actvy.openOrCreateDatabase("VidhvaDB", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS vid_user(user_name VARCHAR, " +
                "user_mobile VARCHAR, user_mail VARCHAR, user_state VARCHAR, user_pwd VARCHAR, auth_token VARCHAR, user_status VARCHAR, user_code VARCHAR, user_level VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS vid_daily_task(dy_code VARCHAR, " +
                "quest_id VARCHAR, user_answer VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS vid_back_event(bk_activity VARCHAR, bk_status VARCHAR);");

        db.execSQL("CREATE TABLE IF NOT EXISTS vid_firebase(fb_token VARCHAR, fb_status VARCHAR);");
    }

    public synchronized void insertUser(String user_name, String user_mobile, String user_mail, String user_state, String user_pwd, String auth_token, String user_code, String user_level, String user_status)
    {
        db.execSQL("INSERT INTO vid_user VALUES('"+user_name+"','"+user_mobile+"','" + user_mail + "','" + user_state + "', '" + user_pwd + "', '" + auth_token +"', '"+user_status+"','"+user_code+"','"+user_level+"' );" );
    }

   /* public synchronized  void storeFirebase(String token)  {
        db.execSQL("INSERT INTO vid_firebase VALUES('"+token+"', '1' );" );
    }

    public String checkFirebase()
    {
        String stat = "0";
        Cursor c=db.rawQuery("SELECT * FROM vid_firebase",null);
        if(c.moveToFirst())
        {
            stat = "1";
        }
        return stat;
    }

    public String getFirebase()
    {
        String token = "0";
        Cursor c=db.rawQuery("SELECT * FROM vid_firebase",null);
        if(c.moveToFirst())
        {
            token = c.getString(0);
        }
        return token;
    }

    public void insertUser_ex(String user_name, String user_mobile, String user_mail, String user_state, String user_pwd, String auth_token, String user_code, String status)
    {
        db.execSQL("INSERT INTO vid_user VALUES('"+user_name+"','"+user_mobile+"','" + user_mail + "','" + user_state + "', '" + user_pwd + "', '" + auth_token +"', '"+status+"','"+user_code+"'   );" );
    }



    public String checkRegisteredUser()
    {
        String stat = "0";
        String u_status = "0";
        Cursor c=db.rawQuery("SELECT * FROM vid_user",null);
        if(c.moveToFirst())
        {
            u_status = c.getString(6);

            if(u_status.equals("1")) {
                stat = "1";
            }
            if(u_status.equals("0")) {
                stat = "2";
            }
        }
        return stat;
    }*/

    public String updateHalfRegistration(String auth_token)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where auth_token='"+auth_token+"' ",null);
        if(c.moveToFirst())  {
            db.execSQL("UPDATE vid_user SET user_status = '0.5'  where auth_token='"+auth_token+"'" );
            stat = "1";
        }

        return stat;
    }

    public String updateFullRegistration(String auth_token, String user_name)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where auth_token='"+auth_token+"' ",null);
        if(c.moveToFirst())  {
            db.execSQL("UPDATE vid_user SET user_status = '1', user_name ='" + user_name +"'  where auth_token='"+auth_token+"'" );
            stat = "1";
        }

        return stat;
    }

    public String closeRegistration(String auth_token)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where auth_token='"+auth_token+"' ",null);
        if(c.moveToFirst())  {
            db.execSQL("DELETE FROM vid_user" );
            stat = "1";
        }

        return stat;
    }

    public String updateRegisterDetails(String auth_token, String user_code, String user_name, String user_mobile, String user_email, String user_pwd)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where auth_token='"+auth_token+"' ",null);
        if(c.moveToFirst())  {
            db.execSQL("UPDATE vid_user SET user_code = '"+ user_code + "', " +
                    "user_name='"+user_name+"', " +
                    "user_mobile ='"+user_mobile+"', " +
                    "user_mail='"+user_email+"'," +
                    "user_pwd='" + user_pwd + "', " +
                    "user_status = '1'  where auth_token='"+auth_token+"'" );
            stat = "1";
        }

        return stat;
    }

    public void updateUserToken(String auth_token, String user_code)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_code='"+user_code+"' ",null);
        if(c.moveToFirst())  {
            stat = "1";
        }
        if(stat.equals("1"))  {
            db.execSQL("UPDATE vid_user SET user_status='1' where user_code='"+user_code+"'" );
        }
    }

    public void updatePassword(String password, String user_code)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_code='"+user_code+"' ",null);
        if(c.moveToFirst())  {
            stat = "1";
        }
        if(stat.equals("1"))  {
            db.execSQL("UPDATE vid_user SET user_pwd='"+password+"' where user_code='"+user_code+"'" );
        }
    }

    public void updateUserDetails(String user_code, String user_name, String user_mobile, String user_email)
    {
        String stat = "0";

        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_code='"+user_code+"' ",null);
        if(c.moveToFirst())  {
            stat = "1";
        }
        if(stat.equals("1"))  {
            db.execSQL("UPDATE vid_user SET user_name='"+user_name+"', user_mobile ='"+user_mobile+"', user_mail='"+user_email+"' where user_code='"+user_code+"'" );
        }
    }

    public String checkUser(String user_mail, String user_pwd)
    {
        String stat = "0";
        String u_status = "0";
        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_mail ='" + user_mail +"' AND user_pwd ='" + user_pwd +"'",null);
        if(c.moveToFirst())
        {
            u_status = c.getString(6);

            if(u_status.equals("1")) {
                stat = "1";
            }
            if(u_status.equals("0")) {
                stat = "2";
            }
        }
        return stat;
    }

    public String checkSignupUser()
    {
        String stat = "0";
        String u_status = "0";
        Cursor c=db.rawQuery("SELECT * FROM vid_user",null);
        if(c.moveToFirst())
        {

        }

        u_status = PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_USER_STATUS).toString();

//        Log.i("user_status", u_status);

        if(u_status.equals("PAID")) {
            stat = "1";     //paid user
        }
        if(u_status.equals("PENDING_OTP")) {
            stat = "2";     //otp pending
        }
        if(u_status.equals("GUEST")) {
            stat = "3";     //dashboard but not paid
        }
        return stat;
    }

    public String getUserName()
    {
        String user_name="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_status='1' ",null);
        if(c.moveToFirst())
        {
            user_name=c.getString(0);
        }
        return user_name;
    }

    public String getAuthToken()
    {
        String auth_token="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_status='1' ",null);
        if(c.moveToFirst())
        {
            auth_token=c.getString(5);
        }
        return PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
    }

    public String getAuthToken_before()
    {
        String auth_token="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user",null);
        if(c.moveToFirst())
        {
            auth_token=c.getString(5);
        }
        return PreferenceUtils.getInstance().getPreferenceContent(PreferenceUtils.PREF_APP_AUTH_TOKEN).toString();
    }

    public String getUserCode()
    {
        String user_code="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user where user_status='1' ",null);
        if(c.moveToFirst())
        {
            user_code=c.getString(7);
        }
        return user_code;
    }

  /*  public String getUserLevel()
    {
        String user_level="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user ",null);
        if(c.moveToFirst())
        {
            user_level=c.getString(8);
        }
        return user_level;
    }

    public String getUserCode_before()
    {
        String user_code="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user ",null);
        if(c.moveToFirst())
        {
            user_code=c.getString(7);
        }
        return user_code;
    }*/

    public String getMobile()
    {
        String user_mobile="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user ",null);
        if(c.moveToFirst())
        {
            user_mobile=c.getString(1);
        }
        return UserDataRepository.getInstance().getMobileNumber();
    }

    public String getEmail()
    {
        String user_mail="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user ",null);
        if(c.moveToFirst())
        {
            user_mail=c.getString(2);
        }
        return UserDataRepository.getInstance().getEmailId();
    }
    public String getMpin(String auth_token)
    {
        String mpin="";
        Cursor c=db.rawQuery("SELECT * FROM vid_user where auth_token ='" + auth_token +"' ",null);
        if(c.moveToFirst())
        {
            mpin=c.getString(4);
        }
        return mpin;
    }

  /*  public void insertDailyTask(String dy_code, String quest_id, String user_answer)
    {
        db.execSQL("INSERT INTO vid_daily_task VALUES('"+dy_code+"','"+quest_id+"','" + user_answer + "');" );
    }

    public void deletetDailyTask()
    {
        db.execSQL("DELETE FROM vid_daily_task");
    }*/

    public List<DailyResult> getDailyTask()
    {
        List<DailyResult> mQuizAnswers = new ArrayList<>();
        DailyResult dr = null;
        Cursor c=db.rawQuery("SELECT * FROM vid_daily_task",null);
        if(c.moveToFirst())
        {
            while (!c.isAfterLast()) {

                dr = new DailyResult();
                dr.setQues_code(c.getString(0));
                dr.setQues_id(c.getString(1));
                dr.setUser_answer(c.getString(2));
                mQuizAnswers.add(dr);

                c.moveToNext();

            }
            //user_code=c.getString(1);

        }

        return mQuizAnswers;
    }

  /*  public void insertActivity(String bk_activity)
    {
        db.execSQL("INSERT INTO vid_back_event VALUES('"+bk_activity+"','1');" );
    }

    public void deleteActivity()
    {
        db.execSQL("DELETE FROM vid_back_event");
    }*/

    public String geBackActivity()
    {
        String bk_activity="";
        Cursor c=db.rawQuery("SELECT * FROM vid_back_event ",null);
        if(c.moveToFirst())
        {
            bk_activity=c.getString(0);
        }
        return bk_activity;
    }

    public void clearDataBase() {
        String clearDBQuery="";
//        Log.d("LocalDB","clearDataBase clearAllSession");
        try {
            clearDBQuery = "DELETE FROM vid_user";
            db.execSQL(clearDBQuery);
            clearDBQuery = "DELETE FROM vid_daily_task";
            db.execSQL(clearDBQuery);
            clearDBQuery = "DELETE FROM vid_firebase";
            db.execSQL(clearDBQuery);
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
