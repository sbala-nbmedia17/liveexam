package com.nbmedia.vidhvaa;


import android.app.Application;
import android.content.Context;

import com.instabug.library.Instabug;
import com.instabug.library.invocation.InstabugInvocationEvent;
import com.nbmedia.vidhvaa.Utils.FileUtils;

public class VidhvaaApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        VidhvaaApplication.context = getApplicationContext();
        /*new Instabug.Builder(this, "f9a54c89568c1fff04beb804b36689a0")
                .setInvocationEvents(
                        InstabugInvocationEvent.TWO_FINGER_SWIPE_LEFT,
                        InstabugInvocationEvent.NONE)
                .build();
        Instabug.identifyUser("Exam Support","nbmexam@gmail.com");
        Instabug.setUserData("Your every valuable suggestion always welcome...");*/

        createCache();
    }

    private void createCache() {
        try {
            FileUtils.getInstance(this).createParentDirectory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Context getAppContext() {
        return VidhvaaApplication.context;
    }
}