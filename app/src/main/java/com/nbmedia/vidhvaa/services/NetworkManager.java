package com.nbmedia.vidhvaa.services;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.nbmedia.vidhvaa.VidhvaaApplication;
import com.nbmedia.vidhvaa.listener.ServiceResponseListener;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class NetworkManager {
    private static NetworkManager instance;
    private RequestQueue mRequestQueue;
    private int mNumberOfRetries = 1;

    private NetworkManager() {
        mRequestQueue = Volley.newRequestQueue(VidhvaaApplication.getAppContext());
    }

    public static NetworkManager getInstance() {
        if (instance == null) {
            instance = new NetworkManager();
        }
        return instance;
    }

    public void cancelAll(Object tag) {
        mRequestQueue.cancelAll(tag);
    }

    public void sendJsonObjectPostRequest(final Object tag, String url, final ServiceResponseListener service, JSONObject jsonObject) {

        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {
//                Log.d("test", "onResponse1234>>>" + object);
                service.onResponse((String) tag, object.toString());


                // ConfigRepository db = new ConfigRepository();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                Log.d("test", "onErrorResponse>>>" + volleyError.toString());
                service.onErrorResponse((String) tag, volleyError);
            }
        };
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url,
                jsonObject, listener, errorListener)

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }
        };


        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                mNumberOfRetries, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(jsObjRequest);
    }

    public void sendGetRequest(final Object tag, String url, final ServiceResponseListener service) {

        Response.Listener<JSONObject> listener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject object) {
//                Log.d("test", "onResponse1234>>>" + object);
                service.onResponse((String) tag, object.toString());


                // ConfigRepository db = new ConfigRepository();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
//                Log.d("test", "onErrorResponse>>>" + volleyError.toString());
                service.onErrorResponse((String) tag, volleyError);
            }
        };
        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url,
                null, listener, errorListener)

        {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/json");
                return headers;
            }
        };


        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(6000,
                mNumberOfRetries, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(jsObjRequest);
    }

}
