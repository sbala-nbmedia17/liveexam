package com.nbmedia.vidhvaa.Model;

public class BookBack {
    private String code;
    private String language;
    private String standard;
    private String standardName;
    private String subject;
    private String subjectName;
    private String status;

    public BookBack(String a_code, String a_language, String a_standard, String a_standard_name, String a_subject, String a_subject_name, String a_status)
    {
        this.code = a_code;
        this.language = a_language;
        this.standard = a_standard;
        this.standardName = a_standard_name;
        this.subject = a_subject;
        this.subjectName = a_subject_name;
        this.status = a_status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getStandardName() {
        return standardName;
    }

    public void setStandardName(String standardName) {
        this.standardName = standardName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
