package com.nbmedia.vidhvaa.Model;

public class LanguageMaterial {
    private String code;
    private String language;
    private String status;

    public LanguageMaterial(String ap_code, String ap_language, String ap_status)
    {
        this.code =ap_code;
        this.language = ap_language;
        this.status = ap_status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
