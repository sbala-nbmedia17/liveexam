package com.nbmedia.vidhvaa.Model;

public class DailyResult {
    private String ques_id;
    private String ques_code;
    private String user_answer;

    public DailyResult() {
        this.ques_id = "";
        this.ques_code = "";
        this.user_answer = "";
    }

    public String getQues_id() {
        return ques_id;
    }

    public void setQues_id(String ques_id) {
        this.ques_id = ques_id;
    }

    public String getQues_code() {
        return ques_code;
    }

    public void setQues_code(String ques_code) {
        this.ques_code = ques_code;
    }

    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }

    public String getUser_answer() {
        return user_answer;
    }
}
