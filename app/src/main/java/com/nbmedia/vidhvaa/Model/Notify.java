package com.nbmedia.vidhvaa.Model;

public class Notify {
    private String code;
    private String date;
    private String time;
    private String notify_data;

    public Notify(String ap_code, String ap_date, String ap_time, String ap_data)
    {
        this.code = ap_code;
        this.date = ap_date;
        this.time = ap_time;
        this.notify_data = ap_data;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNotify_data() {
        return notify_data;
    }

    public void setNotify_data(String notify_data) {
        this.notify_data = notify_data;
    }
}
