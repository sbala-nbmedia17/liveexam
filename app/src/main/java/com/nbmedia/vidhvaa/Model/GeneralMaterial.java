package com.nbmedia.vidhvaa.Model;

public class GeneralMaterial {
    private String code;
    private String category;
    private String exam;
    private String language;
    private String subject;
    private String chapter_code;
    private String chapter_name_english;
    private String chapter_name_tamil;
    private String chapter_order;

    public GeneralMaterial(String ap_c, String ap_cat, String ap_exam, String ap_lang, String ap_subj, String ch_code, String ch_name_english, String ch_name_tamil, String ch_order)
    {
        this.code = ap_c;
        this.category = ap_cat;
        this.exam = ap_exam;
        this.language =ap_lang;
        this.subject = ap_subj;
        this.chapter_code = ch_code;
        this.chapter_name_english = ch_name_english;
        this.chapter_name_tamil = ch_name_tamil;
        this.chapter_order = ch_order;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public String getChapter_code() {
        return chapter_code;
    }

    public void setChapter_code(String chapter_code) {
        this.chapter_code = chapter_code;
    }

    public String getChapter_name_tamil() {
        return chapter_name_tamil;
    }

    public void setChapter_name_tamil(String chapter_name_tamil) {
        this.chapter_name_tamil = chapter_name_tamil;
    }

    public String getChapter_name_english() {
        return chapter_name_english;
    }

    public void setChapter_name_english(String chapter_name_english) {
        this.chapter_name_english = chapter_name_english;
    }

    public String getChapter_order() {
        return chapter_order;
    }

    public void setChapter_order(String chapter_order) {
        this.chapter_order = chapter_order;
    }
}
