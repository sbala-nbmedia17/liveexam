package com.nbmedia.vidhvaa.Model;

public class DailyTaskQuestion implements java.io.Serializable{

    private String mQuestionId;
    private String mExamId;
    private String mQuestion;
    private String mQuestionLanguage;
    private String mQuestionOrder;
    private String mQuestionPattern;
    private String mQuestionSeconds;
    private String mAnswerOne;
    private String mAnswerTwo;
    private String mAnswerThree;
    private String mAnswerFour;
    private String mCorrectAnswer;
    private String mExplain;
    private String mUserAnswer;
    private String mUserTime;

    public DailyTaskQuestion(String mQuestionId, String mExamId, String mQuestionLanguage, String mQuestionOrder, String mQuestionPattern, String mQuestionSeconds,
                         String mQuestion, String mAnswerOne, String mAnswerTwo, String mAnswerThree, String mAnswerFour, String mCorrectAnswer, String mUserAnswer, String mUserTime, String mExplain)
    {
        this.mQuestionId = mQuestionId;
        this.mExamId = mExamId;
        this.mQuestion = mQuestion;
        this.mQuestionLanguage = mQuestionLanguage;
        this.mQuestionOrder =mQuestionOrder;
        this.mQuestionPattern = mQuestionPattern;
        this.mQuestionSeconds = mQuestionSeconds;
        this.mAnswerOne = mAnswerOne;
        this.mAnswerTwo = mAnswerTwo;
        this.mAnswerThree = mAnswerThree;
        this.mAnswerFour = mAnswerFour;
        this.mCorrectAnswer = mCorrectAnswer;
        this.mUserAnswer = mUserAnswer;
        this.mUserTime = mUserTime;
        this.mExplain = mExplain;
    }


    public String getmQuestionId() {
        return mQuestionId;
    }

    public void setmQuestionId(String mQuestionId) {
        this.mQuestionId = mQuestionId;
    }

    public String getmExamId() {
        return mExamId;
    }

    public void setmExamId(String mExamId) {
        this.mExamId = mExamId;
    }

    public String getmQuestionOrder() {
        return mQuestionOrder;
    }

    public void setmQuestionOrder(String mQuestionOrder) {
        this.mQuestionOrder = mQuestionOrder;
    }

    public String getmQuestionPattern() {
        return mQuestionPattern;
    }

    public void setmQuestionPattern(String mQuestionPattern) {
        this.mQuestionPattern = mQuestionPattern;
    }

    public String getmQuestionSeconds() {
        return mQuestionSeconds;
    }

    public void setmQuestionSeconds(String mQuestionSeconds) {
        this.mQuestionSeconds = mQuestionSeconds;
    }

    public String getmAnswerOne() {
        return mAnswerOne;
    }

    public void setmAnswerOne(String mAnswerOne) {
        this.mAnswerOne = mAnswerOne;
    }

    public String getmAnswerTwo() {
        return mAnswerTwo;
    }

    public void setmAnswerTwo(String mAnswerTwo) {
        this.mAnswerTwo = mAnswerTwo;
    }

    public String getmAnswerThree() {
        return mAnswerThree;
    }

    public void setmAnswerThree(String mAnswerThree) {
        this.mAnswerThree = mAnswerThree;
    }

    public String getmAnswerFour() {
        return mAnswerFour;
    }

    public void setmAnswerFour(String mAnswerFour) {
        this.mAnswerFour = mAnswerFour;
    }


    public String getmCorrectAnswer() {
        return mCorrectAnswer;
    }

    public void setmCorrectAnswer(String mCorrectAnswer) {
        this.mCorrectAnswer = mCorrectAnswer;
    }

    public String getmUserAnswer() {
        return mUserAnswer;
    }

    public void setmUserAnswer(String mUserAnswer) {
        this.mUserAnswer = mUserAnswer;
    }

    public String getmUserTime() {
        return mUserTime;
    }

    public void setmUserTime(String mUserTime) {
        this.mUserTime = mUserTime;
    }

    public String getmQuestion() {
        return mQuestion;
    }

    public void setmQuestion(String mQuestion) {
        this.mQuestion = mQuestion;
    }

    public String getmQuestionLanguage() {
        return mQuestionLanguage;
    }

    public void setmQuestionLanguage(String mQuestionLanguage) {
        this.mQuestionLanguage = mQuestionLanguage;
    }

    public String getmExplain() {
        return mExplain;
    }

    public void setmExplain(String mExplain) {
        this.mExplain = mExplain;
    }
}