package com.nbmedia.vidhvaa.Model;

public class CourseCategory {
    private String cat_code;
    private String cat_name;
    private String ln_code;
    private String cat_flag;

    public CourseCategory(String Cd, String ln, String ct_name, String flg)
    {
        this.cat_code = Cd;
        this.ln_code = ln;
        this.cat_name = ct_name;
        this.cat_flag = flg;

    }

    public void setCat_code(String cat_code) {
        this.cat_code = cat_code;
    }

    public String getCat_code() {
        return cat_code;
    }

    public void setLn_code(String ln_code) {
        this.ln_code = ln_code;
    }

    public String getLn_code() {
        return ln_code;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_flag() {
        return cat_flag;
    }

    public void setCat_flag(String cat_flag) {
        this.cat_flag = cat_flag;
    }
}
