package com.nbmedia.vidhvaa.Model;

public class General {
    private String gs_code;
    private String sb_code;
    private String gs_name;
    private String ln_code;


    public General(String Cd, String ln, String sb, String ct_name)
    {
        this.gs_code = Cd;
        this.ln_code = ln;
        this.sb_code = sb;
        this.gs_name = ct_name;
    }

    public void setGs_code(String gs_code) {
        this.gs_code = gs_code;
    }

    public String getGs_code() {
        return gs_code;
    }

    public String getSb_code() {
        return sb_code;
    }

    public void setSb_code(String sb_code) {
        this.sb_code = sb_code;
    }

    public String getLn_code() {
        return ln_code;
    }

    public void setLn_code(String ln_code) {
        this.ln_code = ln_code;
    }

    public String getGs_name() {
        return gs_name;
    }

    public void setGs_name(String gs_name) {
        this.gs_name = gs_name;
    }
}

