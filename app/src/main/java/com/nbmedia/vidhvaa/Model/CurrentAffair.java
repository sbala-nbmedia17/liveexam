package com.nbmedia.vidhvaa.Model;

public class CurrentAffair {

    private String code;
    private String year;
    private String month;
    private String language;
    private String status;

    public CurrentAffair(String a_code, String a_year, String a_month, String a_language, String a_status)
    {
        this.code = a_code;
        this.year = a_year;
        this.month = a_month;
        this.language = a_language;
        this.status = a_status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
