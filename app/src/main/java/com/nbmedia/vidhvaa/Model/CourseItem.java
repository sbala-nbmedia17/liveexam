package com.nbmedia.vidhvaa.Model;

public class CourseItem {
    private String course_title;
    private String one_title;
    private String ln_code;
    private String two_title;
    private String course_code;

    public CourseItem(String Cd, String ln, String st, String one, String two)
    {
        this.course_title = st;
        this.one_title = one;
        this.two_title = two;
        this.course_code = Cd;
        this.ln_code = ln;
    }


    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getOne_title() {
        return one_title;
    }

    public void setOne_title(String one_title) {
        this.one_title = one_title;
    }

    public String getTwo_title() {
        return two_title;
    }

    public void setTwo_title(String two_title) {
        this.two_title = two_title;
    }

    public String getLn_code() {
        return ln_code;
    }

    public void setLn_code(String ln_code) {
        this.ln_code = ln_code;
    }

    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    public String getCourse_code() {
        return course_code;
    }
}
