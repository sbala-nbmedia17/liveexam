package com.nbmedia.vidhvaa.Model;

public class Leader {
    private String code;
    private String title;
    private String currentDate;
    private String currentTime;

    public Leader(String ap_code, String ap_date, String ap_time, String ap_title)
    {
        this.code = ap_code;
        this.currentDate = ap_date;
        this.currentTime = ap_time;
        this.title = ap_title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }
}
