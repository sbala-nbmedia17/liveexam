package com.nbmedia.vidhvaa.Model;

public class CourseItemNew {
    private String course_title;
    private String one_title;
    private String ln_code;
    private String two_title;
    private String three_title;
    private String course_code;
    private String first_image;
    private String second_image;
    private String third_image;

    public CourseItemNew(String Cd, String ln, String st, String one, String two, String three, String f_img, String s_img, String t_img)
    {
        this.course_title = st;
        this.one_title = one;
        this.two_title = two;
        this.three_title = three;
        this.course_code = Cd;
        this.ln_code = ln;
        this.first_image = f_img;
        this.second_image = s_img;
        this.third_image = t_img;
    }


    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getOne_title() {
        return one_title;
    }

    public void setOne_title(String one_title) {
        this.one_title = one_title;
    }

    public String getTwo_title() {
        return two_title;
    }

    public void setTwo_title(String two_title) {
        this.two_title = two_title;
    }

    public String getLn_code() {
        return ln_code;
    }

    public void setLn_code(String ln_code) {
        this.ln_code = ln_code;
    }

    public void setCourse_code(String course_code) {
        this.course_code = course_code;
    }

    public String getCourse_code() {
        return course_code;
    }

    public String getThree_title() {
        return three_title;
    }

    public void setThree_title(String three_title) {
        this.three_title = three_title;
    }

    public String getFirst_image() {
        return first_image;
    }

    public void setFirst_image(String first_image) {
        this.first_image = first_image;
    }

    public String getSecond_image() {
        return second_image;
    }

    public void setSecond_image(String second_image) {
        this.second_image = second_image;
    }

    public String getThird_image() {
        return third_image;
    }

    public void setThird_image(String third_image) {
        this.third_image = third_image;
    }
}
