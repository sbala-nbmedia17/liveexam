package com.nbmedia.vidhvaa.Model;

public class DailyAffair {
    private String dy_date;
    private String dy_day;

    public DailyAffair(String ap_dy_date, String ap_dy_day)
    {
        this.dy_date = ap_dy_date;
        this.dy_day = ap_dy_day;
    }

    public String getDy_date() {
        return dy_date;
    }

    public void setDy_date(String dy_date) {
        this.dy_date = dy_date;
    }

    public String getDy_day() {
        return dy_day;
    }

    public void setDy_day(String dy_day) {
        this.dy_day = dy_day;
    }
}
