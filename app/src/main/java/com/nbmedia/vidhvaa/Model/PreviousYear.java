package com.nbmedia.vidhvaa.Model;

public class PreviousYear {
    private String code;
    private String language;
    private String year;
    private String question1;
    private String question2;
    private String status;

    public PreviousYear(String a_code, String a_language, String a_year, String a_question1, String a_question2, String a_status)
    {
        this.code = a_code;
        this.language = a_language;
        this.year = a_year;
        this.question1 = a_question1;
        this.question2 = a_question2;
        this.status = a_status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
