package com.nbmedia.vidhvaa.Model;

public class DailyAffairEvent {
    private String code;
    private String language;
    private String date;
    private String day;
    private String material;
    private String more;

    public DailyAffairEvent(String ap_code, String ap_language, String ap_date, String ap_day, String ap_material, String ap_more)
    {
        this.code = ap_code;
        this.language = ap_language;
        this.date = ap_date;
        this.day = ap_day;
        this.material = ap_material;
        this.more = ap_more;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getMore() {
        return more;
    }

    public void setMore(String more) {
        this.more = more;
    }
}
