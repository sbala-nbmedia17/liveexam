package com.nbmedia.vidhvaa.Model;

public class ModelQuestionResult implements java.io.Serializable{

    private String ques_id;
    private String ques_code;
    private String question;
    private String question_order;
    private String ans_1;
    private String ans_2;
    private String ans_3;
    private String ans_4;
    private String correct_answer;
    private String explain;
    private String user_answer;
    private String language;

    public ModelQuestionResult() {
        this.ques_id = "";
        this.ques_code = "";
        this.question = "";
        this.question_order = "";
        this.ans_1 = "";
        this.ans_2 = "";
        this.ans_3 = "";
        this.ans_4 = "";
        this.correct_answer = "";
        this.explain = "";
        this.language = "";
    }

    public String getQues_id() {
        return this.ques_id;
    }

    public void setQues_id(String ques) {
        this.ques_id = ques;
    }

    public String getQues_code( ) {return this.ques_code; }

    public void setQues_code(String q)
    {
        this.ques_code =q;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String ques) {
        this.question = ques;
    }

    // First
    public String getAns_1() {
        return this.ans_1;
    }

    public void setAns_1(String an) {
        this.ans_1 = an;
    }

    // Second
    public String getAns_2() {
        return this.ans_2;
    }

    public void setAns_2(String an) {
        this.ans_2 = an;
    }

    // Third
    public String getAns_3() {
        return this.ans_3;
    }

    public void setAns_3(String an) {
        this.ans_3 = an;
    }

    // Fourth
    public String getAns_4() {
        return this.ans_4;
    }

    public void setAns_4(String an) {
        this.ans_4 = an;
    }

    //Correct Answer
    public String getCorrect_answer() {
        return this.correct_answer;
    }

    public void setCorrect_answer(String an) {
        this.correct_answer = an;
    }

    // Explanation
    public String getExplain() {
        return this.explain;
    }

    public void setExplain(String an) {
        this.explain = an;
    }

    //User Answer
    public String getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }

    public String getQuestion_order() {
        return question_order;
    }

    public void setQuestion_order(String question_order) {
        this.question_order = question_order;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}

